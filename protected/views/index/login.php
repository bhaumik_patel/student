<!--  Light boxes -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/multi/jquery.qtip.min.css" />
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/multi/jquery.qtip.min.js"></script>
<?php
$this->pageTitle=Yii::app()->name . ' - Login';
?>
<div class="home_sidebar_box mrgn_b20"> 
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>false,
//	 'action'	=>	'eh/index/login',
)); ?>
	<?php
    foreach(Yii::app()->customer->getFlashes() as $key => $message) {
        echo "<div class='flash-error-client'>" . $message . "</div>\n";
    }
	?>
<span id="login-validation"  class="flash-error-client" style="display:none;"></span>
<p>		
<?php //echo $form->labelEx($loginForm,'username'); ?>
<?php echo $form->textField($loginForm,'username',array('title'=>'Full name or e-mail')); ?>
<?php echo $form->error($loginForm,'username'); ?>
</p>

<p>
<?php //echo $form->labelEx($loginForm,'password'); ?>
<?php echo $form->passwordField($loginForm,'password',array('title'=>'Password')); ?>
<?php echo $form->error($loginForm,'password'); ?>
</p>
 <div class="default_width overflow_hidden">
	 <div class="remember_me fl">
		<?php echo $form->label($loginForm,'rememberMe'); ?>
		<?php echo $form->checkBox($loginForm,'rememberMe'); ?>		
		<?php echo $form->error($loginForm,'rememberMe'); ?>
	</div>
	<div class="fr"><INPUT TYPE="image" SRC="<?php echo Yii::app()->request->baseUrl; ?>/images/sign-in-btn.jpg" ALT="Submit Form" width="65" height="26" class="fr"></div>
<?php $this->endWidget(); ?>	
	<div class="forgot_password fl">
		<a href="#" rel="<?php echo $this->createUrl("/index/retrievepassword");?>" class="tooltip_pop"><span class="right_link fr">Forgot Password</span></a></div> 
   </div>

</div>
<!-- form -->

<!--  On Focus Default Text -->
<script>
$('input[type="text"]').each(function(){
 
    this.value = $(this).attr('title');
    $(this).addClass('text-label');
 
    $(this).focus(function(){
        if(this.value == $(this).attr('title')) {
            this.value = '';
            $(this).removeClass('text-label');
        }
    });
 
    $(this).blur(function(){
        if(this.value == '') {
            this.value = $(this).attr('title');
            $(this).addClass('text-label');
        }
    });
});
$('input[type="password"]').each(function(){
	 
    this.value = $(this).attr('title');
    $(this).addClass('text-label');
 
    $(this).focus(function(){
        if(this.value == $(this).attr('title')) {
            this.value = '';
            $(this).removeClass('text-label');
        }
    });
 
    $(this).blur(function(){
        if(this.value == '') {
            this.value = $(this).attr('title');
            $(this).addClass('text-label');
        }
    });
});

$('#login-form').submit(function() {
	  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	  var email = $("#Customer_email").attr('value');
	  if($("#LoginForm_username").attr('value')=="Full name or e-mail"){
		  document.getElementById("login-validation").innerHTML="Please enter fullname or email";
		  document.getElementById("login-validation").style.display="block";
		  document.getElementById("LoginForm_username").focus();
		  return false;
	  }else if($("#LoginForm_password").attr('value')=="Password"){	  
		  document.getElementById("login-validation").innerHTML="Password cannot be blank";
		  document.getElementById("login-validation").style.display="block";
		  document.getElementById("LoginForm_password").focus();
		  return false;
	  }else{
			return true;
	  }
	});
</script>

<!--  Light Boxes  -->
<script>
$(document).ready(function()
{
   $('a.tooltip_pop').each(function()
   {
      $(this).qtip(
      {
         content: {
            text: 'Loading',
            ajax: {
               url: $(this).attr('rel') // Use the rel attribute of each element for the url to load
            },
            title: {
               text: $(this).text(), // Give the tooltip a title using each elements text
               button: true
            }
         },
         position: {
            at: 'bottom center', // Position the tooltip above the link
            my: 'top center',
            viewport: $(window), // Keep the tooltip on-screen at all times
            effect: false // Disable positioning animation
         },
         show: {
            event: 'click',
            solo: true // Only show one tooltip at a time
         },
         hide: 'unfocus',
         style: {
            classes: 'ui-tooltip-wiki ui-tooltip-light ui-tooltip-shadow'
         }
      })
   })
   .click(function(event) { event.preventDefault(); });
});
</script>

