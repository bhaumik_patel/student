Verification has been completed for your <?php $email ?>.
Thank you for verifying your account with <?php echo SystemConfig::getValue('site_name')?>. 
You now have access to your account and related services.