<div style="border: 1px solid #01538b; width: 599px; min-height: 540px; color: #616161">
	<div
		style="color: #616161; font-size: 13px; padding-left: 30px; padding-right: 20px; margin-top: 8px; line-height: 18px; min-height: 130px">
		<p style="line-height: 18px; margin: 0; padding: 0">
			Dear User (<b><?php echo ucfirst($firstname)?> </b>),
		</p>
		<p style="text-indent: 2em; line-height: 18px; margin: 0; padding: 0">
			Welcome to visit <span class="il"><?php echo SystemConfig::getValue('site_name')?>
			</span>.com! This is a website that mainly providing the service for
			online advertisement.
		</p>
		<p style="text-indent: 2em; line-height: 18px; margin: 0; padding: 0">
			You have <span class="il">registered</span> to be our member
			successfully, and below is your account information, please keep it
			carefully.
		</p>
	</div>
	<div
		style="background-color: rgb(119, 190, 238); background-image: url(); width: 599px; font-size: 18px; line-height: 30px; color: #fff; min-height: 68px">
		<span style="margin-right: 70px">
		User: <a style="color: #FFFFF" href="mailto:<?php echo $email?>" target="_blank"><?php echo $email?></a> </span>
		<BR /> 
		<span style="margin-right: 70px">password:<?php echo $password?></span>
	</div>
	<div
		style="color: #616161; font-size: 13px; padding-left: 30px; padding-right: 20px; margin-top: 8px; line-height: 18px;">
		<p>You can access your account after click on the verification link as
			mentioned below.</p>
		<p>
			Click to <a href="<?php echo $verification_link?>">Verify</a>
		</p>
	</div>
	<div
		style="font-size: 12px; padding-left: 30px; padding-right: 30px; line-height: 18px; color: #616161; margin-top: 8px">


		<div style="padding-left: 2em; margin-top: 10px">
			<p style="line-height: 18px; margin: 0; padding: 0">Sincerely,</p>
			<p style="line-height: 18px; margin: 0; padding: 0">
				<span class="il"><?php echo SystemConfig::getValue('site_name')?> </span>
				Service Team
			</p>
			<p style="line-height: 18px; margin: 0; padding: 0">Ahmedabad
				Operational Headquarters</p>
			<p style="line-height: 18px; margin: 0; padding: 0">
				<a href="<?php echo UtilityHtml::getFullUrl('service')?>"
					style="text-decoration: underline" target="_blank">Customers
					Service Center</a>
			</p>
		</div>
	</div>
	<div
		style="font-size: 11px; padding-left: 30px; line-height: 14px; color: #a6a6a6; margin-top: 20px">
		<p style="line-height: 18px; margin: 0; padding: 0">
			Copyright � <?php echo date('Y')?>
		</p>
	</div>
</div>
