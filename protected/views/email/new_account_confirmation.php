<?php if(!empty($model)):?>
<?php $customer	=	new Customer()?>
<?php $confirmationURL	=	$customer->encodeConfirmationLink($model->email);?>
<div style="background:#F6F6F6; font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px; margin:0; padding:0;">
<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td align="center" valign="top" style="padding:20px 0 20px 0"><!-- [ header starts here] -->
            <table width="650" cellspacing="0" cellpadding="10" border="0" bgcolor="FFFFFF" style="border:1px solid #E0E0E0;">
                <tbody>
                    <tr>
                        <td valign="top">
                        <div class="logo"><a href="<?php echo  $_SERVER["HTTP_HOST"].Yii::app()->request->baseUrl;?>"><img width="154" height="26" src="<?php echo Yii::getPathOfAlias('webroot') ?>;/images/logo.jpg" alt="easyhopping" title="easyhopping" /></a></div>
                        </td>
                    </tr>
                    <!-- [ middle starts here] -->
                    <tr>
                        <td valign="top">
                        <h1 style="font-size:22px; font-weight:normal; line-height:22px; margin:0 0 11px 0;">Dear <?php echo $model->fullname?>;,</h1>
                        <p style="font-size:12px; line-height:16px; margin:0 0 16px 0;">Your e-mail must be confirmed before using it to log in.</p>
                        <p style="font-size:12px; line-height:16px; margin:0 0 8px 0;">To confirm the e-mail and instantly log in, please, use <a href="<?php echo $confirmationURL?>" style="color:#1E7EC8;">this confirmation link</a>. This link is valid only once.</p>
                        <p style="border:1px solid #E0E0E0; font-size:12px; line-height:16px; margin:0 0 16px 0; padding:13px 18px; background:#f9f9f9;">Use the following values when prompted to log in:<br />
                        <strong>E-mail:</strong> <?php echo $model->email;?>; <br />
                        <strong>Password:</strong> <?php echo $password;?></p>
                        <p>&nbsp;</p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#EAEAEA" style="background:#EAEAEA; text-align:center;"><center>
                        <p style="font-size:12px; margin:0;">Thank you again, <strong><?php echo SystemConfig::getValue('site_name')?> Team</strong></p>
                        </center></td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>
</div>
<?php endif;?>