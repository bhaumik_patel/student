<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'bjois-ad-file-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'ad_id',); ?>
		<?php echo $form->dropDownList($model, 'ad_id', GxHtml::listDataEx(BjoisAd::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'ad_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'user_id',); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(BjoisUser::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'user_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'file',); ?>
		<?php echo $form->textField($model, 'file', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'file'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'file_size',); ?>
		<?php echo $form->textField($model, 'file_size', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'file_size'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'file_info_type',); ?>
		<?php echo $form->textField($model, 'file_info_type', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'file_info_type'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'file_type',); ?>
		<?php echo $form->textField($model, 'file_type', array('maxlength' => 8)); ?>
		<?php echo $form->error($model,'file_type'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at',); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at',); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status',); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->