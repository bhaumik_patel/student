<?php
$criteria = new CDbCriteria();
$criteria->condition = 't.is_teaser=1 AND t.status=1';
$criteria->order = 't.position ASC';
$criteria->limit = 4;
$criteria->offset = 0;



if($tvShows = Tvshows::getListBanners($criteria)): ?>
<div class="tvShows_banners">
	<div class="content">
		<ul>
		<?php foreach($tvShows as $banner): ?>
			<li>
				<div class="tvShows_banners_img">
				<?php echo CHtml::link($banner['image'], $banner['link']) ?>
				</div>
				<div class="tvShows_banners_text">
				<?php echo CHtml::link($banner['description'], $banner['link'])?>
				</div>
			</li>
			<?php endforeach;?>
		</ul>
	</div>
</div>
<?php endif;?>