<?php
	$criteria = new CDbCriteria();
	$criteria->addCondition('t.is_banner=1 AND t.status=1');
	$criteria->join = 'LEFT JOIN mst_tvshows AS tv ON tv.id = t.tvshow_id
			LEFT JOIN mst_hosts AS h ON h.id = t.host_id';
	$criteria->select = 't.*, tv.custom_url_key as tv_url';
	
	if($show_id = Yii::app()->request->getParam('show_id')) {
		$criteria->addCondition('t.tvshow_id= \''.$show_id.'\' OR tv.custom_url_key = \''.$show_id.'\' ');
	}
	if($host_id = Yii::app()->request->getParam('host_id')) {
		$criteria->addCondition('tv.host_id = \''.$host_id.'\' OR h.custom_url_key = \''.$host_id.'\' ');
	}
	$criteria->order = 't.position ASC';
	$banners = Videos::getFeatureBanner($criteria);
	?>
	<?php if($banners): ?>
<div class="main_slider">
	<div class="content">
	<?php
		$this->widget('application.extensions.OrbitSlider.OrbitSlider', array(
            	'images'=>$banners,
	                'slider_options'=>array(
                        'width'=>SystemConfig::getValue('home_slider_width'),
                        'height'=>SystemConfig::getValue('home_slider_height'),
                        'animation'=>'horizontal-slide',
                        'bullets'=>true,
						'timer'=>true,
						'animationSpeed'=>SystemConfig::getValue('home_slider_speed'),
						'startClockOnMouseOut' => false,
						'startClockOnMouseOutAfter'=>SystemConfig::getValue('home_slider_clock_mouse_after_speed'),
						'advanceSpeed'=>SystemConfig::getValue('home_slider_advance_speed'),
						//'captionAnimationSpeed'=>'2000',
		)
		));
	?>
	<?php endif;?>
	</div>
</div>
