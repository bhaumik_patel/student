<?php 
$criteria = new CDbCriteria();

if($show_id = Yii::app()->request->getParam('show_id')) {
	$criteria->join = " LEFT JOIN mst_tvshows as tv ON tv.host_id=t.id ";
	$criteria->addCondition("tv.id='".$show_id."' OR tv.custom_url_key = '".$show_id."'");
}

if($host_id = Yii::app()->request->getParam('host_id')) {
	$criteria->join = 'LEFT JOIN mst_tvshows AS tv ON tv.host_id=t.id 
			LEFT JOIN mst_hosts AS h ON h.id = tv.host_id';
	$criteria->addCondition('h.id = \''.$host_id.'\' OR h.custom_url_key = \''.$host_id.'\' ');
}
$criteria->addCondition('t.status=1');
if($host = Hosts::model()->find($criteria)): ?>
<div class="tvShows_banners">
                <div class="content">
                        <div class="tvShows_banners_left">
                            <div class="host-image">
									<?php echo $host->getHostImage($host)?>
                            </div>
                        </div>
                       <div class="tvShows_banners_right">
                       <div class="host-name"><?php echo $host->name?></div>
                    <div class="host-desc"><?php echo $host->description?></div>
                    <?php //p($host->getRelated('tvshows')); ?>
                   
                    </div>
                     <div class="host-show-img">
                    	<?php foreach($host->getRelated('tvshows') as $rowShow) :?>
                    	<?php if($img = Tvshows::getTvShowImage($rowShow->id)): ?> 
                    		<?php echo CHtml::link($img, CController::createUrl('show/index', array('show_id'=>$rowShow->custom_url_key)) )?>
                    	<?php endif; ?>
                    	<?php endforeach;?>
                    </div>
                </div>
	</div>
<?php endif;?>