<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/stylesheet.css"/>
	<link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/favicon.ico" type="image/x-icon" /> 
	<script language="JavaScript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
<!-- FaceBook Like Script -->
<div id="wrapper">
	<div id="header">
		<?php $this->renderPartial('/layouts/header');?>
	</div>
	<div id="contentarea" <?php if(($this->getAction()->id=="login") || ($this->getAction()->id=="signupcomplete")):?>class="home-col2-layout" <?php endif;?>>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
				'links'=>$this->breadcrumbs,
			)); ?>
		<!-- breadcrumbs -->
			<?php echo $content; ?>
	</div>
	<!-- Footer -->
	<div id="footer">
		<?php $this->renderPartial('/layouts/footer');?>		
  </div>
  <!-- Footer -->
</div>
<!-- page -->




</body>
</html>