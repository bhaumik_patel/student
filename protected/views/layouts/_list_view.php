<div class="recent_episode_list">
	<div class="thumb_video">
		<?php // echo Videos::getPlayer($data, 'thumb'); ?>
		<?php echo CHtml::link(Videos::getThumbImage($data), 
			CController::createUrl('/video/detail', 
				array(
					'show_id' =>$data->tv_url,
					'id'=>$data->custom_url_key))); ?>
    </div>
	<h4><b><?php echo $data->show_title;?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->title), CController::createUrl('/video/detail', 
			array(
					'show_id' =>$data->tv_url,
					'id'=>$data->custom_url_key))); ?></h4>
	<span class="publish_date"><?php echo $data->publish_up ?></span>
	 <span class="season"><?php echo $data->type_name;?>
				<?php if($data->type_name=='season') : ?>
							<?php if($data->season!='') echo Yii::t('inx', 'Season'). $data->season; ?>&nbsp;&nbsp;
                            <?php echo Yii::t('inx', 'Episode').': '.$data->episode_no; ?>(<?php echo $data->time;?>)
				<?php else: ?>
						(<?php echo $data->time;?>)
				<?php endif;?>
      </span>
  <span class="more_keywords">
	<b><?php echo Yii::t('inx', 'More')?>:</b>
	<?php if($keywords = Keywords::getKeywords($data->keyword_ids)): ?> 
	<?php $i=0;?>
	<?php foreach($keywords as $row) : ?>
		<?php echo CHtml::link(trim($row->title), CController::createUrl('show/index', 
			array('keyword_id'=>$row->custom_url_key)))?>, 
		<?php if($i==2) { echo CHtml::link(Yii::t('inx', 'More'), CController::createUrl('index'));  break; }?>
		<?php $i++;?>
	<?php endforeach; ?>
	<?php endif;?>
	<?php //p($data->attributes)?>
    </span>
</div>