<div class="content">
	<div class="head_row1">
		<div class="logo">
			<a href="<?php echo Yii::app()->homeUrl; ?>"> <img src="<?php echo Yii::app()->baseUrl; ?>/images/logo.png" alt="<?php echo SystemConfig::getValue('site_title')?>" title="<?php echo SystemConfig::getValue('site_title')?>" /> </a>
		</div>
		<div class="head_col2">
			<?php /* 
			<ul>
				<li class="top-icon1"><span> &nbsp; </span> <?php echo Yii::t('inx', 'Your <em>Interest</em> Online')?> </li>
				<li class="top-icon2"><span>&nbsp; </span> <?php echo Yii::t('inx', 'Completely <em> Free and Accessible </em> from anywhere')?></li>
				<li class="top-icon3 last"><span>&nbsp; </span><?php echo Yii::t('inx', 'Professional <em> in TV quality</em>')?>	</li>
			</ul>
			 */?>
		</div>
		<div class="head_col3">
			<?php echo Chtml::link(Yii::t('inx', 'Contact Us'), CController::createUrl('index/contact'))?>
			| <?php echo Chtml::link(Yii::t('inx', 'Support'), CController::createUrl('index/support'))?>
		</div>
	</div>
	<div class="head_row2">
		<div class="head_nav">
			<?php $this->widget('FrontMenu', JVAccessControlFilter::getFrontMenuItems()); ?>
		</div>
		<div class="search_mod">
		<?php $form = $this->beginWidget('GxActiveForm', array(
				'id' => 'search-form',
				'action'=> CController::createUrl('/show'),
				'method'=>'post',
				'enableAjaxValidation' => false,
 				'htmlOptions'=>array('enctype'=>'multipart/form-data'),
			));
		?>
			<input type="text" name="search" value="" /> 
			<?php
		echo GxHtml::submitButton(Yii::t('app', 'submit'), array('class'=>'search_btn'));
		$this->endWidget();
		?>
		</div>
	</div>
</div>
