<?php
/**
 * VikingBro
 * @author: Bhaumik PHP Guru
 * @date: Feb 7, 2013  7:10:37 PM
 */
class UpgradeForm extends CFormModel
{
	//input params
	public $api_token;
	public $current_version;
	
	
	public $order;
	
	public function rules()
	{
		return array(
			array('api_token, current_version', 'required'),
			array('api_token, current_version', 'length', 'max'=>250),
			array('api_token', 'isAuthenticate'),
			array('api_token, current_version', 'safe'),
		);
	}
	public function isAuthenticate($attribute)
	{
		$valid = ApiCustomer::isValidToken($this->api_token);
		if(UtilityHtml::getDataByKey($valid,'success')=='0') {
			$this->addError($attribute, UtilityHtml::getDataByKey($valid,'error'));
		}
		if(UtilityHtml::getDataByKey($valid,'order')) {
			$this->order = UtilityHtml::getDataByKey($valid,'order');
		}
		return false;
	}
	
}