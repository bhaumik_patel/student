<?php
/**
 * VikingBro
 * @author: Bhaumik PHP Guru
 * @date: Feb 7, 2013  7:10:45 PM
 */
class ApiOrder extends VkOrders
{
	public function getUpgradeInfo($data)
	{
		$result = array();
		$order = $data->order;
		if($order) {
			$criteria = new CDbCriteria();
			$criteria->join = "LEFT JOIN `vk_product_release` as t1 ON t1.id > t.id  AND t.`version` =  '".UtilityHtml::getDataByKey($data,'current_version')."'
							   LEFT JOIN vk_orders as o ON o.product_id = t1.product_id";
			$criteria->addCondition("t1.product_id = '".$order->product_id."' AND  t1.status='1'
			AND o.allow_upgrade=1 AND  now() > o.allo_upgrade_till");
			$criteria->order = 't1.id ASC';
			$criteria->select = 't1.*';
			$newReleases = VkProductRelease::model()->findAll($criteria);
			if(count($newReleases)>0) {
				foreach ($newReleases as $row) {
					$array = array(
						'product_id' 			=> $row->product_id,
						'version' 				=> $row->version,
						'full_source_file' 		=> $row->full_source_file,
						'full_source_file_size' => $row->full_source_file_size,
						'full_db_file' 			=> $row->full_db_file,
						'full_db_file_size' 	=> $row->full_db_file_size,
						 
						'upgrade_source_file' 	=> $row->upgrade_source_file,
						'upgrade_source_file_size' => $row->upgrade_source_file_size,
						'upgrade_db_file' 		=> $row->upgrade_db_file,
						'upgrade_db_file_sizse'	=> $row->upgrade_db_file_size,
						'notes' 				=> $row->notes,
					);

					$result['result'][] = $array;
				}
			}
		}
		if(count($result)==0){
			$result['error'] = Yii::t('vk','No upgrades available!');
		}
		return $result;
	}
	public function upgrade()
	{
		$model = new VkOrderUpgradeHistory();
		// Check current version with the old version

		// if the version is
	}
}