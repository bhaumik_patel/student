<?php
/**
 * VikingBro
 * @author: Bhaumik PHP Guru
 * @date: Feb 7, 2013  7:10:45 PM
 */
class AuthenticateForm extends CFormModel
{
	public $api_key;
	public $api_username;
	public $other;
	
	public function rules()
	{
		return array(
			array('api_username, api_key', 'required'),
			array('api_username, api_key', 'length', 'max'=>250),
			array('api_username, api_key', 'safe'),
		);
	}
}