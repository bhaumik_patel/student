<?php
/**
 * VikingBro
 * @author: Bhaumik PHP Guru
 * @date: Feb 7, 2013  7:10:45 PM
 */
class ApiCustomer extends VkCustomer
{
	public $order;
	/**
	 * Enter description here ...
	 * @param unknown_type $data
	 * @return Array
	 */
	public function AuthenticateKey($data)
	{

		$criteria = new CDbCriteria();
		$criteria->join = "LEFT JOIN vk_customer as c ON c.id = t.customer_id AND c.username='".UtilityHtml::getDataByKey($data,'api_username')."'";
		$criteria->addCondition("t.api_key='".UtilityHtml::getDataByKey($data,'api_key')."'");
		/* @var $order VkOrders */
		$order = VkOrders::model()->find($criteria);
		$response=array();
		$response['success'] = 0;
		if($order) {
			if($order->order_status == VkOrders::STATUS2) {
				$order->api_token = UtilityHtml::generateRandomKey($order->customer_id.'_'.$order->product_id);
				$order->last_logged_in_at = new CDbExpression('now()');
				$order->total_loggedin +=1;
				$order->save(false);
				$response['success'] = 1;
			}
		}else {
			$response['errors']['custom'] = Yii::t('vk','fail to authenticate, Your user name and key not valid!');
		}
		return $response;
	}

	public function apiLogout($data)
	{
		$criteria = new CDbCriteria();
		$criteria->join = "LEFT JOIN vk_customer as c ON c.id = t.customer_id AND c.username='".UtilityHtml::getDataByKey($data,'api_username')."'";
		$criteria->addCondition("t.api_token='".UtilityHtml::getDataByKey($data,'api_token')."'");
		/* @var $order VkOrders */
		$order = VkOrders::model()->find($criteria);
		$response=array();
		$response['success'] = 0;
		if($order) {
			if($order->order_status == VkOrders::STATUS2) {
				$order->api_token = NULL;
				$order->save(false);
				$response['api_token'] = $order->api_token;
				$response['success'] = 1;
			}
		}else {
			$response['errors']['custom'] = Yii::t('vk','fail to logout, You may already logout!');
		}
		return $response;
	}
	
	public static function isValidToken($token)
	{
		$response=array();
		$response['success'] = 0;
		$validFlag = false;
		$criteria = new CDbCriteria();
		$criteria->join = "LEFT JOIN vk_customer as c ON c.id = t.customer_id";
		$criteria->addCondition("t.api_token='".$token."'");
		$criteria->select = '!((t.`expired_at` IS NULL) || (t.`expired_at`=\'0000-00-00 00:00:00\') || (t.`expired_at` > now())) as is_expired, t.*';
		/* @var $order VkOrders */
		$order = VkOrders::model()->find($criteria);
		
		if(!$token) {
			$response['errors']['custom'] = Yii::t('vk','Api token is invalid');
		}if($order) {
			$response['order'] = $order;
			if($order->order_status == VkOrders::STATUS2) {
				if($order->is_expired) {
					$response['errors']['custom'] = Yii::t('vk','Your application is expired, Please contact to support team');
				}else {
					$response['success'] = 1;
				}
			}else {
				$response['errors']['custom'] = Yii::t('vk','Your account is suspected, Please contact to support team.');
				$response['success'] = 0;
			}
		}else {
			$response['errors']['custom'] = Yii::t('vk','Api token is invalid');
		}
		return $response;
	}
}