<?php
/**
 * VikingBro
 * @author: Bhaumik PHP Guru
 * @date: Feb 7, 2013  7:10:42 PM
 */
class LogoutForm extends CFormModel
{
	public $api_token;
	public $api_username;
	public $other;
	
	public function rules()
	{
		return array(
			array('api_username, api_token', 'required'),
			array('api_username, api_token', 'length', 'max'=>250),
			array('api_username, api_token', 'safe'),
		);
	}
}