<?php
/**
 * VikingBro
 * @author: Bhaumik PHP Guru
 * @date: Feb 7, 2013  2:32:16 PM
 */
class  CustomerController extends ApiController
{
	/**
	 * Enter description here ...
	 */
	public function actionAuth()
	{
		$data = $this->getInput();

		$form = new AuthenticateForm;
		$form->attributes = $data;
		$response['success']=0;
		if(!$form->validate()) {
			$response['errors'] = $form->errors;
		}else {
			$customer = new ApiCustomer();
			$response=array_merge($response,$customer->AuthenticateKey($data));
		}
		$this->sendOutput($response);
	}

	public function actionLogout()
	{
		$data = $this->getInput();

		$form = new LogoutForm();
		$form->attributes = $data;
		$response['success']=0;
		if(!$form->validate()) {
			$response['errors'] = $form->errors;
		}else {
			$customer = new ApiCustomer();
			$response=array_merge($response,$customer->ApiLogout($data));
		}
		$this->sendOutput($response);
	}
}