<?php
/**
 * VikingBro
 * @author: Bhaumik PHP Guru
 * @date: Feb 7, 2013  2:32:16 PM
 */
class  OrderController extends ApiController
{
	/**
	 * Enter description here ...
	 */
	public function actionUpgradeInfo()
	{
		$data = $this->getInput();
		$form = new UpgradeForm;
		$form->attributes = $data;
		$response['success']=0;
		if(!$form->validate()) {
			$response['errors'] = $form->errors;
		}else {
			$order = new ApiOrder();
			$response['success']=1;
			$response=array_merge($response, $order->getUpgradeInfo($form));
		}
		$this->sendOutput($response);
	}
}