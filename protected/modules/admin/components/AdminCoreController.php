<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */

class AdminCoreController extends GxController
{
	public $layout 		= 'admin';
	public $accessRule  = '';
	public $userType 	= 'admin';
	public $defaultAction = 'admin';
	
	

	public function __construct($id,$module=null)
	{
		parent::__construct($id,$module);
		
		$this->initTheme();		
		//set usertype
		$admin = Yii::app()->admin->getState('admin');
		if(isset($admin['user_type']) && $admin['user_type'] !='') {
			$this->userType=$admin['user_type'];
		}
		
		$cs=Yii::app()->getClientScript();
		
		// Admin SB Theme
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'bower_components/bootstrap/dist/css/bootstrap.min.css');
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'bower_components/metisMenu/dist/metisMenu.min.css');
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'dist/css/sb-admin-2.css');
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'bower_components/font-awesome/css/font-awesome.min.css');
		  
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'bower_components/jquery/dist/jquery.min.js');
    	$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'bower_components/bootstrap/dist/js/bootstrap.min.js');
    	$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'bower_components/metisMenu/dist/metisMenu.min.js');
    	$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'dist/js/sb-admin-2.js');
		
    	/*
		
		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery-ui-1.8.11.custom.min.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.tipsy.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery-settings.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/toogle.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.uniform.min.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.wysiwyg.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.tablesorter.min.js');
		$cs->registerScriptFile(Yii::app()->baseUrl.'/js/colorbox/jquery.colorbox.js');
		$cs->registerCssFile(Yii::app()->baseUrl.'/css/colorbox/colorbox.css');
		//$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/popup.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/fullcalendar.min.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.prettyPhoto.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.ui.core.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.ui.mouse.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.ui.widget.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.ui.slider.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.ui.datepicker.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.ui.tabs.js');
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.ui.accordion.js');
		$cs->registerScriptFile(Yii::app()->baseUrl.'/js/common.js');
		//p($this);
		$cs->registerScriptFile(UtilityHtml::getAdminSkinUrl().'/js/jquery.dataTables.js');
		
	//	$cs->registerScriptFile(Yii::app()->baseUrl.'/js/DP_DateExtensions.js');
		
		
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'/../common.css');
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'/css/reset.css');
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'/css/root.css');
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'/css/grid.css');
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'/css/typography.css');
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'/css/jquery-ui.css');
		$cs->registerCssFile(UtilityHtml::getAdminSkinUrl().'/css/jquery-plugin-base.css');
		*/
		
		$this->accessRule = new UserAccessControll();
	}
	
	/**
	 * Theme initialization
	 */
	public function initTheme()
	{
		$theme = SystemConfig::getValue('admin_theme');
		Yii::app()->setTheme($theme);
	}
	
	/**
	 * The filter method for 'accessControl' filter.
	 * This filter is a wrapper of {@link CAccessControlFilter}.
	 * To use this filter, you must override {@link accessRules} method.
	 * @param CFilterChain $filterChain the filter chain that the filter is on.
	 */
	public function filterAccessControl($filterChain)
	{
		//$filter=new CAccessControlFilter;
		$filter=new JVAccessControlFilter;
		$filter->setRules($this->accessRules());
		$filter->filter($filterChain);
	}

	public function getControllerName()
	{
		return get_class($this);
	}
	public function getModuleId()
	{
		return $this->accessRule->getModule($this->getModule()->id, 'id');
	}
	public function defaultAccessRules()
	{
		return array(
		array('allow',
			'actions'=>array('index','view'),
			'roles'=>array('*'),
			'desc'=>'List / Details Data',
		),
		array('allow',
			'actions'=>array('minicreate', 'create','update'),
			'roles'=>array('UserCreator'),
			'desc'=>'Add / Update Data',
		),
		array('allow',
			'actions'=>array('admin','delete'),
			'users'=>array($this->userType),
			'desc'=>'Delete and Manage Operation',
		),
		array('deny',
			'users'=>array('*'),
		),
		);
	}
	public function getRole()
	{
		return AdminModule::getUserRoles();
	}
	public function accessRules($userType = 'admin', $isDefault=false)
	{
		$user_roles = $this->getRole();
		
		//$user_roles = ((CustomerModule::getUserDataByKey('user_roles')!='')?CustomerModule::getUserDataByKey('user_roles'):"''");
		$models = UserRules::model()->findAll("privileges_controller = '".$this->getControllerName()."'
		AND module_id = '".$this->getModuleId()."' AND role_id IN (".$user_roles.")");
		
		foreach($models as $model) {
			$array[] = array(
			$model->permission,
				'actions'=>explode(',',$model->privileges_actions),
				'users'=>explode(',',$model->permission_type),
				'desc'=>$model->role_desc,
			);
		}

		
		if(isset($array)) {
			return $array;
		}else {
			return $this->defaultAccessRules();
		}
	}
}