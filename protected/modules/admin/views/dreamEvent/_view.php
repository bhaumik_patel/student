<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('branch_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->branch)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('song_ids')); ?>:
	<?php echo GxHtml::encode($data->song_ids); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name')); ?>:
	<?php echo GxHtml::encode($data->name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('details')); ?>:
	<?php echo GxHtml::encode($data->details); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('contact_name')); ?>:
	<?php echo GxHtml::encode($data->contact_name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('contact_no')); ?>:
	<?php echo GxHtml::encode($data->contact_no); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('fees')); ?>:
	<?php echo GxHtml::encode($data->fees); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('event_total_minute')); ?>:
	<?php echo GxHtml::encode($data->event_total_minute); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_editing_done')); ?>:
	<?php echo GxHtml::encode($data->is_editing_done); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('start_date')); ?>:
	<?php echo GxHtml::encode($data->start_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('end_date')); ?>:
	<?php echo GxHtml::encode($data->end_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('created_at')); ?>:
	<?php echo GxHtml::encode($data->created_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('updated_at')); ?>:
	<?php echo GxHtml::encode($data->updated_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_paid')); ?>:
	<?php echo GxHtml::encode($data->is_paid); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	*/ ?>

</div>