<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

/*$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);*/
$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/question.png'), 'url'=>array('#'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage Event')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create Event')), 
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/refresh.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Refresh')),
	);
?>

<!--h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1-->

<?php /*$this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'branch',
			'type' => 'raw',
			'value' => $model->branch !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->branch)), array('dreamBranch/view', 'id' => GxActiveRecord::extractPkValue($model->branch, true))) : null,
			),
'song_ids',
'name',
'details',
'contact_name',
'contact_no',
'fees',
'event_total_minute',
'is_editing_done:boolean',
'start_date',
'end_date',
'created_at',
'updated_at',
'is_paid:boolean',
'status:boolean',
	),
));*/ ?>

<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo $model->label(2)?></h3>
   </div>
<div class="body" style="z-index: 690; ">

<?php  $form=$this->beginWidget('CActiveForm', array('id'=>'dream-event-grid','enableAjaxValidation'=>false,)); ?>
       
	   <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'id'); ?></span></label> 
              <?php echo $model->id;?>
	         <div class="clear" style="z-index: 670; "></div>
        </div> 
		
	   <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'branch_name'); ?></span></label> 
              <?php echo $model->branch_id;?>
	         <div class="clear" style="z-index: 670; "></div>
        </div>         
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('songs')?></span></label> 
             <?php echo $model->song_ids;?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('name')?></span></label> 
              <?php echo $model->name;?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
               
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'details'); ?></span></label>           
					<?php echo $model->details;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'contact_name'); ?></span></label>           
					<?php echo $model->contact_name;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'contact_no'); ?></span></label>           
					<?php echo $model->contact_no;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'fees'); ?></span></label>           
					<?php echo $model->fees;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'event_total_minute'); ?></span></label>           
					<?php echo $model->event_total_minute;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'is_editing_done'); ?></span></label> 
             <?php echo $model->is_editing_done== "1" ? "Yes": "No"?>
           <div class="clear" style="z-index: 670; "></div>
        </div>  
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'start_date'); ?></span></label>           
					<?php echo $model->start_date;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'end_date'); ?></span></label>           
					<?php echo $model->end_date;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>		
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'is_paid'); ?></span></label> 
             <?php echo $model->is_paid== "1" ? "Yes": "No"?>
           <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'created_at'); ?></span></label>           
					<?php echo $model->created_at;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'updated_at'); ?></span></label>           
					<?php echo $model->updated_at;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
         <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'status'); ?></span></label> 
             <?php echo $model->status== "1" ? "Active": "InActive"?>
           <div class="clear" style="z-index: 670; "></div>
        </div>        
        <?php $this->endWidget();?>
</div>
</div>

