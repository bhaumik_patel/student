<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'bjois-category-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'parent_id',); ?>
		<?php echo $form->textField($model, 'parent_id', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'parent_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'admin_user_id',); ?>
		<?php echo $form->dropDownList($model, 'admin_user_id', GxHtml::listDataEx(AdminUser::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'admin_user_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'name',); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'desc',); ?>
		<?php echo $form->textArea($model, 'desc'); ?>
		<?php echo $form->error($model,'desc'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at',); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at',); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_impression',); ?>
		<?php echo $form->textField($model, 'total_impression', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'total_impression'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_click',); ?>
		<?php echo $form->textField($model, 'total_click', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'total_click'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status',); ?>
		<?php echo $form->textField($model, 'status', array('maxlength' => 1)); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('bjoisAds')); ?></label>
		<?php echo $form->checkBoxList($model, 'bjoisAds', GxHtml::encodeEx(GxHtml::listDataEx(BjoisAd::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->