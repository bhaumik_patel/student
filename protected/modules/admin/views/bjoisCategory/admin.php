<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('bjois-category-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $grid_id = 'bjois-category-grid'; ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => $grid_id,
	'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize',UtilityHtml::getPageSize(),UtilityHtml::getPageSizeArray(),array('class'=>'change-pageSize')) . ' rows per page',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		'parent_id',
		array(
				'name'=>'admin_user_id',
				'value'=>'GxHtml::valueEx($data->adminUser)',
				'filter'=>GxHtml::listDataEx(AdminUser::model()->findAllAttributes(null, true)),
				),
		'name',
		'desc',
		'created_at',
		/*
		'updated_at',
		'total_impression',
		'total_click',
		'status',
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>
<?php Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }});
    });
EOD
,CClientScript::POS_READY); ?>