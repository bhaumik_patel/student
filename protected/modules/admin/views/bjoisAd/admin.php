<?php
$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
		array('label'=>Yii::t('app', 'Import') . ' ' . $model->label(), 'url'=>array('import')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('bjois-ad-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'bjois-ad-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'id',
		array(
				'name'=>'user_id',
				'value'=>'GxHtml::valueEx($data->user)',
				'filter'=>GxHtml::listDataEx(BjoisUser::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'locality_id',
				'value'=>'GxHtml::valueEx($data->locality0)',
				'filter'=>GxHtml::listDataEx(LocalityMaster::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'city_id',
				'value'=>'GxHtml::valueEx($data->city)',
				'filter'=>GxHtml::listDataEx(CityMaster::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'state_id',
				'value'=>'GxHtml::valueEx($data->state)',
				'filter'=>GxHtml::listDataEx(StateMaster::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'category_id',
				'value'=>'GxHtml::valueEx($data->category)',
				'filter'=>GxHtml::listDataEx(BjoisCategory::model()->findAllAttributes(null, true)),
				),
		/*
		'ad_user_type',
		'ad_type',
		'title',
		'description',
		'external_video_code',
		'name',
		'email',
		'mobile',
		'phone',
		'address',
		'locality',
		'price',
		'total_views',
		'total_reply',
		'total_likes',
		'total_spam_report',
		'avg_rate',
		array(
					'name' => 'is_approved',
					'value' => '($data->is_approved === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'created_at',
		'updated_at',
		'approved_at',
		'valid_till',
		'validity_in_minutes',
		array(
					'name' => 'satus',
					'value' => '($data->satus === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>