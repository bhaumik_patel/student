<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'bjois-ad-form',
	'enableAjaxValidation' => false,
));
GxActiveForm
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(BjoisUser::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'user_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'locality_id'); ?>
		<?php echo $form->dropDownList($model, 'locality_id', GxHtml::listDataEx(LocalityMaster::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'locality_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'city_id'); ?>
		<?php echo $form->dropDownList($model, 'city_id', GxHtml::listDataEx(CityMaster::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'city_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'state_id'); ?>
		<?php echo $form->dropDownList($model, 'state_id', GxHtml::listDataEx(StateMaster::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'state_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php echo $form->dropDownList($model, 'category_id', GxHtml::listDataEx(BjoisCategory::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'category_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ad_user_type'); ?>
		<?php echo $form->textField($model, 'ad_user_type', array('maxlength' => 1)); ?>
		<?php echo $form->error($model,'ad_user_type'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'ad_type'); ?>
		<?php echo $form->textField($model, 'ad_type', array('maxlength' => 1)); ?>
		<?php echo $form->error($model,'ad_type'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textArea($model, 'title'); ?>
		<?php echo $form->error($model,'title'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model, 'description'); ?>
		<?php echo $form->error($model,'description'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'external_video_code'); ?>
		<?php echo $form->textArea($model, 'external_video_code'); ?>
		<?php echo $form->error($model,'external_video_code'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model, 'email', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'mobile'); ?>
		<?php echo $form->textField($model, 'mobile', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'mobile'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model, 'phone', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'phone'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textField($model, 'address', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'address'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'locality'); ?>
		<?php echo $form->textField($model, 'locality', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'locality'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model, 'price', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'price'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_views'); ?>
		<?php echo $form->textField($model, 'total_views'); ?>
		<?php echo $form->error($model,'total_views'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_reply'); ?>
		<?php echo $form->textField($model, 'total_reply'); ?>
		<?php echo $form->error($model,'total_reply'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_likes'); ?>
		<?php echo $form->textField($model, 'total_likes'); ?>
		<?php echo $form->error($model,'total_likes'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_spam_report'); ?>
		<?php echo $form->textField($model, 'total_spam_report'); ?>
		<?php echo $form->error($model,'total_spam_report'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'avg_rate'); ?>
		<?php echo $form->textField($model, 'avg_rate'); ?>
		<?php echo $form->error($model,'avg_rate'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'is_approved'); ?>
		<?php echo $form->checkBox($model, 'is_approved'); ?>
		<?php echo $form->error($model,'is_approved'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'approved_at'); ?>
		<?php echo $form->textField($model, 'approved_at'); ?>
		<?php echo $form->error($model,'approved_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'valid_till'); ?>
		<?php echo $form->textField($model, 'valid_till'); ?>
		<?php echo $form->error($model,'valid_till'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'validity_in_minutes'); ?>
		<?php echo $form->textField($model, 'validity_in_minutes'); ?>
		<?php echo $form->error($model,'validity_in_minutes'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('bjoisAdFiles')); ?></label>
		<?php echo $form->checkBoxList($model, 'bjoisAdFiles', GxHtml::encodeEx(GxHtml::listDataEx(BjoisAdFile::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('bjoisAdRatings')); ?></label>
		<?php echo $form->checkBoxList($model, 'bjoisAdRatings', GxHtml::encodeEx(GxHtml::listDataEx(BjoisAdRating::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('bjoisLinkTagsAds')); ?></label>
		<?php echo $form->checkBoxList($model, 'bjoisLinkTagsAds', GxHtml::encodeEx(GxHtml::listDataEx(BjoisLinkTagsAd::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->