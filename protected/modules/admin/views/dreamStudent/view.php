<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'branch',
			'type' => 'raw',
			'value' => $model->branch !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->branch)), array('dreamBranch/view', 'id' => GxActiveRecord::extractPkValue($model->branch, true))) : null,
			),
array(
			'name' => 'currentBatch',
			'type' => 'raw',
			'value' => $model->currentBatch !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->currentBatch)), array('dreamBatch/view', 'id' => GxActiveRecord::extractPkValue($model->currentBatch, true))) : null,
			),
'name',
'contact_no',
'comments',
'inquiry_date',
'created_at',
'updated_at',
'status:boolean',
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentBatches')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->dreamStudentBatches as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('dreamStudentBatch/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentPayments')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->dreamStudentPayments as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('dreamStudentPayment/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>