<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'dream-student-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'branch_id'); ?>
		<?php echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'branch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'current_batch_id'); ?>
		<?php echo $form->dropDownList($model, 'current_batch_id', GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'current_batch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'contact_no'); ?>
		<?php echo $form->textField($model, 'contact_no', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'contact_no'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'comments'); ?>
		<?php echo $form->textArea($model, 'comments'); ?>
		<?php echo $form->error($model,'comments'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'inquiry_date'); ?>
		<?php echo $form->textField($model, 'inquiry_date'); ?>
		<?php echo $form->error($model,'inquiry_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentBatches')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudentBatches', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudentBatch::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentPayments')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudentPayments', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudentPayment::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->