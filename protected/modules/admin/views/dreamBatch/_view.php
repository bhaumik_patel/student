<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('branch_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->branch)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('batch_name')); ?>:
	<?php echo GxHtml::encode($data->batch_name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('detail')); ?>:
	<?php echo GxHtml::encode($data->detail); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('time_from')); ?>:
	<?php echo GxHtml::encode($data->time_from); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('time_to')); ?>:
	<?php echo GxHtml::encode($data->time_to); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('days')); ?>:
	<?php echo GxHtml::encode($data->days); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('registration_fee')); ?>:
	<?php echo GxHtml::encode($data->registration_fee); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('batch_fee')); ?>:
	<?php echo GxHtml::encode($data->batch_fee); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('start_date')); ?>:
	<?php echo GxHtml::encode($data->start_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('end_date')); ?>:
	<?php echo GxHtml::encode($data->end_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('created_at')); ?>:
	<?php echo GxHtml::encode($data->created_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('updated_at')); ?>:
	<?php echo GxHtml::encode($data->updated_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	*/ ?>

</div>