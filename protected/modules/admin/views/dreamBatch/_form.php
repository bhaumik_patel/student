<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'dream-batch-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'branch_id'); ?>
		<?php echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'branch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'batch_name'); ?>
		<?php echo $form->textField($model, 'batch_name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'batch_name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'detail'); ?>
		<?php echo $form->textArea($model, 'detail'); ?>
		<?php echo $form->error($model,'detail'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'time_from'); ?>
		<?php echo $form->textField($model, 'time_from'); ?>
		<?php echo $form->error($model,'time_from'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'time_to'); ?>
		<?php echo $form->textField($model, 'time_to'); ?>
		<?php echo $form->error($model,'time_to'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'days'); ?>
		<?php echo $form->textField($model, 'days', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'days'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'registration_fee'); ?>
		<?php echo $form->textField($model, 'registration_fee', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'registration_fee'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'batch_fee'); ?>
		<?php echo $form->textField($model, 'batch_fee', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'batch_fee'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'start_date'); ?>
		<?php echo $form->textField($model, 'start_date'); ?>
		<?php echo $form->error($model,'start_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'end_date'); ?>
		<?php echo $form->textField($model, 'end_date'); ?>
		<?php echo $form->error($model,'end_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudents')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudents', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentBatches')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudentBatches', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudentBatch::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentPayments')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudentPayments', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudentPayment::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->