<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'dream-monthly-fees-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'fee_id'); ?>
		<?php echo $form->textField($model, 'fee_id'); ?>
		<?php echo $form->error($model,'fee_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'stud_id'); ?>
		<?php echo $form->dropDownList($model, 'stud_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'stud_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->dropDownList($model, 'batch_id', GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'batch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'branch_id'); ?>
		<?php echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'branch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'year'); ?>
		<?php echo $form->textField($model, 'year', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'year'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'jan'); ?>
		<?php echo $form->textField($model, 'jan'); ?>
		<?php echo $form->error($model,'jan'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'feb'); ?>
		<?php echo $form->textField($model, 'feb'); ?>
		<?php echo $form->error($model,'feb'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'mar'); ?>
		<?php echo $form->textField($model, 'mar'); ?>
		<?php echo $form->error($model,'mar'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'apr'); ?>
		<?php echo $form->textField($model, 'apr'); ?>
		<?php echo $form->error($model,'apr'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'may'); ?>
		<?php echo $form->textField($model, 'may'); ?>
		<?php echo $form->error($model,'may'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'jun'); ?>
		<?php echo $form->textField($model, 'jun'); ?>
		<?php echo $form->error($model,'jun'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'jul'); ?>
		<?php echo $form->textField($model, 'jul'); ?>
		<?php echo $form->error($model,'jul'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'aug'); ?>
		<?php echo $form->textField($model, 'aug'); ?>
		<?php echo $form->error($model,'aug'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'sep'); ?>
		<?php echo $form->textField($model, 'sep'); ?>
		<?php echo $form->error($model,'sep'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'oct'); ?>
		<?php echo $form->textField($model, 'oct'); ?>
		<?php echo $form->error($model,'oct'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'nov'); ?>
		<?php echo $form->textField($model, 'nov'); ?>
		<?php echo $form->error($model,'nov'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'dec'); ?>
		<?php echo $form->textField($model, 'dec'); ?>
		<?php echo $form->error($model,'dec'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->