<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('dream-monthly-fees-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php $grid_id = 'dream-monthly-fees-grid'; ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => $grid_id,
	'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize',UtilityHtml::getPageSize(),UtilityHtml::getPageSizeArray(),array('class'=>'change-pageSize')) . ' rows per page',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'fee_id',
		array(
				'name'=>'stud_id',
				'value'=>'GxHtml::valueEx($data->stud)',
				'filter'=>GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'batch_id',
				'value'=>'GxHtml::valueEx($data->batch)',
				'filter'=>GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'branch_id',
				'value'=>'GxHtml::valueEx($data->branch)',
				'filter'=>GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true)),
				),
		'year',
		'jan',
		/*
		'feb',
		'mar',
		'apr',
		'may',
		'jun',
		'jul',
		'aug',
		'sep',
		'oct',
		'nov',
		'dec',
		'created_at',
		'updated_at',
		array(
					'name' => 'status',
					'value' => '($data->status === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		*/
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>
<?php Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }});
    });
EOD
,CClientScript::POS_READY); ?>