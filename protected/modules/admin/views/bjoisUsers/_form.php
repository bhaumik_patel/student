<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'bjois-users-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'firstname',); ?>
		<?php echo $form->textField($model, 'firstname', array('maxlength' => 30)); ?>
		<?php echo $form->error($model,'firstname'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'lastname',); ?>
		<?php echo $form->textField($model, 'lastname', array('maxlength' => 30)); ?>
		<?php echo $form->error($model,'lastname'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'email',); ?>
		<?php echo $form->textField($model, 'email', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'email'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'password',); ?>
		<?php echo $form->passwordField($model, 'password', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'password'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'phone',); ?>
		<?php echo $form->textField($model, 'phone', array('maxlength' => 25)); ?>
		<?php echo $form->error($model,'phone'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'last_login_date',); ?>
		<?php echo $form->textField($model, 'last_login_date'); ?>
		<?php echo $form->error($model,'last_login_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_loggedin',); ?>
		<?php echo $form->textField($model, 'total_loggedin', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'total_loggedin'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'toatl_ads',); ?>
		<?php echo $form->textField($model, 'toatl_ads', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'toatl_ads'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_clicks',); ?>
		<?php echo $form->textField($model, 'total_clicks', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'total_clicks'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_ad_reply',); ?>
		<?php echo $form->textField($model, 'total_ad_reply', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'total_ad_reply'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'toatl_ad_files',); ?>
		<?php echo $form->textField($model, 'toatl_ad_files', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'toatl_ad_files'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_likes',); ?>
		<?php echo $form->textField($model, 'total_likes', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'total_likes'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'total_amount_ads',); ?>
		<?php echo $form->textField($model, 'total_amount_ads', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'total_amount_ads'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'user_roles',); ?>
		<?php echo $form->textField($model, 'user_roles', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'user_roles'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'extra',); ?>
		<?php echo $form->textArea($model, 'extra'); ?>
		<?php echo $form->error($model,'extra'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at',); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at',); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status',); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('bjoisAds')); ?></label>
		<?php echo $form->checkBoxList($model, 'bjoisAds', GxHtml::encodeEx(GxHtml::listDataEx(BjoisAd::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('bjoisAdRatings')); ?></label>
		<?php echo $form->checkBoxList($model, 'bjoisAdRatings', GxHtml::encodeEx(GxHtml::listDataEx(BjoisAdRating::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('bjoisTags')); ?></label>
		<?php echo $form->checkBoxList($model, 'bjoisTags', GxHtml::encodeEx(GxHtml::listDataEx(BjoisTags::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('bjoisUserAddresses')); ?></label>
		<?php echo $form->checkBoxList($model, 'bjoisUserAddresses', GxHtml::encodeEx(GxHtml::listDataEx(BjoisUserAddress::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->