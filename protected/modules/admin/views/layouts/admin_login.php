<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
</head>

<body  class="loginpage">
  <div id="header">
  		<div class="content">
	    	<div id="logo">
      <?php /*?><?php echo CHtml::encode(Yii::app()->name); ?> <?php */?>
      <div class="header-left">
      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-admin.png" alt="" height="30" /> 
      	<span class="main-text">Admin Panel</span>
      </div>
  </div>
		  </div>
  </div>
  <!-- header -->
  <div class="login_content">
  <div class="content">
	  <?php
	    foreach(Yii::app()->admin->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }
		?>
     <?php echo $content; ?> 
     </div>
  </div>
</div>
  <!-- footer -->
  <div id="footer"> Copyright &copy; <?php echo date('Y'); ?> by <?php echo SystemConfig::getValue('site_name')?>. All Rights Reserved.<br/>
  </div>
<!-- page -->
</body>
</html>