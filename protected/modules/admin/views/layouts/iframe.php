<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
</head>
<body>
<div class="container" id="page">
    
  
  <div class="maincont"> 
	  <?php
	    foreach(Yii::app()->user->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }
		?>
    
    <div  class="content_right"> <?php echo $content; ?> 
     <?php if($this->action->id != 'admin' && $this->action->id !='index'): ?>
    	 
    	<a href="javascript:history.back()" class="backlink"> <?php echo Yii::t('app', 'Back');?> </a>
    	<?php endif;?></div>
  </div>
  </div>
  
<!-- page -->
</body>
</html>