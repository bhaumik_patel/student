<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery.js"></script>
</head>
<body>
<div class="container" id="page">
  <div id="header">
    <div id="logo">
      <?php /*?><?php echo CHtml::encode(Yii::app()->name); ?> <?php */?>
      <div class="header-left">
      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-admin.png" alt="" /> 
		<span class="main-text">Admin Panel</span>
      </div> 
      <div class="header-right"><A href="<?php echo CHtml::encode(AdminModule::getLoginUrl())?>"><?php echo CHtml::encode(AdminModule::getLoginText()) ?></A> 
        <?php echo CHtml::encode(AdminModule::getWelcomeText()); ?></div>
    </div>
  </div>
  <!-- header -->  
  <div id="adminmenu" class="jqueryslidemenu">
    <?php  $this->widget('AdminMenu', JVAccessControlFilter::getAdminMenuItems()); ?>
  </div>
  <!-- mainmenu --> 
  <!-- breadcrumbs start -->
  <div class="breadcrumb">
    <?php  $this->widget('application.extensions.inx.AdminBreadcrumbs', array(
		'links'=>$this->breadcrumbs,
	));
		//m($this); //->breadcrumbs->homeLink = 'admin';
	?>
  </div>
  <!-- breadcrumbs Ends -->
  <div class="maincont"> 
	  <?php
	    foreach(Yii::app()->user->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }
		?>
    <div class="content_left">
      <div id="sidebar">
        <?php
			$this->beginWidget('zii.widgets.CPortlet', array(
				'title'=>'Operations',
			));
			$this->widget('zii.widgets.CMenu', array(
				'items'=>$this->menu,
				'htmlOptions'=>array('class'=>'operations'),
			));
			$this->endWidget();
		?>
      </div>
      <!-- sidebar --> 
    </div>
    <div  class="content_right"> <?php echo $content; ?> 
     <?php if($this->action->id != 'admin' && $this->action->id !='index'): ?>
    	 
    	<a href="javascript:history.back()" class="backlink"> <?php echo Yii::t('app', 'Back');?> </a>
    	<?php endif;?></div>
  </div>
  </div>
  <!-- footer -->
  <div id="footer"> Copyright &copy; <?php echo date('Y'); ?> by <?php echo SystemConfig::getValue('site_name')?>. All Rights Reserved.<br/>
  </div>
<!-- page -->
</body>
</html>