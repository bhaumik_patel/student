<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'dream-student-batch-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'student_id'); ?>
		<?php echo $form->dropDownList($model, 'student_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'student_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->dropDownList($model, 'batch_id', GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'batch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'adminssion_date'); ?>
		<?php echo $form->textField($model, 'adminssion_date'); ?>
		<?php echo $form->error($model,'adminssion_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'joining_date'); ?>
		<?php echo $form->textField($model, 'joining_date'); ?>
		<?php echo $form->error($model,'joining_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'registration_fee'); ?>
		<?php echo $form->textField($model, 'registration_fee', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'registration_fee'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'is_paid_registration'); ?>
		<?php echo $form->checkBox($model, 'is_paid_registration'); ?>
		<?php echo $form->error($model,'is_paid_registration'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fee_amount'); ?>
		<?php echo $form->textField($model, 'fee_amount', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'fee_amount'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'paid_amount'); ?>
		<?php echo $form->textField($model, 'paid_amount', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'paid_amount'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->