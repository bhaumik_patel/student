<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('student_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->student)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('batch_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->batch)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('adminssion_date')); ?>:
	<?php echo GxHtml::encode($data->adminssion_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('joining_date')); ?>:
	<?php echo GxHtml::encode($data->joining_date); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('registration_fee')); ?>:
	<?php echo GxHtml::encode($data->registration_fee); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('is_paid_registration')); ?>:
	<?php echo GxHtml::encode($data->is_paid_registration); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('fee_amount')); ?>:
	<?php echo GxHtml::encode($data->fee_amount); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('paid_amount')); ?>:
	<?php echo GxHtml::encode($data->paid_amount); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('created_at')); ?>:
	<?php echo GxHtml::encode($data->created_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('updated_at')); ?>:
	<?php echo GxHtml::encode($data->updated_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	*/ ?>

</div>