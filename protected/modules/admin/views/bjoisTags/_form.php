<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'bjois-tags-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'user_id',); ?>
		<?php echo $form->dropDownList($model, 'user_id', GxHtml::listDataEx(BjoisUser::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'user_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'admin_user_id',); ?>
		<?php echo $form->dropDownList($model, 'admin_user_id', GxHtml::listDataEx(AdminUser::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'admin_user_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'title',); ?>
		<?php echo $form->textField($model, 'title', array('maxlength' => 50)); ?>
		<?php echo $form->error($model,'title'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'notes',); ?>
		<?php echo $form->textArea($model, 'notes'); ?>
		<?php echo $form->error($model,'notes'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at',); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at',); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'approved_at',); ?>
		<?php echo $form->textField($model, 'approved_at'); ?>
		<?php echo $form->error($model,'approved_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status',); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'is_approved',); ?>
		<?php echo $form->checkBox($model, 'is_approved'); ?>
		<?php echo $form->error($model,'is_approved'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('bjoisLinkTagsAds')); ?></label>
		<?php echo $form->checkBoxList($model, 'bjoisLinkTagsAds', GxHtml::encodeEx(GxHtml::listDataEx(BjoisLinkTagsAd::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->