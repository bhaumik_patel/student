<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	Yii::t('app', 'Manage'),
);

/*$this->menu = array(
		array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);*/
$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/question.png'), 'url'=>array('#'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage Branch')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create Branch')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/refresh.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Refresh')),
	);	
	
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('dream-branch-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1-->
<!--p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p-->

<?php $grid_id = 'dream-branch-grid'; ?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => $grid_id,
	'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize',UtilityHtml::getPageSize(),UtilityHtml::getPageSizeArray(),array('class'=>'change-pageSize')) . ' rows per page',
	'dataProvider' => $model->search(),
	'itemsCssClass'=>'tablesorter',
	'filter' => $model,
	'columns' => array(
		'id',
		'branch_name',
		'address',
		'contact_no',
		//'created_at',
		//'updated_at',
		array(
					'name'=>'created_at',
					'filter'=> false, 
			),
		array(
					'name'=>'updated_at',
					'filter'=> false, 
			),
		array(
					'name'=>'status',
					'filter'=> array('1'=>'Active','0'=>'Inactive'), 
					'type' => 'html',
					'value'=> 'CHtml::tag("div",  array("style"=>"text-align: center" ) , CHtml::tag("img", array( "src" => UtilityHtml::getStatusImage(GxHtml::valueEx($data, \'status\')))))',
		),
		/*array(
					'name' => 'status',
					'value' => '($data->status === 0) ? Yii::t(\'app\', \'InActive\') : Yii::t(\'app\', \'Active\')',
					'filter' => array('0' => Yii::t('app', 'InActive'), '1' => Yii::t('app', 'Active')),
					),*/
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'htmlOptions'=>array('width'=>'75px'),
		),
	),
)); ?>
<?php Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }});
    });
EOD
,CClientScript::POS_READY); ?>