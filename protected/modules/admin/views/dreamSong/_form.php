<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo $model->label(2)?></h3>
   <div class="shortcuts-icons" style="z-index: 700; "></div>
   </div>
<div class="body" style="z-index: 690; ">
<?php  $form=$this->beginWidget('CActiveForm', array('id'=>'dream-song-form','enableAjaxValidation'=>false,)); ?>     
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'song_name'); ?></span></label>              
			 <?php echo $form->textField($model, 'song_name', array('maxlength' => 255)); ?>	    	
		   <?php echo $form->error($model,'song_name',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>       
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'status'); ?></span></label>             
			<?php echo $form->dropDownlist($model,'status',array('1'=>'Active','0'=>'InActive'),array('class'=>'uniform')); ?>	     
		    <?php echo $form->error($model,'status',array('class'=>'error_position')); ?>
           <div class="clear" style="z-index: 670; "></div>
        </div>
        
         <div class="button-box" style="z-index: 460;">
				<?php echo GxHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',  array('class'=>'st-button save_continue'))?>	
				<?php echo GxHtml::resetButton(Yii::t('app', 'Reset'),array('class'=>'st-button'));?>
		</div>
        <?php $this->endWidget();?>
</div>
</div>

<?php /* ?>
<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'dream-song-form',
	'enableAjaxValidation' => false,
));
?>
	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'song_name'); ?>
		<?php echo $form->textField($model, 'song_name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'song_name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->
<?php */?>