<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('stud_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->stud)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('batch_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->batch)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('branch_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->branch)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('year')); ?>:
	<?php echo GxHtml::encode($data->year); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('month')); ?>:
	<?php echo GxHtml::encode($data->month); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('amount_paid')); ?>:
	<?php echo GxHtml::encode($data->amount_paid); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('created_at')); ?>:
	<?php echo GxHtml::encode($data->created_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('updated_at')); ?>:
	<?php echo GxHtml::encode($data->updated_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	*/ ?>

</div>