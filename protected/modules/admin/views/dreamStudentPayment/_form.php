<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'dream-student-payment-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'stud_id'); ?>
		<?php echo $form->dropDownList($model, 'stud_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'stud_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'batch_id'); ?>
		<?php echo $form->dropDownList($model, 'batch_id', GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'batch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'branch_id'); ?>
		<?php echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'branch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'year'); ?>
		<?php echo $form->textField($model, 'year'); ?>
		<?php echo $form->error($model,'year'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'month'); ?>
		<?php echo $form->textField($model, 'month'); ?>
		<?php echo $form->error($model,'month'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'amount_paid'); ?>
		<?php echo $form->textField($model, 'amount_paid', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'amount_paid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->