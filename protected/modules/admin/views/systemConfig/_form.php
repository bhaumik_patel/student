<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'system-config-form',
	'enableAjaxValidation' => true,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>
<?php /* 
		<div class="row">
		<?php echo $form->labelEx($model,'system_section_id'); ?>
		<?php echo $form->dropDownList($model, 'system_section_id', GxHtml::listDataEx(SystemSection::model()->findAllAttributes(null, true, 'status=1'))); ?>
		<?php echo $form->error($model,'system_section_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'system_group_id'); ?>
		<?php echo $form->dropDownList($model, 'system_group_id', GxHtml::listDataEx(SystemGroup::model()->findAllAttributes(null, true, 'status=1'))); ?>
		<?php echo $form->error($model,'system_group_id'); ?>
		</div><!-- row -->
*/ ?>
		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<h3><?php echo $model->name; //echo $form->textField($model, 'name', array('maxlength' => 100)); ?></h3>
		<?php //echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'value'); ?>
		<?php echo $form->textArea($model, 'value'); ?>
		<?php echo $form->error($model,'value'); ?>
		</div><!-- row -->
		<?php /* 
		<div class="row">
		<?php echo $form->labelEx($model,'input_type'); ?>
		<?php echo $form->textArea($model, 'input_type'); ?>
		<?php echo $form->error($model,'input_type'); ?>
		</div><!-- row -->
		
		<div class="row">
		<?php echo $form->labelEx($model,'input_options'); ?>
		<?php echo $form->textArea($model, 'input_options'); ?>
		<?php echo $form->error($model,'input_options'); ?>
		</div><!-- row -->
		
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', array('1'=>Yii::t('app','Active'),'0'=>Yii::t('app','Inactive'))); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->
		*/ ?>
		<div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
		<?php echo $form->textField($model, 'position'); ?>
		<?php echo $form->error($model,'position'); ?>
		</div><!-- row -->


<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->
