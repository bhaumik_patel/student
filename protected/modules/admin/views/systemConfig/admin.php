<?php
$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
		array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
		//array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('system-config-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1><?php //echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>
<h1><?php echo Yii::t("app", 'Manage SystemConfigs'); ?></h1>
<p>

<?php echo Yii::t("messages", 'You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.'); ?>
</p>


<?php /*
<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->
*/?>


<?php
$grid_id = 'system-config-grid';
$template = '{update}';
if($this->userType=='admin')
	$template = '{update}'; 
?>
<?php $this->widget('zii.widgets.grid.CGridView', array(

	'id' => $grid_id,

	'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' .CHtml::dropDownList('pageSize', UtilityHtml::getPageSize(), array(5=>5,20=>20,50=>50,100=>100),array('class'=>'change-pageSize')) . ' Rows per page',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(

		
		array(
			'name'=> 'id',
		    'header'=>Yii::t("messages", 'Id'), 
			'value'=>'GxHtml::valueEx($data, \'id\')',
			//'headerHtmlOptions'=> array('style'=> 'width: 35px; margin-right:15px;'),
			),
		array(
				'name'=>'system_section_id',
				'header'=>Yii::t("messages", 'System Section'),
				'value'=>'GxHtml::valueEx($data, \'systemSection\')', 
				'filter'=>GxHtml::listDataEx(SystemSection::model()->findAllAttributes(null, true, 'status=1')),
				),
		array(
				'name'=>'system_group_id',
				'header'=>Yii::t("messages", 'System Group'),
				'value'=>'GxHtml::valueEx($data, \'systemGroup\')', 
				'filter'=>GxHtml::listDataEx(SystemGroup::model()->findAllAttributes(null, true, 'status=1')),
				),
		
		array(
				'name'=>'name',
				'header'=>Yii::t("messages", 'Name'),
				'value'=>'GxHtml::valueEx($data, \'name\')',
				),
	
		array(
				'name'=>'value',
				'header'=>Yii::t("messages", 'Value'),
				'value'=>'GxHtml::valueEx($data, \'value\')',
				),
		array(
			'name'=>'status',
			'type' => 'html',
			'filter'=> UtilityHtml::getStatusArray(),
			'value'=> 'CHtml::tag("div",  array("style"=>"text-align: center" ) , CHtml::tag("img", array( "src" => UtilityHtml::getStatusImage(GxHtml::valueEx($data, \'status\')))))',
		),
		//'input_type',
		/*
		'input_options',
		'status',
		'position',
		*/
		
		array(
			'class' => 'CButtonColumn',
			'template'=>$template,
			
		),
	),
)); ?>

<?php Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }})
    });
EOD
,CClientScript::POS_READY); ?>