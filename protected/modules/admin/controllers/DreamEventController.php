<?php

class DreamEventController extends AdminCoreController {

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);

	}
	public function actionView($id) {
		$this->pageTitle = 'View Event';
		$this->render('view', array(
			'model' => $this->loadModel($id, 'DreamEvent'),
		));
	}

	public function actionCreate() {
		$model = new DreamEvent;
	
		$this->pageTitle = 'Create Event';
		if (isset($_POST['DreamEvent'])) {
			$model->setAttributes($_POST['DreamEvent']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}
		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'DreamEvent');
		$this->pageTitle = 'Update Event';

		if (isset($_POST['DreamEvent'])) {
			$model->setAttributes($_POST['DreamEvent']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'DreamEvent')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('DreamEvent');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new DreamEvent('search');
		$model->unsetAttributes();
		$this->pageTitle = 'Manage Event';
		if (isset($_GET['DreamEvent']))
			$model->setAttributes($_GET['DreamEvent']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}