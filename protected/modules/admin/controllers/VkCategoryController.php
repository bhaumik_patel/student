<?php

class VkCategoryController extends AdminCoreController {

	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);

	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function defaultAccessRules()
	{
		return array(
		array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','admin','delete'),
				'users'=>array('admin'),
		),
		
		);
	}
	public function actionCreate() {
		$model = new VkCategory;


		if (isset($_POST['VkCategory'])) {
			$model->setAttributes($_POST['VkCategory']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('admin'));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'VkCategory');


		if (isset($_POST['VkCategory'])) {
			$model->setAttributes($_POST['VkCategory']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			//only update status to deleted.
			$this->loadModel($id, 'VkCategory')->update(array('status'=>2));

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionAdmin() {
		$model = new VkCategory('search');
		$model->unsetAttributes();

		if (isset($_GET['VkCategory']))
			$model->setAttributes($_GET['VkCategory']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}