<?php
class BjoisUsersController extends AdminCoreController {
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
		);

	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function defaultAccessRules()
	{
		return array(
				array('allow', // allow authenticated user to perform 'create' and 'update' actions
						'actions'=>array('create','update','admin','delete'),
						'users'=>array('admin','superadmin'),
				),

		);
	}
	public function actionCreate() {
		$model = new BjoisUser;

		$model->scenario = 'registerwcaptcha';
		if (isset($_POST['BjoisUser'])) {
			$model->setAttributes($_POST['BjoisUser']);
			if($model->validate()) {
				$model->scenario = NULL;
				if ($model->save()) {
					if (Yii::app()->getRequest()->getIsAjaxRequest())
						Yii::app()->end();
					else
						$this->redirect(array('admin'));
				}
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'BjoisUser');


		if (isset($_POST['BjoisUser'])) {
			$model->setAttributes($_POST['BjoisUser']);

			if ($model->save()) {
				$this->redirect(array('admin'));
			}
		}

		$this->render('update', array(
				'model' => $model,
		));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			//only update status to deleted.
			$this->loadModel($id, 'BjoisUser')->update(array('status'=>2));

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}



	public function actionAdmin() {
		$model = new BjoisUser('search');
		$model->unsetAttributes();

		if (isset($_GET['BjoisUser']))
			$model->setAttributes($_GET['BjoisUser']);

		$this->render('admin', array(
				'model' => $model,
		));
	}

}