<?php

class DreamBatchController extends AdminCoreController {

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);

	}
	public function actionView($id) {
		$this->pageTitle = 'View Batch';
		$this->render('view', array(
			'model' => $this->loadModel($id, 'DreamBatch'),
		));
	}

	public function actionCreate() {
		$model = new DreamBatch;
		$this->pageTitle = 'Create Batch';

		if (isset($_POST['DreamBatch'])) {

		$model->setAttributes($_POST['DreamBatch']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'DreamBatch');
		$this->pageTitle = 'Update Batch';

		if (isset($_POST['DreamBatch'])) {
			$model->setAttributes($_POST['DreamBatch']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'DreamBatch')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('DreamBatch');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new DreamBatch('search');
		$model->unsetAttributes();
		$this->pageTitle = 'Manage Batch';
		if (isset($_GET['DreamBatch']))
			$model->setAttributes($_GET['DreamBatch']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}