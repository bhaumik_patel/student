<?php

Yii::import('application.models.AdminUser.*');

class AdminUserController extends AdminCoreController {
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function defaultAccessRules1() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('view'),
                'users' => array('admin'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'admin', 'delete', 'change'),
                'users' => array('admin'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->pageTitle = 'View Admin User';
        $this->render('view', array(
            'model' => $this->loadModel($id, 'AdminUser'),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new AdminUser();
        $this->pageTitle = Yii::t('app', 'Create ' . $model->label(0));
        if (isset($_POST['AdminUser'])) {

            //$_POST['AdminUser']['created_at']	=	date("Y-m-d H:i:s");

            if (isset($_POST['AdminUser']['password'])) {
                $adminUserObj = new AdminUser();
                $MD5Pwd = $adminUserObj->makeMD5Password($_POST['AdminUser']['password']);
                $_POST['AdminUser']['password'] = $MD5Pwd;
            }

            $model->setAttributes($_POST['AdminUser']);
            if ($model->user_type == 'admin')
                $model->user_roles = '2';
            else if ($model->user_type == 'superadmin')
                $model->user_roles = '1';

            if ($_POST['AdminUser']['email'] != '') {
                $Obj = $model->findByAttributes(array('email' => $_POST['AdminUser']['email']));
                if (isset($Obj->attributes)) {
                    $model->attributes = $_POST['AdminUser'];
                    Yii::app()->admin->setFlash('error', Yii::t("messages", "There is already an account with this email address, please try again.!"));
                    $this->render('create', array('model' => $model,));
                    Yii::app()->end();
                }
            }

            if ($_POST['AdminUser']['username'] != '') {
                $Obj = $model->findByAttributes(array('username' => $_POST['AdminUser']['username']));

                if (isset($Obj->attributes)) {
                    $model->attributes = $_POST['AdminUser'];
                    Yii::app()->admin->setFlash('error', Yii::t("messages", "There is already an account with this User Name, please try again.!"));
                    $this->render('create', array('model' => $model,));
                    Yii::app()->end();
                }
            }

            if ($model->save()) {
                Yii::app()->admin->setFlash('success', Yii::t("messages", "Thank you for your registration.!"));
                $this->redirect(array('admin'));
            }
        }

        $this->render('create', array('model' => $model));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'AdminUser');
        $this->pageTitle = Yii::t('app', 'Update ' . $model->label(0));
        if (isset($_POST['AdminUser'])) {
            if ($model->user_type == 'admin')
                $model->user_roles = '2';
            else if ($model->user_type == 'superadmin')
                $model->user_roles = '1';
            /* if(isset($model->created_at))
              {

              $createdAt=$model->created_at;
              $dateArr=explode("-",$createdAt);

              $y=$dateArr['2'];
              $m=$dateArr['1'];
              $d=$dateArr['0'];
              $cDate=$m."-".$d."-".$y;
              $_POST['AdminUser']['created_at']	=	date("Y-m-d ",strtotime($cDate));
              }
             */
            //$_POST['AdminUser']['updated_at']	=	date("Y-m-d H:i:s");

            $model->setAttributes($_POST['AdminUser']);

            if ($model->save()) {
                Yii::app()->admin->setFlash('success', Yii::t("messages", "Successfully Updated Records.!"));
                $this->redirect(array('admin'));
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if ($id == Yii::app()->admin->id) {
            Yii::app()->admin->setFlash('error', 'You can not delete your account!');
            $this->redirect(array('admin'));
        }
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id, 'AdminUser')->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    /**
     * Lists all models.
     */
    /* public function actionIndex()
      {
      $dataProvider=new CActiveDataProvider('AdminUser');
      $this->render('index',array(
      'dataProvider'=>$dataProvider,
      ));
      } */

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new AdminUser('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['AdminUser']))
            $model->attributes = $_GET['AdminUser'];

        $this->pageTitle = Yii::t('app', 'Manage ' . $model->label(0));
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionChangePass()
    {
        $this->actionChange(Yii::app()->admin->id);
    }
    public function actionChange($id) {

        $model = AdminUser::model()->findByPk((int) $id);
        $this->pageTitle = Yii::t('app', 'Change Password');
        //$_POST['AdminUser']['password']="admin";

        $modelForm = new ChangePasswordForm();
        if (isset($_POST['ChangePasswordForm']['password'])) {
            
            if ((count(CJSON::decode(CActiveForm::validate($modelForm))) > 0)) {
                $this->render('change', array(
                    'model' => $modelForm,
                ));
                Yii::app()->end();
            }
            
            $modelForm->password_repeat = $_POST['ChangePasswordForm']['password_repeat'];
            //print_r($modelForm->password_repeat); die;
            $model->password = md5($modelForm->password_repeat);
            //p($model->password); die;
            $model->save(false);
            if (!$model->hasErrors()) {
                Yii::app()->admin->setFlash('success', "Password change successfully!");
                if(Yii::app()->admin->id==$id && UtilityHtml::getDataByKey(Yii::app()->admin->getState('admin'), 'user_type')!='superadmin') $this->redirect(array('/admin/index'));
                $this->redirect(array('admin'));
            } else {
                Yii::app()->admin->setFlash('error', "Password change failure!");
                if(Yii::app()->admin->id==$id && UtilityHtml::getDataByKey(Yii::app()->admin->getState('admin'), 'user_type')!='superadmin') $this->redirect(array('/admin/index'));
                $this->redirect(array('admin'));
            }
        }
        $this->render('change', array('model' => $modelForm,));
    }

    public function actionUpdateProfile() {
        $id = Yii::app()->admin->id;
        $this->update($id);
    }

    public function update($id) {
        if (Yii::app()->admin->id <> $id) {
            Yii::app()->admin->setFlash('error', Yii::t("messages", "Access denied!"));
            $this->redirect('admin');
        }
        $model = AdminUser::model()->find('id=' . $id);
        if (Yii::app()->admin->id == $id) {
            $this->pageTitle = Yii::t('app', 'Update My Profile');
        } else {
            $this->pageTitle = Yii::t('app', 'Update ' . $model->label(1));
        }

        if (isset($_POST['AdminUser'])) {
            $model->setAttributes($_POST['AdminUser']);
            if ($model->save(false)) {
                Yii::app()->admin->setFlash('success', Yii::t("messages", "Successfully Updated Records.!"));
                $this->redirect(array('/admin/index'));
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

}
