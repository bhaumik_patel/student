<?php

class DreamStudentController extends AdminCoreController {
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);

	}
    public function actionView($id) {
        $this->pageTitle = 'View Student';
        $this->render('view', array(
            'model' => $this->loadModel($id, 'DreamStudent'),
        ));
    }

    public function actionCreate() {
        $model = new DreamStudent;
        $this->pageTitle = 'Create Student';
        $this->performAjaxValidation($model, 'dream-student-form');        
        if (isset($_POST['DreamStudent'])) {
			$model->address = $_POST['DreamStudent']['address'];
            $model->setAttributes($_POST['DreamStudent']);
			
            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    //$this->redirect(array('view', 'id' => $model->id));
					$this->redirect(array('dreamStudentBatch/create', 'sid' => $model->id));
            }
        }
        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'DreamStudent');
        $this->pageTitle = 'Update Student';
        $this->performAjaxValidation($model, 'dream-student-form');

        if (isset($_POST['DreamStudent'])) {
            $model->setAttributes($_POST['DreamStudent']);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
		
			if(DreamStudentPayment::model()->find(array("condition"=>"stud_id =  $id"))){
				Yii::app()->admin->setFlash('error', Yii::t("messages", "Batch Payment Dependency exist!"));
			}else {	
				$this->loadModel($id, 'DreamStudent')->delete();
			}
            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('DreamStudent');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {
        $model = new DreamStudent('search');
        $model->unsetAttributes();
        $this->pageTitle = 'Manage Student';
        if (isset($_GET['DreamStudent']))
            $model->setAttributes($_GET['DreamStudent']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /* public function actionContactList() {
      $this->pageTitle = 'Contact List';
      $model = new DreamStudent;
      $model->searchcontactlist();

      if (isset($_GET['DreamStudentBatch']))
      $model->setAttributes($_GET['DreamStudentBatch']);

      $this->render('contactlist', array(
      'model' => $model,
      ));
      } */

    public function actionAdminBatch() {
        $this->layout = 'iframe';
        $model = new DreamStudent('search');
        $model->unsetAttributes();

        if (isset($_GET['DreamStudent']))
            $model->setAttributes($_GET['DreamStudent']);

        $this->render('admin_batch', array(
            'model' => $model,
        ));
    }

    public function actionDynamicbatches() {
        $data = DreamBatch::model()->findAll('branch_id=:branch_id', array(':branch_id' => (int) $_POST['DreamStudent_branch_id']));

        $data = CHtml::listData($data, 'id', 'batch_name');
        echo CHtml::tag('option', array('value' => '--'), CHtml::encode('Select Batch'), true);
        foreach ($data as $value => $batch_name) {
            echo CHtml::tag('option', array('value' => $value), CHtml::encode($batch_name), true);
        }
    }

}
