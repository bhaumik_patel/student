<?php
/**
 * Enter description here ...
 * @author: Bhaumik PHP Guru
 * @date: Dec 17, 2012  5:58:33 PM
 */
class WikiController extends AdminCoreController {
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);

	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function videoLoad()
	{
		$baseUrl = Yii::app()->baseUrl;
		$uri = $baseUrl.'/css/video-js.css';
		$uri1 = $baseUrl.'/js/video.js';
		$cs=Yii::app()->getClientScript();
		$cs->registerScriptFile($uri1);
	  	$cs->registerCssFile($uri);
	  	return parent::init();
	}
	public function defaultAccessRules()
	{
		return array(
		array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('run','runReverse','admin'),
				'users'=>array('admin'),
		),

		);
	}

	public function actionIndex() {
		$this->videoLoad();
		$model = new WikiCategory('search');
		$model->unsetAttributes();

		$this->pageTitle = "Help";
		if (isset($_GET['WikiCategory']))
		$model->setAttributes($_GET['WikiCategory']);

		$this->render('index', array(
			'model' => $model,
		));
	}
	public function actionVideo()
	{
		$this->videoLoad();
		$this->layout = '';
		$this->renderPartial('video', array(
			
		));
	}
	
	
	public function actionPage(){
		$this->videoLoad();
	 	$this->layout='admin';
		  if(isset($_REQUEST['page_name'])) {
		   $page = $_REQUEST['page_name'];
		   $page_category = WikiCategory::model()->find('category = "'.$page.'"');
		   $page_id = $page_category['id']; 
		   $pageModel = WikiCategoryPage::model()->find('wiki_id = "'.$page_id.'"');
		   if(empty($pageModel)) {
		    $this->forward('Wiki');
		   }
		   $page_name = $pageModel['file_name'];
		   $page_title = explode(".",$page_name);
		   $this->pageTitle = "View"." ".ucfirst($page_title[0])." "."Information";
		   $this->render($page_title[0], array(
			'pageModel'=>$pageModel,
			));
		  }else {
		   $this->forward('Wiki');
		  }
	 }
	
}