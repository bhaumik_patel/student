<?php

class DreamBranchController extends AdminCoreController {

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);

	}
	public function actionView($id) {
		$this->pageTitle = 'View Branch';
		$this->render('view', array(
			'model' => $this->loadModel($id, 'DreamBranch'),
		));
	}

	public function actionCreate() {
		$model = new DreamBranch;
		$this->pageTitle = 'Create Branch';
		$this->performAjaxValidation($model, 'dream-branch-form');

		if (isset($_POST['DreamBranch'])) {
			$model->setAttributes($_POST['DreamBranch']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'DreamBranch');
		$this->pageTitle = 'Update Branch';
		$this->performAjaxValidation($model, 'dream-branch-form');

		if (isset($_POST['DreamBranch'])) {
			$model->setAttributes($_POST['DreamBranch']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'DreamBranch')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('DreamBranch');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new DreamBranch('search');
		$model->unsetAttributes();
		$this->pageTitle = 'Manage Branch';
		if (isset($_GET['DreamBranch']))
			$model->setAttributes($_GET['DreamBranch']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}