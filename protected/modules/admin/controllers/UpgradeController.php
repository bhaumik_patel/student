<?php 
class UpgradeController extends AdminCoreController {
	
	public function filters() {
		return array(
			'accessControl', 
		);
	}

	public function defaultAccessRules()
	{
		return array(
		array('deny',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view'),
				'users'=>array('admin'),
		),
		array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update','admin','minicreate'),
				'users'=>array('admin'),
		),
		);
	}
	public function actionDownload(){
		
	}
}
?>