<?php

class DreamSongController extends AdminCoreController {

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);

	}
	public function actionView($id) {
		$this->pageTitle = "View Song";
		$this->render('view', array(
			'model' => $this->loadModel($id, 'DreamSong'),
		));
	}

	public function actionCreate() {
		$model = new DreamSong;

		$this->pageTitle = "Create Song";
		if (isset($_POST['DreamSong'])) {
			$model->setAttributes($_POST['DreamSong']);

			if ($model->save()) {
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
					$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('create', array( 'model' => $model));
	}

	public function actionUpdate($id) {
		$model = $this->loadModel($id, 'DreamSong');

		$this->pageTitle = "Update Song";
		if (isset($_POST['DreamSong'])) {
			$model->setAttributes($_POST['DreamSong']);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
				'model' => $model,
				));
	}

	public function actionDelete($id) {
		if (Yii::app()->getRequest()->getIsPostRequest()) {
			$this->loadModel($id, 'DreamSong')->delete();

			if (!Yii::app()->getRequest()->getIsAjaxRequest())
				$this->redirect(array('admin'));
		} else
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('DreamSong');
		$this->render('index', array(
			'dataProvider' => $dataProvider,
		));
	}

	public function actionAdmin() {
		$model = new DreamSong('search');
		$model->unsetAttributes();
		$this->pageTitle = "Manage Song";
		if (isset($_GET['DreamSong']))
			$model->setAttributes($_GET['DreamSong']);

		$this->render('admin', array(
			'model' => $model,
		));
	}

}