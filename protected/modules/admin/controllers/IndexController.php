<?php
class IndexController extends AdminCoreController
{
	public $defaultAction = 'index';
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function defaultAccessRules()
	{
		//$rules = parent::accessRules();
	 
		$rules = array(
		array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('login','logout','forgot'),
				'users'=>array('*'),
				'desc'=>'Login and Logout',
		),
		
		array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('admin'),
				'desc'=>'Dashboard',
		),
		array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('superadmin'),
				'desc'=>'Dashboard',
		),
		);
		return $rules;
	}

	public function actionIndex()
	{
		//$this->actionLogin();
		$this->pageTitle = 'Welcome';
		$this->render('welcome');
		//$this->forward('adminUser/admin');
	}
	
	public function actionReport()
	{
		//$this->actionLogin();
		$this->pageTitle = 'Reports';
		$this->render('index');
		//$this->forward('adminUser/admin');
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{	
		$this->layout = 'admin_login';
		/*$exp_date  = strtotime(CustomFunction::getExpireDate());
		$curr_date = strtotime(date('d-m-Y'));
		//$curr_date = date('26-02-2016');
		//echo $curr_date;
		//echo $exp_date; die;
		if($exp_date<$curr_date)
		{
			$this->render('expire');
			exit;
		}	*/
		$model=new AdminLoginForm;
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		// collect user input data
		if(isset($_POST['AdminLoginForm']))
		{
			$model->attributes=$_POST['AdminLoginForm'];
			// validate user input and redirect to the previous page if valid

			if($model->login()) {
				$this->redirect("index");
			}else {
				Yii::app()->admin->setFlash('error','Fail to Authenticate Your Username!');
			}
		}	
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->admin->logout(false);
		$this->redirect(AdminModule::getUrl('home'));
	}
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
			echo $error['message'];
			else
			$this->render('error', $error);
		}
	}
	public function actionChangeSkin()
	{
		$model = SystemConfig::model()->find("name='admin_skin'");
		$allowSkin = array('blue','dark','green','orange','purple','seaGreen');
		$data['success']=0;
		$skin = UtilityHtml::getDataByKey($_POST, 'skin');
		if(in_array($skin, $allowSkin)) {
			$model->value = $skin;
			$model->save(false);
			$data['success']=1;
		}
		echo json_encode($data);
		Yii::app()->end();
	}
}