<?php

class DreamStudentPaymentController extends AdminCoreController {

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function actionView($id) {
        $this->pageTitle = 'View Student Payment';
        $this->render('view', array(
            'model' => $this->loadModel($id, 'DreamStudentPayment'),
        ));
    }

    public function actionCreate() {
        $this->pageTitle = 'Create Student Payment';
        $model = new DreamStudentPayment;


        if (isset($_POST['DreamStudentPayment'])) {
            $model->setAttributes($_POST['DreamStudentPayment']);

            if ($model->save()) {
			//p($model);
				$model->studBatch->next_payment_date = CustomFunction::dateFormatymd($_POST['next_paydate']);
                $model->studBatch->save(false);
				
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id) {
        $this->pageTitle = 'Update Student Payment';
        $model = $this->loadModel($id, 'DreamStudentPayment');


        if (isset($_POST['DreamStudentPayment'])) {
            $model->setAttributes($_POST['DreamStudentPayment']);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id, 'DreamStudentPayment')->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('DreamStudentPayment');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {
        $model = new DreamStudentPayment('search');
        $model->unsetAttributes();
        $this->pageTitle = 'Manage Student Payment';
        if (isset($_GET['DreamStudentPayment']))
            $model->setAttributes($_GET['DreamStudentPayment']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionMonthwiseReport() {
        $model = new DreamStudentPayment('search');
        $model->unsetAttributes();
        $this->pageTitle = 'Month Wise Report';
        if (isset($_GET['DreamStudentPayment']))
            $model->setAttributes($_GET['DreamStudentPayment']);

        $this->render('monthwiseReport', array(
            'model' => $model,
        ));
    }

    public function actionajaxBranchBatch() {
        $result['success'] = 0;
        $studentdata = DreamStudent::model()->findAll(array('condition' => 'id=' . $_POST['stud_id']));
        if ($studentdata) {
            $branchdata = DreamBranch::model()->findAll(array('condition' => 'id=' . $studentdata[0]->branch_id));
            $batchdata = DreamBatch::model()->findAll(array('condition' => 'id=' . $studentdata[0]->current_batch_id));
            $paymentdata = DreamStudentBatch::model()->findAll(array('condition' => 'student_id=' . $_POST['stud_id']));

            $result['branch_id'] = $studentdata[0]->branch_id;
            $result['batch_id'] = $studentdata[0]->current_batch_id;
            $result['branch_name'] = $branchdata[0]->branch_name;
            $result['batch_name'] = $batchdata[0]->batch_name;
            if ($paymentdata) {
                $result['total_fee_amount'] = $paymentdata[0]->fee_amount;
                $result['total_paid_amount'] = $paymentdata[0]->paid_amount;
            }
            $result['success'] = 1;
        }
        echo json_encode($result);
        Yii::app()->end();
    }

}
