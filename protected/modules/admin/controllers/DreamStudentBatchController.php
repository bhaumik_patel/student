<?php

class DreamStudentBatchController extends AdminCoreController {
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
    public function actionView($id) {
        $this->pageTitle = 'View Student Admission';
        $this->render('view', array(
            'model' => $this->loadModel($id, 'DreamStudentBatch'),
        ));
    }

    public function actionCreate() {
        $this->pageTitle = 'Create Student Admission';
        $model = new DreamStudentBatch;
        if ($student_id = Yii::app()->request->getParam('sid')) {
            if (!$student_data = DreamStudent::model()->findByPk($student_id)) {
                $this->redirect('admin');
            }
        }
        if (isset($_POST['DreamStudentBatch'])) {
            $model->setAttributes($_POST['DreamStudentBatch']); 
			/*
			if(!($model->checkUserExist()))
			{
				$model->roll_no = $model->getRollno();
			}else{
				$model->unsetAttributes();
				Yii::app()->admin->setFlash('error', Yii::t("messages", "There is already an admission with this User"));
                $this->render('create', array('model' => $model,));
                Yii::app()->end();
				//flash msg for exist
			}	
			*/		
			if($model->roll_no==0)			$model->roll_no = $model->getRollno();
            if ($model->save()) {
                $model->student->current_batch_id = $model->batch_id;
                $model->student->save(false);
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    //$this->redirect(array('view', 'id' => $model->id));
					$this->redirect(array('dreamStudentPayment/create', 'sbatchid' => $model->id));
            }
        }
		//$model->roll_no = $model->getRollno($student_id);
        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id) {
        $this->pageTitle = 'Update Student Admission';
        $model = $this->loadModel($id, 'DreamStudentBatch');
        if (isset($_POST['DreamStudentBatch'])) {
            $model->setAttributes($_POST['DreamStudentBatch']);

            if ($model->save()) {
                $model->student->current_batch_id = $model->batch_id;
                $model->student->save(false);
                DreamStudentPayment::model()->updatePaymentbatch($model); // update old payment batch id
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id, 'DreamStudentBatch')->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('DreamStudentBatch');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {
        $this->pageTitle = 'Manage Student Admission';
        $model = new DreamStudentBatch('search');
        $model->unsetAttributes();
        if (isset($_GET['DreamStudentBatch']))
            $model->setAttributes($_GET['DreamStudentBatch']);
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /* public function actionContactList() {
      $this->pageTitle = 'Contact List';
      $model = new DreamStudentBatch;
      $model->searchcontactlist();

      if (isset($_GET['DreamStudentBatch']))
      $model->setAttributes($_GET['DreamStudentBatch']);

      $this->render('contactlist', array(
      'model' => $model,
      ));
      } */

    public function actionajaxBatch() {

        $result['success'] = 0;
        if (isset($_POST['DreamStudentBatch'])) {
            $batch = DreamBatch::model()->findByPk(UtilityHtml::getDataByKey($_POST, 'DreamStudentBatch', 'batch_id'));
            if ($batch) {
                $result['data'] = $batch->attributes;
                $result['success'] = 1;
            }
        }
        echo json_encode($result);
        Yii::app()->end();
    }

    public function actionajaxStudentBatch() {
        $result['success'] = 0;
        $result['data'] = '<option value="prompt">Select Batch</option>';
        if (isset($_POST['DreamStudentBatch'])) {
            $studentdata = DreamStudent::model()->findAll(array('condition' => 'id=' . $_POST['DreamStudentBatch']['student_id']));
            if ($studentdata) {
                $data = DreamBatch::model()->findAll('branch_id=:branch_id', array(':branch_id' => (int) $studentdata[0]->branch_id));

                $data = CHtml::listData($data, 'id', 'batch_name');
                foreach ($data as $value => $batch_name) {
                    $result['data'] .= CHtml::tag('option', array('value' => $value), CHtml::encode($batch_name), true);
                }
                $result['success'] = 1;
            }
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    

}
