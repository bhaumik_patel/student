<?php

class UserModule extends CWebModule
{
	public $returnUrl 	= "/buser/index";
	public $homeUrl 	= "/buser/index";
	public $loginUrl 	= "/index/login";
	public $logoutUrl 	= "/buser/logout";
	public $defaultController='myAccount';
	public $defaultAction='index';
	/**
	 * @var string
	 * @desc hash method (md5,sha1 or algo hash function http://www.php.net/manual/en/function.hash.php)
	 */
	public $hash='md5';

	/**
	 * @var boolean
	 * @desc use email for activation user account
	 */
	public $sendActivationMail=true;

	public $loginNotActiv=false;

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
				'user.models.*',
				'application.models.User.*',
				'user.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			$loginAction = TRUE;
			if($controller->id=='index' && $action->id=='login') {
				$loginAction=FALSE;
			}
			if(!Yii::app()->user->id && $loginAction) {
				Yii::app()->controller->redirect(UserModule::getUrl('login'));
			}
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else return false;
	}


	public static function getUrl($type='home')
	{
		switch($type) {
			case 'home':
				return Yii::app()->createUrl('index');
				break;
			case 'return':
				return Yii::app()->createUrl('index');
				break;
			case 'login':
				return Yii::app()->createUrl('user/login');
				break;
			case 'logout':
				return Yii::app()->createUrl('user/logout');
				break;
		}
	}
	public static function getUserData()
	{
		return self::getUser()->attributes; // Yii::app()->user->getState('user');
	}

	public static function getUserRoles()
	{
		// return self::getUserDataByKey('user_roles');
		$role = UserRole::model()->find('role_type = \''.UserModule::getUserDataByKey('user_type').'\'');
		if($role) {
			if($role->parent_id!=0) return implode(',', array($role->id,$role->parent_id));
			else return $role->id;
		}
		else return -1;
	}
	public static function getUserDataByKey()
	{
		$array = func_get_args(); //explode('.', $path);
		$userData = self::getUser()->attributes; //Yii::app()->user->getState('user');
		$str ='';
		$val = $userData;
		foreach($array as $data) {
			$str .= '[\''.$data.'\']';
			if(!isset($val[$data])) return false;
			$val = $val[$data];
		}
		return $val;
	}

	public static function encrypting($string="") {

		$hash = Yii::app()->getModule('user')->hash;
		if ($hash=="md5") 	return md5($string);
		if ($hash=="sha1") 	return sha1($string);
		else 				return hash($hash,$string);
	}


	public static function getLoginUrl()
	{
		if(!Yii::app()->user->id) {
			return Yii::app()->createUrl('/buser/login');
		}else {
			return Yii::app()->createUrl('/buser/logout');
		}
	}
	public static function getRegisterUrl()
	{
		if(!Yii::app()->user->id) {
			return Yii::app()->createUrl('/buser/register');
		}else {
			return Yii::app()->createUrl('/user/myAccount/index');
		}
	}

	public static function getLoginText()
	{
		if(!Yii::app()->user->id) {
			return 'Login';
		}else {
			return 'Logout';
		}
	}

	public static function getFullname()
	{
		if(Yii::app()->user->id) {
			$user = self::getUser();
			return ucwords($user->fullname);
		}else {
			return false;
		}
	}
	public static function getRegisterText()
	{
		if(Yii::app()->user->id) {
			$user = self::getUser();
			return 'My Account';
		}else {
			return 'Register';
		}
	}

	public static function getWelcomeText()
	{
		if(Yii::app()->user->id) {
			$user = self::getUser();
			return 'Hello, '.ucwords($user->firstname);
		}else {
			return false;
		}
	}
	public static function getUser()
	{
		if(Yii::app()->user->id) {
			$criteria = new CDbCriteria();
			//$criteria->select = 't.*, TIMESTAMPDIFF(YEAR, t.dob, CURDATE()) AS age';
			$criteria->addCondition("id=".Yii::app()->user->id);
			return $user = BjoisUser::model()->find($criteria);
		}else {
			return false;
		}
	}
	public static function getInterestedIn()
	{
		if($user = self::getUser()) {
			$str='';
			$interests = array();
			$userInterestObj	=	UserInterest::model()->find("user_id='$user->id'");
			$interestObj		=	Interest::model();
			$interestString		=	"";
			if(isset($userInterestObj->attributes)){
				$interestsArr	=	explode(",",$userInterestObj->interest_ids);
				if(isset($interestsArr)){
					foreach ($interestsArr as $_interest){
						if(!empty($_interest)){
							$data	=	$interestObj->find("id='$_interest'");
							if(isset($data->attributes)){
								$interestString.="<a href='#'> $data->name </a>".",";
							}
						}
					}
				}
			}
			return $interestString;
		}else {
			return false;
		}
	}
	public static function getPhoto($type='icon')
	{
		switch ($type) {
			case 'icon':
				return CHtml::image(Yii::app()->request->baseUrl."/uploads/user/welcome_user.jpg", self::getFullname());
				break;
			default:
				return CHtml::image(Yii::app()->request->baseUrl."/images/welcome_user.jpg", self::getFullname());
				break;
		}
	}
	public static function getAge()
	{
		if(Yii::app()->user->id) {
			$user = self::getUser();
			return $user->age;

		}else {
			return false;
		}
	}

	public function getProfilePhoto()
	{
		if(Yii::app()->user->id){
			$user = self::getUser();
			if(isset($user->photo)){
				return $user->photo;
			}else{
				return false;
			}
		}
	}
	public function getLocation()
	{
		if(Yii::app()->user->id){
			$user = self::getUser();
			if(isset($user->attributes)){
				return $user->attributes;
			}else{
				return false;
			}
		}
	}

	public function calculateProfileProgressBar()
	{
		if(Yii::app()->user->id){
			$user = self::getUser();
			$progressBarCount	=	0;
			if((!empty($user->fullname)) && (!empty($user->email)) && (!empty($user->password))){
				$progressBarCount	=	($progressBarCount + 10);
			}

			if((!empty($user->country)) && (!empty($user->state)) && (!empty($user->city))){
				$progressBarCount	=	($progressBarCount + 20);
			}

			if((!empty($user->phone)) && (!empty($user->gender))){
				$progressBarCount	=	($progressBarCount + 25);
			}
			if(!empty($user->dob)){
				$progressBarCount	=	($progressBarCount + 5);
			}
			if(!empty($user->photo)){
				$progressBarCount	=	($progressBarCount + 10);
			}
			$innterest	=	UserModule::getInterestedIn();
			if(!empty($innterest)){
				$progressBarCount	=	($progressBarCount + 30);
			}
			return $progressBarCount;
		}
	}
}
