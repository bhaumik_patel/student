<?php
class MyAccountController extends UserCoreController
{

	public function actionIndex()
	{
		$cs=Yii::app()->getClientScript();
		$cs->registerScriptFile(Yii::app()->baseUrl.'/js/ckeditor/ckeditor.js');
		$cs->registerScriptFile(Yii::app()->baseUrl.'/js/chosen-master/chosen/chosen.jquery.js');
		$cs->registerCssFile(Yii::app()->baseUrl.'/js/chosen-master/chosen/chosen.css');

		$this->render('index');
	}
	public function actionUploadPhoto()
	{
		Yii::import("ext.EAjaxUpload.qqFileUploader");

		$folder=UtilityHtml::getTempPath();// folder for uploaded files

		$allowedExtensions = array("jpg","jpeg","gif","png");//array("jpg","jpeg","gif","exe","mov" and etc...
		$sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($folder,false);
		$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		if(isset($result['filename'])) {
			$fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
			$fileName=$result['filename'];//GETTING FILE NAME
		}
		echo $return;// it's array
	}
	public function actionCrop()
	{
		p($_REQUEST);
		Yii::import('ext.jcrop.EJCropper');
		$jcropper = new EJCropper();
		$jcropper->thumbPath = UtilityHtml::getTempPath();

		// some settings ...
		$jcropper->jpeg_quality = 95;
		$jcropper->png_compression = 8;

		// get the image cropping coordinates (or implement your own method)
		$coords = $jcropper->getCoordsFromPost('idOrAttributeName');

		// returns the path of the cropped image, source must be an absolute path.
		$thumbnail = $jcropper->crop('/my/images/imageToCrop.jpg', $coords);
	}
	public function actionAjaxSave()
	{
		$resp = BjoisUser::model()->updateUserProfile();
		echo CJSON::encode($resp);
		die;
	}
	public function actionAdList()
	{
		/*
		 $model=new BjoisAd('searchByUser');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BjoisAd']))
			$model->attributes=$_GET['BjoisAd'];

		$this->render('_ad_list',array(
				'model'=>$model,
		));
		*/


		$model =new BjoisAd('searchByUser');
		

		$params =array(
				'model'=>$model,
		);

		if(!isset($_GET['ajax'])) $this->render('_ad_list', $params);
		else  $this->renderPartial('_ad_list', $params);



		//$this->render('_ad_list');
	}

	public function actionAd()
	{
		$id = Yii::app()->getRequest()->getParam('id');
		$this->render('_view_ad', array(
				'model' => $this->loadModel($id, 'BjoisAd'),
		));
	}

	public function actionChangePassword()
	{
		$id = Yii::app()->user->id;
		$model=BjoisUser::model()->findByPk((int)$id);
		$this->pageTitle = Yii::t('app','Change Password');
		//$_POST['AdminUser']['password']="admin";
			
		$modelForm=new ChangePasswordForm();
		if(isset($_POST['ChangePasswordForm']['password']))

		{

			if((count(CJSON::decode(CActiveForm::validate($modelForm)))>0)) {
				$this->render('_change_pwd',array(
						'model'=>$modelForm,
				));
				Yii::app()->end();

			}
			$modelForm->password_repeat = $_POST['ChangePasswordForm']['password_repeat'];
			//print_r($modelForm->password_repeat); die;
			$model->password=md5($modelForm->password_repeat);
			//p($model->password); die;
			$model->save();
			if (!$model->hasErrors()) {
				Yii::app()->user->setFlash('success', "Password change successfully!");
				$this->redirect(array('index'));
			}else {
				Yii::app()->user->setFlash('error', "Password change failure!");
				$this->redirect(array('index'));
			}
		}
		$this->render('_change_pwd',array('model'=>$modelForm,));

	}
}