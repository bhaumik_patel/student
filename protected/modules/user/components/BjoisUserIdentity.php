<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class BjoisUserIdentity extends UserIdentity
{
	private $_id;
	const ERROR_NONE=0;
	const ERROR_EMAIL_INVALID=3;
	const ERROR_STATUS_NOTACTIV=4;
	const ERROR_STATUS_BAN=5;
	const ERROR_PASSWORD_INVALID=6;
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the email and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public $email;

	public function __construct($username,$password)
	{
		$this->username=$username;
		$this->email=$username;
		$this->password=$password;
	}

	public function authenticate()
	{
		$email = $this->username;
		$criteria = new CDbCriteria();
		$criteria->select = "t.*";
		$criteria->addCondition(' (t.`email` = \''.$email.'\') or (t.`mobile` = \''.$email.'\')');
		$user = BjoisUser::model()->find($criteria);
		
		
		//$user=user::model()->findByAttributes(array('email'=>$email),$criteria);

		if($user===null) {
			$this->errorCode=self::ERROR_EMAIL_INVALID;
		} else if(Yii::app()->getModule('user')->encrypting($this->password)!==$user->password) {
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		} else if($user->status>1&&Yii::app()->getModule('user')->loginNotActiv==false) {
			$this->errorCode=self::ERROR_STATUS_NOTACTIV;
		} else if($user->status==2) {
			$this->errorCode=self::ERROR_STATUS_BAN;
		} else {
			$this->_id		= $user->id;
			$this->email	= $user->email;
			$this->username	= $user->email;
			$this->errorCode= self::ERROR_NONE;
			Yii::app()->user->setId($this->_id);
			Yii::app()->user->guestName = $user->email;
			//Yii::app()->user->fullname = $user->fullname;
			//Yii::app()->user->name = strtolower($user->user_type);	
			
			//update log
			$user->total_loggedin = $user->total_loggedin+1;
			$user->last_login_date = new CDbExpression('now()');
			$user->save(false);
			
			$userData = $user->attributes;
			Yii::app()->user->setState('user',$userData);
		}
		return !$this->errorCode;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}
}