<?php
class IndexController extends FrontCoreController
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha'=>array(
						'class'=>'CCaptchaAction',
						'backColor'=>0xFFFFFF,
				),
				// page action renders "static" pages stored under 'protected/views/site/pages'
				// They can be accessed via: index.php?r=site/page&view=FileName
				'page'=>array(
						'class'=>'CViewAction',
				),
		);
	}

	public function actionCatSelect()
	{
		$this->layout = '';
		$this->renderPartial('_cat_select');
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
		$this->render('error');
	}
	public function actionIndex()
	{
		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_desc'), 'description');
		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_keywords'), 'keyword');
		$this->pageTitle = SystemConfig::getValue('site_title');
		$this->render('index');
	}
	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				@mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the support page
	 */
	public function actionSupport()
	{
		$this->render('support');
	}

	public function actionSetCity()
	{
		$gd = new GlobalData();
		$return = $gd->setCookie('city');
		$ar['success']=$return;
		$ar['html']=$this->renderPartial('/ad/search_setting',null,true);
		echo CJSON::encode($ar);
		Yii::app()->end();
	}

	public function actionSetAdType()
	{
		$gd = new GlobalData();
		$return = $gd->setCookie('ad_type');
		$ar['success']=$return;
		$ar['html']=$this->renderPartial('/ad/search_setting',null,true);
		echo CJSON::encode($ar);
		Yii::app()->end();
	}
}