<?php
class BUserController extends FrontCoreController
{

	public function actions() {
		return array(
				'captcha' => array(
						'class' => 'CCaptchaAction',
						'backColor' => 0xFFFFFF,
				),
		);
	}
	public function actionRegister()
	{
		//if(Yii::app()->user->id) $this->redirect('user/myaccount');

		$cs=Yii::app()->getClientScript();
		$cs->registerScriptFile(Yii::app()->baseUrl.'/js/chosen-master/chosen/chosen.jquery.js');
		$cs->registerCssFile(Yii::app()->baseUrl.'/js/chosen-master/chosen/chosen.css');

		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_desc'), 'description');
		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_keywords'), 'keyword');
		$this->pageTitle = SystemConfig::getValue('site_title')." : Registration";
		$model=new RegistrationForm();
		if (isset($_POST['RegistrationForm'])) {
			$response['success']=0;
			$model->setAttributes($_POST['RegistrationForm']);
			if($validate = $model->validate()) {
				$response = BjoisUser::model()->register($model);
				if($response['success']==1) {
					Yii::app()->user->setState('reg_email',$model->email);
					Yii::app()->user->setState('reg_password',$model->password);
					Yii::app()->user->setFlash('success',"Thank you! Your account created successfully!");
					$this->redirect(Yii::app()->createUrl('buser/confirm', array('success'=>$response['success'])));
				}else {
					if(UtilityHtml::getDataByKey($response, 'errors')) {
						foreach($response['errors'] as $msg) {
							Yii::app()->user->setFlash('error', $msg);
						}
					}
					Yii::app()->user->setFlash('error',"Sorry! Your account can not be created!");
				}
			}
		}
		$this->render('register', array('model'=>$model));
	}

	public function actionConfirm()
	{
		$data['success'] = Yii::app()->request->getParam('success');
		$data['resendLink'] = Yii::app()->createUrl('buser/resendVerification');
		$this->render('confirm', $data);
	}
	public function actionResendVerification()
	{
		$data['success']=0;
		if($email = Yii::app()->user->getState('reg_email')) {
			if($model = BjoisUser::model()->find("verify_code='".$code."' AND status='".BjoisUser::STATUS_PENDING_APPROVAL."'")) {
				if($model->sendRegistrationEmail(Yii::app()->user->getState('reg_password'))) {
					$data['success']=1;
					Yii::app()->user->setFlash('success',"Verification link email sent!");
				}
			}
		}
		if($data['success']==0) {
			Yii::app()->user->setFlash('error',"Sorry! Email can not be send!");
		}
		$this->redirect(Yii::app()->createUrl('buser/confirm', $data));
	}
	public function actionVerify()
	{
		$code = Yii::app()->request->getParam('code');
		$data['success'] = 0;
		if($code) {
			if($model = BjoisUser::model()->find("verify_code='".$code."' AND status='".BjoisUser::STATUS_PENDING_APPROVAL."'")) {
				$model->status = BjoisUser::STATUS_ACTIVE;
				$model->verified_at = new CDbExpression('now()');
				$model->save(false);
				Yii::app()->user->setState('reg_email', null);
				$data['success'] = 1;
			}else {
				$data['success'] = 0;
			}
		}
		$this->render('verify',$data);
	}
	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;
		
		
		
		$cs=Yii::app()->getClientScript();
		$cs->registerCssFile(UtilityHtml::getSkinUrl().'css/form.css');
		$cs->registerCssFile(UtilityHtml::getSkinUrl().'css/animate-custom.css');
		

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	public function actionLogout()
	{
		Yii::app()->user->logout(false);
		$this->redirect(UserModule::getUrl('home'));
	}
}