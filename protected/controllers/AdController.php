<?php
class AdController extends FrontCoreController
{
	public function actionIndex()
	{
		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_desc'), 'description');
		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_keywords'), 'keyword');
		$this->pageTitle = SystemConfig::getValue('site_title');
		$this->render('index');
	}

	
	public function actionLeftAjaxCat()
	{
		
	}
	
	public function actionSearch()
	{
		$cs=Yii::app()->getClientScript();
		$cs->registerCssFile(UtilityHtml::getFrontSkinUrl().'/css/list.css');
		
		
		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_desc'), 'description');
		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_keywords'), 'keyword');
		$this->pageTitle = SystemConfig::getValue('site_title');
		
		if(Yii::app()->request->isAjaxRequest) {
			$this->renderPartial('list/listdata');
		}else {
			$this->render('list');
		}
	}

	public function actionCreate()
	{
		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_desc'), 'description');
		Yii::app()->clientScript->registerMetaTag(SystemConfig::getValue('site_meta_keywords'), 'keyword');
		$this->pageTitle = SystemConfig::getValue('site_title');
		$model=new AdPostForm;
		$response['success']=0;
		if (isset($_POST['AdPostForm'])) {
			$model->setAttributes($_POST['AdPostForm']);
			$model->create_account  = UtilityHtml::getDataByKey($_POST, 'AdPostForm','create_account');
			$model->description		= UtilityHtml::getDataByKey($_POST, 'AdPostForm','description');
			$model->photos			= UtilityHtml::getDataByKey($_POST, 'AdPostForm','photos');
			$model->locality		= UtilityHtml::getDataByKey($_POST, 'AdPostForm','locality');
			$model->address		 	= UtilityHtml::getDataByKey($_POST, 'AdPostForm','address');
			$model->tags		 	= UtilityHtml::getDataByKey($_POST, 'AdPostForm','tags');
			if($validate = $model->validate()) {
				$response = BjoisAd::model()->createAd($model);
				if($response['success']==0) {
					foreach($response['errors'] as $errors) {
						foreach($errors as $key=>$msg) {
							Yii::app()->user->setFlash('error', $key.": ".$msg);
						}
					}
					Yii::app()->user->setFlash('error',"Sorry! Ad content can not be post!");
				}
				$this->redirect('posted', array('success'=>$response['success']));
			}
		}else {
			if(Yii::app()->user->id) {
				$model->name = UserModule::getUserDataByKey('firstname');
				$model->email = UserModule::getUserDataByKey('email');
				$model->mobile = UserModule::getUserDataByKey('mobile');
				$model->city_id = UserModule::getUserDataByKey('city_id');
				$model->address = UserModule::getUserDataByKey('address');
				$model->locality = UserModule::getUserDataByKey('locality');
			}
		}
		$this->render('post', array('model'=>$model));
	}

	public function actionPosted()
	{
		$this->render('posted', array('success'=>Yii::app()->request->getParam('success')));
	}
	public function actionUpload()
	{
		Yii::import("ext.EAjaxUpload.qqFileUploader");

		$folder=UtilityHtml::getTempPath();// folder for uploaded files

		$allowedExtensions = array("jpg","jpeg","gif","png");//array("jpg","jpeg","gif","exe","mov" and etc...
		$sizeLimit = 10 * 1024 * 1024;// maximum file size in bytes
		$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
		$result = $uploader->handleUpload($folder,false);
		$return = htmlspecialchars(json_encode($result), ENT_NOQUOTES);

		if(isset($result['filename'])) {
			$fileSize=filesize($folder.$result['filename']);//GETTING FILE SIZE
			$fileName=$result['filename'];//GETTING FILE NAME
		}
		echo $return;// it's array
	}
}