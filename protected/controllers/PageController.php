<?php
class PageController extends FrontCoreController
{
	public function init()
	{
		parent::init();
		
	}
	public function actionView()
	{
		if($view = Yii::app()->request->getParam('name')) {
			if($file= $this->getViewFile($view)) {
				if(file_exists($file)) {
					$this->render($view);
					Yii:app()->end();
					
				}
			}
		}
		$this->render('error');
	}
}