<?php
Yii::import('zii.widgets.jui.CJuiAutoComplete');

class AutoComplete extends CJuiAutoComplete
{
	public $hidden_id='';
	public $hidden_id_value='0';

	/**
	 * Run this widget.
	 * This method registers necessary javascript and renders the needed HTML code.
	 */
	public function run()
	{
		list($name,$id)=$this->resolveNameID();

		if(isset($this->htmlOptions['id']))
			$id=$this->htmlOptions['id'];
		else
			$this->htmlOptions['id']=$id;

		if(isset($this->htmlOptions['name']))
			$name=$this->htmlOptions['name'];

		if($this->hasModel())
			echo CHtml::activeTextField($this->model,$this->attribute,$this->htmlOptions);
		else
			echo CHtml::textField($name,$this->value,$this->htmlOptions);

		echo CHtml::tag('span',array('id'=>'selected_'.$id),'-');
		$close = '<a class=\"close\" onclick=\"js:$(this).parent().hide(); $(\'#'.$id.'\').val(\'\'); $(\'#'.$this->hidden_id.'\').val(\'\'); \">[X]</a>';
		echo CHtml::hiddenField($this->hidden_id,$this->hidden_id_value);

		if($this->sourceUrl!==null)
			$this->options['source']=CHtml::normalizeUrl($this->sourceUrl);
		else
			$this->options['source']=$this->source;
		
		$source = $this->options['source'];
		
		$this->options['sortResults'] = 'false';

		$this->options['select']='js:function( event, ui ) {
				$("#'.$this->hidden_id.'").val(ui.item.id);
				if(ui.item.path) {
					$("#selected_'.$id.'").html(ui.item.path +"'.$close.'");
				}else {
					$("#selected_'.$id.'").html(ui.item.label +"'.$close.'");
				}
				$("#selected_'.$id.'").css("position","absolute");
				$("#selected_'.$id.'").show();
				return false;
	}';
		/*
		$this->options['focus']='js:function( event, ui ) {
				$("#'.$id.'").val(ui.item.label);
				//$("#selected_'.$id.'").html(ui.item.label +"'.$close.'");
				return false;
	}';
	*/
		
		
		$options=CJavaScript::encode($this->options);
		$addtionalJs = 'jQuery(function($){
            $("#selected_'.$id.' a.close").click(function () {
                $("#selected_'.$id.'").hide();
            });
        });';

		$js = "jQuery('#{$id}').autocomplete($options);";
		//$js .= $addtionalJs;
		
		
		$cs = Yii::app()->getClientScript();
		$cs->registerScript(__CLASS__.'#'.$id, $js);
	}
}
