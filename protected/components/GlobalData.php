<?php
class GlobalData
{
	public static function getIniDataRoot()
	{
		return YiiBase::getPathOfAlias('webroot').'/protected/jsonData/';
	}

	/**
	 * @return number
	 * @todo: set the Country based on the local settings
	 */
	public static function getCountryId()
	{
		return 99;
	}
	public static function getAutoCateogryDropDownList($is_json=false)
	{
		$str='';
		$file = 'category_auto_chosen.json';
		if(!file_exists(self::getIniDataRoot().$file)) {
			BjoisCategory::model()->writeDropdownJson($file);
		}
		if(file_exists(self::getIniDataRoot().$file)) {
			$str = file_get_contents(self::getIniDataRoot().$file);
		}
		if($is_json) return $str;
		else {
			return $ar = array_merge(array('0'=>''), objectToArray(UtilityHtml::json_decode($str)));
		}
	}
	public static function getAutoCityDropDownList($is_json=false) {
		$ar = GxHtml::listDataEx(CityMaster::model()->findAllConutryCity());
		$ar['0'] = '';
		asort($ar);
		return $ar;
	}

	public function setCookie($fld)
	{
		if($param = Yii::app()->request->getParam($fld)) {
			$cookie = new CHttpCookie('bjois_'.$fld, $param);
			$cookie->expire = time()+60*60*24*180;
			Yii::app()->request->cookies[$fld] = $cookie;
			return true;
		}else {
			return false;
		}
	}
	public static function getCookie($fld)
	{
		if($val = Yii::app()->request->getParam($fld)) {
			return (string) $val;
		}
		if($val = Yii::app()->request->cookies['bjois_'.$fld]) {
			return (string) $val;
		}
		return false;
	}

	public static function getAdUserTypeList()
	{
		return array(1=>'Individual',2=>'Business',9=>'Other');
	}
	public static function getAdTypeList()
	{
		return array(1=>'Sell',2=>'Buy',3=>'Offer',4=>'Requierment', 9=>'Other');
	}

	public static function getCity($id=0)
	{
		if($res = CityMaster::model()->findByPK($id)){
			return $res->name;
		}
		return false;
	}
	
	public static function getCategoryName($id=0)
	{
		if($res = BjoisCategory::model()->findByPK($id)){
			return $res->name;
		}
		return false;
	}
	

	public static function getAdType($id=0)
	{
		$res = self::getAdTypeList();
		if(isset($res[$id])){
			return $res[$id];
		}
		return false;
	}
}