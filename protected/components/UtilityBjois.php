<?php

class UtilityBjois extends CHtml
{
	public static function getAdtypes()
	{
		return array(1=>'Buy',2=>'Sell');
	}
	public static function getAdType($id)
	{
		$types = self::getAdtypes();
		return UtilityHtml::getDataByKey($types,$id);
	}
	public static function getSubDesc($text, $length=300, $dots=true)
	{
		$text = trim(preg_replace('#[\s\n\r\t]{2,}#', ' ', $text));
		$text_temp = $text;
		while (substr($text, $length, 1) != " ") { $length++; if ($length > strlen($text)) { break; } }
		$text = substr($text, 0, $length);
		return $text . ( ( $dots == true && $text != '' && strlen($text_temp) > $length ) ? '...' : '');
	}
	
	public static function getTopSearchTerms()
	{
		$ar =array();
		if($res =  BjoisSearchKeyword::model()->getAllSearchLog()) {
			foreach($res as $row) {
				$ar[] = $row->keyword;
			}
			return $ar;
		}
		return $ar;
	}
}