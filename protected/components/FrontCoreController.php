<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */

class FrontCoreController extends GxController
{
	public $layout='main';

	public function init()
	{
		parent::init();
		$theme = SystemConfig::getValue('front_theme');
		Yii::app()->setTheme($theme);


		$cs=Yii::app()->getClientScript();
		if($theme=='orange') {
			//$cs->registerScriptFile(UtilityHtml::getFrontSkinUrl().'/js/jquery-1.3.2.min.js');
			$cs->registerScriptFile(Yii::app()->baseUrl.'/js/jquery1.8.2.js');
			$cs->registerScriptFile(UtilityHtml::getFrontSkinUrl().'/js/jquery.easing.1.3.js');
			//$cs->registerScriptFile(UtilityHtml::getFrontSkinUrl().'/js/jquery.timers-1.2.js"');
			$cs->registerScriptFile(UtilityHtml::getFrontSkinUrl().'/js/jquery.dualSlider.0.3.min.js');
			$cs->registerScriptFile(UtilityHtml::getFrontSkinUrl().'/js/slimbox2.js');
			$cs->registerScriptFile(UtilityHtml::getFrontSkinUrl().'/js/ddsmoothmenu.js');




			$cs->registerCssFile(UtilityHtml::getFrontSkinUrl().'/css/templatemo_style.css');
			$cs->registerCssFile(UtilityHtml::getFrontSkinUrl().'/css/ddsmoothmenu.css');
			$cs->registerCssFile(UtilityHtml::getFrontSkinUrl().'/css/jquery.dualSlider.0.2.css');

			$cs->registerCssFile(Yii::app()->baseUrl.'/css/dev.css');
			$cs->registerCssFile(Yii::app()->baseUrl.'/css/form.css');


		} else if($theme=='blue') {
				
			$cs->registerCssFile(UtilityHtml::getFrontSkinUrl().'/css/main.css');
			$cs->registerCssFile(UtilityHtml::getFrontSkinUrl().'/css/jquery.selectbox.css');
			$cs->registerCssFile(UtilityHtml::getFrontSkinUrl().'/css/metro_main.css');


			//$cs->registerScriptFile(UtilityHtml::getFrontSkinUrl().'/js/jquery-1.7.2.min.js');
			$cs->registerScriptFile(UtilityHtml::getFrontSkinUrl().'/js/jquery.selectbox-0.2.js');
			
			$cs->registerScriptFile(Yii::app()->baseUrl.'/js/ckeditor/ckeditor.js');
			$cs->registerScriptFile(Yii::app()->baseUrl.'/js/chosen-master/chosen/chosen.jquery.js');
			$cs->registerCssFile(Yii::app()->baseUrl.'/js/chosen-master/chosen/chosen.css');
			
			//echo Yii::app()->bootstrap->registerCoreCss();
			
		}
		
	}

}