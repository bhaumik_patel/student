<?php
return array(
	'error' => array(
		'prob_save_order_log' 	 		=> 'Problem to save order info',
		'prob_retrive_order_log' 		=> 'Problem to retrieve order info',
		'not_valid_bridge_config'		=> 'Bridge Configuration is not valid',
		'profile_not_found'      		=> 'Profile not found',

		// bridge configuration error messages 
		
		'bc_step_init_step'				=> 'Bridge Configuration initiated failed',
		'bc_step_configuration_step'	=> 'Bridge Configuration validated failed',
		'bc_step_get_store_info_step'	=> 'Store information retrieved failed',
		'bc_step_get_dept_info_step'	=> 'Department information retrieved failed',
		'bc_step_get_entity_info_step'	=> 'Entity information retrieved failed',
		'bc_step_get_attribute_info_step'=>'Attribute information retriving failed',
		'bc_step_get_demodata_info_step'=> 'prepare demo data failed',
		'bc_step_get_delivery_info_step'=> 'Delivery methods information retrieved failed',
		'bc_step_get_payment_info_step'	=> 'Payment methods information retrieving failed',
		'bc_step_get_textrate_info_step'=> 'TaxRate methods information retrieving failed',
		'bc_step_complete_step'			=> 'Bridge Configuration process finalizing failed',

	),
	'success' =>array(),
	'info' =>array(
		'total_orders'  				=> 'Total Orders:',
		'total_retrieved_orders'  		=> 'Total Retrived Orders:',
		'total_processed_orders'  		=> 'Total Processed Orders:',
	
		'total_orders_pre'				=> 'Retriving Total Orders..',
		'total_retrieved_orders_pre'	=> 'Retriving Orders..',
		'total_processed_orders_pre'	=> 'Processing Orders..',
	
		//bridge configuration step POST message
		'bc_step_init'					=> 'Bridge Configuration initiated',
		'bc_step_connect'				=> 'Bridge Configuration validated',
		'bc_step_get_store_info'		=> 'Store information  retrieved',
		'bc_step_get_dept_info'			=> 'Department information  retrieved',
		'bc_step_get_entity_info'		=> 'Entity information retrieved',
		'bc_step_get_attribute_info'	=> 'Attribute information retrieved',
		'bc_step_get_demodata_info'		=> 'prepare demo data',
		'bc_step_get_delivery_info'		=> 'Delivery methods information retrieved',
		'bc_step_get_payment_info'		=> 'Payment methods information retrieved',
		'bc_step_get_textrate_info'		=> 'TaxRate methods information retrieving',
		'bc_step_complete'				=> 'Bridge Configuration process completed',
	
		//bridge configuration step Pre message
		'bc_step_init_pre'				=> 'Bridge Configuration process starts',
		'bc_step_connect_pre'			=> 'Bridge Configuration start to check connection',
		'bc_step_get_store_info_pre'	=> 'Store information retrieving',
		'bc_step_get_dept_info_pre'		=> 'Department information retrieving',
		'bc_step_get_entity_info_pre'	=> 'Entity information retrieving',
		'bc_step_get_attribute_info_pre'=> 'Attribute information retriving',
		'bc_step_get_demodata_info_pre'	=> 'Demo data preparing',
		'bc_step_get_delivery_info_pre'	=> 'Delivery methods information retrieving',
		'bc_step_get_payment_info_pre'	=> 'Payment methods information retrieving',
		'bc_step_get_textrate_info_pre'	=> 'TaxRate methods information retrieving',
		'bc_step_complete_pre'			=> 'Bridge Configuration process finalizing',
	
	),
);