<?php
/**
 * VikingBro
 * @author: Bhaumik PHP Guru
 * @date: Dec 19, 2012  2:49:26 PM
 */
class UtilityMessage
{
	public static function getMessage($key, $type='error')
	{
		$array=require (Yii::app()->basePath.'/components/Messages.php');
		if($msg = UtilityHtml::getDataByKey($array, $type, $key)) {
			$msg = Yii::t('vk', $msg);
		}
		return $msg;
	}
}
 