<?php
Yii::import('zii.widgets.grid.CGridView');
class BGridView extends CGridView {
	public $isRender = false;  // If the configuration sets 'isRender' to true, then rendering is 'forced'.

	public function init() {
		/* @var $request CHttpRequest */
		$request = Yii::app()->request;
		$ajax    = $request->getParam($this->ajaxVar,null);
		if($ajax!==null) {
			$this->isRender |= array_search($this->getId(),explode(',',$ajax))!==false;
		} else {
			$this->isRender = true;
		}
		return parent::init();
	}

	public function run() {
		if($this->isRender) {
			return parent::run();
		} else {
			// Unfortunately we have to render the grid to make sure that he
			// 'id' numbering works.
			ob_start();
			ob_implicit_flush(false);
			parent::run();
			ob_get_clean();                 return null;           return null;
		}
	}
}