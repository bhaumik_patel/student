<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
require_once(dirname(__FILE__).'/db.php');
require_once(dirname(__FILE__).'/ftp.php');

Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
$configArray =  array(
		'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
		'name'=>'DWDA',
		//'theme'=>'admin_dark',

		'preload'=>array('log'),
		'defaultController'=>'admin/index',
		// autoloading model and component classes
		
		'aliases' => array(
			
			// yiistrap configuration
			'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'), // change if necessary
			// yiiwheels configuration
			'yiiwheels' => realpath(__DIR__ . '/../extensions/yiiwheels'), // change if necessary
		),
		'import'=>array(
				'application.models.*',
				'application.models.User.*',
				'application.web.*',
				'application.components.*',
				'application.modules.user.*',
				'application.models.form.*',
				'application.modules.user.components.*',
				'ext.giix-components.*', // giix components
				'ext.slajaxtabs.*', // giix components
				'application.modules.rights.*',
				'application.modules.rights.components.*', // Correct paths if necessary.
				'application.modules.admin.*',
				'application.modules.admin.components.*',
				'application.extensions.fckeditor.FCKEditorWidget.*',
				'application.helpers.*',
				'application.extensions.inx.*',
				'application.extensions.sftp.*',
				'application.extensions.AutoComplete.*',
				'application.extensions.EAjaxUpload.*',
				'application.extensions.infiniteScroll.*',
				'application.extensions.MTreeView.*',
				'bootstrap.helpers.TbHtml',
				


				//'ext.eHttpClient.*',
				//oAuth extension
				//'ext.eoauth.*',
				//'ext.eoauth.lib.*',
		),

		'modules'=>array(
				'gii'=>array(
						'class'=>'system.gii.GiiModule',
						'password'=>'admin',
						'generatorPaths' => array(
								'ext.giix-core', // giix generators
						),
						'ipFilters'=>array('127.0.0.1','::1'),
				),
				'rights'=>array( 'install'=>true, // Enables the installer.
				),
				'admin',
				'user',
				'api',

				// uncomment the following to enable the Gii tool
			/*
			 'gii'=>array(
					'class'=>'system.gii.GiiModule',
					'password'=>'Enter Your Password Here',
			 ),
			*/
		),
		'sourceLanguage'    =>'en_US',
		'language'          =>'en',
		// application components
		'components'=>array(
				'widgetFactory'=>array(
						'enableSkin'=>true,
				),

				/*
				/*
				'bootstrap'=>array(
					'class'=>'bootstrap.components.Bootstrap',
				),
				*/
				
				// yiistrap configuration
				'bootstrap' => array(
					'class' => 'bootstrap.components.TbApi',
				),
				// yiiwheels configuration
				'yiiwheels' => array(
					'class' => 'yiiwheels.YiiWheels',   
				),
				'admin'=>array(
						'allowAutoLogin'=>true,
						'autoUpdateFlash'=>true,
						'class'=>'AdminRWebUser',
						'loginUrl' => array('/admin/login'),
						'fullname' => '',
				),

				/*
				 'customer'=>array(
				 		'allowAutoLogin'=>true,
				 		'autoUpdateFlash'=>true,
				 		'class'=>'CustomerRWebUser',
				 		'loginUrl' => array('/customer/login'),
				 ),
*/
				'user'=>array(
						'allowAutoLogin'=>true,
						'autoUpdateFlash'=>true,
						'class'=>'UserRWebUser',
						'class'=>'RWebUser', //k
						'loginUrl' => array('/user/login'),
				),
				'authManager'=>array( //k
						'class'=>'RDbAuthManager',     // Provides support authorization item sorting.
				),
				'sftp'=>$ftpConfig,
				'zip'=>array(
						'class'=>'application.extensions.Zip.EZip',
				),

				'email'=>array(
						'class'=>'application.extensions.email.Email',
						'delivery'=>'php', //Will use the php mailing function.
						//May also be set to 'debug' to instead dump the contents of the email into the view
				),
				// uncomment the following to enable URLs in path-format

				'urlManager'=>array(
						//'useStrictParsing'=>true,
						'urlFormat'=>'path',
						'showScriptName'=>false,
						'rules'=>array(
									
								'register'			=> 'buser/register',
								'login'				=> 'buser/login',

								'admin/index'		=>'admin/index/index',
								'admin/login'		=>'admin/index/login',
								'admin/logout'		=>'admin/index/logout',
								'admin/<controller:\w+>/<action:\w+>'=>'admin/<controller>/<action>',
								'gii/<controller:\w+>/<action:\w+>'=>'gii/<controller>/<action>',

								//'ad/search/<\w+>' => 'ad/search/?cat_id/</d+>/name/<\w+>',
								
								'<controller:\w+>/<id:\d+>'=>'<controller>/view',
								'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
								'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
								
								'user/<controller:\w+>/<id:\d+>'=>'user/<controller>/view',
								'user/<controller:\w+>/<action:\w+>/<id:\d+>'=>'user/<controller>/<action>',
								'user/<controller:\w+>/<action:\w+>'=>'user/<controller>/<action>',
								
								
								/*
								 'user/'				=>'user/index/index',
'user/login'		=>'user/index/login',
'user/logout'		=>'user/index/logout',
'user/<controller:\w+>/<action:\w+>'=>'user/<controller>/<action>',
'<controller:\w+>/<id:\d+>'=>'<controller>/view',
'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
*/
						),
				),

				// uncomment the following to use a MySQL database
//'db'=>getDbConfig(),

				'errorHandler'=>array(
						// use 'site/error' action to display errors
						'errorAction'=>'index/error',
				),
				'db'=>$dbConfig,
				'log'=>array(
						'class'=>'CLogRouter',
						'routes'=>array(
								array(
										'class'=>'CFileLogRoute',
										'levels'=>'error, warning',
								),
								array( 'class'=>'CProfileLogRoute','report'=>'summary',),
								array('class'=>'ext.yii-debug-toolbar.YiiDebugToolbarRoute','ipFilters'=>array('127.0.0.1'),),
									
								// uncomment the following to show log messages on web pages
								//array('class'=>'CWebLogRoute',),

						),
				),

				'CURL' =>array(
						'class' => 'application.extensions.curl.Curl',
						//you can setup timeout,http_login,proxy,proxylogin,cookie, and setOPTIONS

/*'setOptions'=>array(
 CURLOPT_SSL_VERIFYPEER => false,
),
*/
				),
				'image'=>array(
						'class'=>'application.extensions.image.CImageComponent',
						// GD or ImageMagick
						'driver'=>'GD',
						// ImageMagick setup path
						'params'=>array('directory'=>'/opt/local/bin'),
				),
		),

		// application-level parameters that can be accessed
		// using Yii::app()->params['paramName']
		'params'=>array(
				// this is used in contact page
				'adminEmail'=>'',
		),
);


//p($configArray);
return $configArray;