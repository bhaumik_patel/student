<?php

/**
 * AdPostForm class.
 * AdPostForm is the data structure for keeping
 * AdPostForm
 */

class AdPostForm extends CFormModel
{
	public $category_id;
	public $title;
	public $ad_user_type;
	public $ad_type;
	public $description;
	public $price;
	public $photos;


	public $name;
	public $email;
	public $create_account;
	public $password;

	public $mobile;
	public $phone;
	public $city;
	public $locality;

	public $city_id;
	public $state_id;
	public $locality_id;
	public $zipcode;
	public $address;
	public $tags;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
				// name, email, subject and body are required
				array('category_id, ad_user_type, ad_type, name, email, mobile, title, city_id', 'required'),
				// email has to be a valid email address
				array('email', 'email'),
				array('category_id, city_id', 'numerical', 'integerOnly'=>true, 'min'=>1, 'tooSmall'=>'{attribute} can not be blank'),
				array('price', 'priceValidate'),
				array('mobile', 'PcSimplePhoneValidator', 'minNumDigits'=>'10' , 'maxNumDigits'=>'10'),
				array('phone', 'PcSimplePhoneValidator', 'allowEmpty'=>true ),
				array('password', 'length', 'max'=>128,'min'=>5),
				array('locality_id', 'numerical', 'integerOnly'=>true),
				array('address',  'safe', 'on'=>'search'),
				// verifyCode needs to be entered correctly
				//array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
				'category_id'=> Yii::t('b','Category'),
				'title'=> Yii::t('b','Ad Title'),
				'ad_user_type'=> Yii::t('b','I am'),
				'ad_type'=> Yii::t('b','I want to'),
				'description'=> Yii::t('b','Description'),
				'price'=> Yii::t('b','Price'),
				'Photos'=> Yii::t('b','Upload Photos'),
				'tags'=> Yii::t('b','Tags'),

				'name'=> Yii::t('b','Name'),
				'email'=> Yii::t('b','Email'),
				'create_account'=> Yii::t('b','Do you want FREE account?'),


				'Mobile'=> Yii::t('b','Mobile Number'),
				'phone'=> Yii::t('b','Phone'),
				'city_id'=> Yii::t('b','City'),
				'locality_id'=> Yii::t('b','Locality'),
				'address'=> Yii::t('b','Address'),
				//'verifyCode'=>'Verification Code',
		);
	}
	public function priceValidate($attribute)
	{
		// 12,10 OR 12.10
		$pattern = '/^[��$�Rs]?[ ]?[-]?[ ]?[0-9]*[.,]{0,1}[0-9]{0,2}[ ]?[��$�]?$/';
	}
	public function getAdUserTypeList()
	{
		return GlobalData::getAdUserTypeList();
	}
	public function getAdTypeList()
	{
		return GlobalData::getAdTypeList();
	}
}