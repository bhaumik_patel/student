<?php

Yii::import('application.models._base.BaseDreamBranch');

class DreamBranch extends BaseDreamBranch {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function rules() {
        return array(
            array('branch_name, address, contact_no', 'required'),
            array('status', 'numerical', 'integerOnly' => true),
            array('branch_name', 'length', 'max' => 60),
            //array('contact_no', 'length', 'max'=>15),
            array('contact_no', 'match', 'pattern' => '/^([0-9\(\)\/\+ \-]*)$/'),
            array('created_at, updated_at', 'safe'),
            array('id, created_at, updated_at, status', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, branch_id, branch_name, address, contact_no, created_at, updated_at, status', 'safe', 'on' => 'search'),
        );
    }

    public function beforeSave() {
        if ($this->isNewRecord)
            $this->created_at = new CDbExpression('NOW()');

        $this->updated_at = new CDbExpression('NOW()');

        return parent::beforeSave();
    }

    public static function label($n = 1) {
        return Yii::t('app', 'Branch|Branch', $n);
    }

    public static function getBranchField($form, $model, $value) {
        if (AdminModule::getUserDataByKey('user_type') == 'superadmin') {
            $batches = GxHtml::listDataEx(DreamBranch::model()->findAll("t.`status`=1 OR t.id='$model->branch_id'"));
            echo $form->dropDownList($model, 'branch_id', $batches, array('empty' => Yii::t('fim', 'Select Branch')));
        } else {
            echo '<span>' . AdminModule::getLoggedInUser()->branch . '</span>' . $form->hiddenField($model, 'branch_id', array('value' => AdminModule::getUserDataByKey('branch_id')));
        }
    }

}
