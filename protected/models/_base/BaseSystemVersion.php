<?php

/**
 * This is the model base class for the table "system_version".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SystemVersion".
 *
 * Columns in table "system_version" available as properties of the model,
 * followed by relations of table "system_version" available as properties of the model.
 *
 * @property string $id
 * @property string $version
 * @property string $description
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_current
 * @property integer $status
 *
 * @property SystemBackup[] $systemBackups
 */
abstract class BaseSystemVersion extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'system_version';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'SystemVersion|SystemVersions', $n);
	}

	public static function representingColumn() {
		return 'version';
	}

	public function rules() {
		return array(
			array('is_current, status', 'numerical', 'integerOnly'=>true),
			array('version', 'length', 'max'=>50),
			array('description, notes, created_at, updated_at', 'safe'),
			array('version, description, notes, created_at, updated_at, is_current, status', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, version, description, notes, created_at, updated_at, is_current, status', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'systemBackups' => array(self::HAS_MANY, 'SystemBackup', 'version_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'version' => Yii::t('app', 'Version'),
			'description' => Yii::t('app', 'Description'),
			'notes' => Yii::t('app', 'Notes'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'is_current' => Yii::t('app', 'Is Current'),
			'status' => Yii::t('app', 'Status'),
			'systemBackups' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('version', $this->version, true);
		$criteria->compare('description', $this->description, true);
		$criteria->compare('notes', $this->notes, true);
		$criteria->compare('created_at', $this->created_at, true);
		$criteria->compare('updated_at', $this->updated_at, true);
		$criteria->compare('is_current', $this->is_current);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination'=>array('pageSize'=>UtilityHtml::getPageSize())
		));
	}
}