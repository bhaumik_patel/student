<?php

/**
 * This is the model base class for the table "dream_student_payment".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "DreamStudentPayment".
 *
 * Columns in table "dream_student_payment" available as properties of the model,
 * followed by relations of table "dream_student_payment" available as properties of the model.
 *
 * @property string $id
 * @property string $stud_id
 * @property string $batch_id
 * @property string $branch_id
 * @property string $stud_batch_id
 * @property string $pay_date
 * @property integer $year
 * @property string $month
 * @property string $amount_paid
 * @property string $registration_paid
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 *
 * @property DreamBatch $batch
 * @property DreamBranch $branch
 * @property DreamStudent $stud
 * @property DreamStudentBatch $studBatch
 */
abstract class BaseDreamStudentPayment extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'dream_student_payment';
	}

	public static function label($n = 1) {
		return Yii::t('app', 'DreamStudentPayment|DreamStudentPayments', $n);
	}

	public static function representingColumn() {
		return 'created_at';
	}

	public function rules() {
		return array(
			array('created_at', 'required'),
			array('year, status', 'numerical', 'integerOnly'=>true),
			array('stud_id, batch_id, branch_id, stud_batch_id, amount_paid, registration_paid', 'length', 'max'=>10),
			array('month', 'length', 'max'=>50),
			array('pay_date, updated_at', 'safe'),
			array('stud_id, batch_id, branch_id, stud_batch_id, pay_date, year, month, amount_paid, registration_paid, updated_at, status', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, stud_id, batch_id, branch_id, stud_batch_id, pay_date, year, month, amount_paid, registration_paid, created_at, updated_at, status', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
			'batch' => array(self::BELONGS_TO, 'DreamBatch', 'batch_id'),
			'branch' => array(self::BELONGS_TO, 'DreamBranch', 'branch_id'),
			'stud' => array(self::BELONGS_TO, 'DreamStudent', 'stud_id'),
			'studBatch' => array(self::BELONGS_TO, 'DreamStudentBatch', 'stud_batch_id'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'stud_id' => null,
			'batch_id' => null,
			'branch_id' => null,
			'stud_batch_id' => null,
			'pay_date' => Yii::t('app', 'Pay Date'),
			'year' => Yii::t('app', 'Year'),
			'month' => Yii::t('app', 'Month'),
			'amount_paid' => Yii::t('app', 'Amount Paid'),
			'registration_paid' => Yii::t('app', 'Registration Paid'),
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'status' => Yii::t('app', 'Status'),
			'batch' => null,
			'branch' => null,
			'stud' => null,
			'studBatch' => null,
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('stud_id', $this->stud_id);
		$criteria->compare('batch_id', $this->batch_id);
		$criteria->compare('branch_id', $this->branch_id);
		$criteria->compare('stud_batch_id', $this->stud_batch_id);
		$criteria->compare('pay_date', $this->pay_date, true);
		$criteria->compare('year', $this->year);
		$criteria->compare('month', $this->month, true);
		$criteria->compare('amount_paid', $this->amount_paid, true);
		$criteria->compare('registration_paid', $this->registration_paid, true);
		$criteria->compare('created_at', $this->created_at, true);
		$criteria->compare('updated_at', $this->updated_at, true);
		$criteria->compare('status', $this->status);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'pagination'=>array('pageSize'=>UtilityHtml::getPageSize())
		));
	}
}