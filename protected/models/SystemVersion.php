<?php

Yii::import('application.models._base.BaseSystemVersion');

class SystemVersion extends BaseSystemVersion
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
}