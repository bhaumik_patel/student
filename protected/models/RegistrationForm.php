<?php
class RegistrationForm extends CFormModel
{
	public $email;
	public $firstname;
	public $lastname;
	public $is_newsletter;
	public $terms_conditions;
	public $mobile;

	// public $address;
	// public $locality;

	public $validacion;
	public $interests;
	public $city;

	public $password;
	public $verifyPassword;
	public $secureCode; // Captcha

	public function rules()
	{
		$rules = parent::rules();

		/* FIXME: As soon as i grasp how i can dynamically add variables to a
		 class in PHP, i will enable this code snippet for flexibility:

		$profile = new YumProfile;
		$profile_rules = $profile->rules();
		foreach($profile_rules as $rule)
			if(isset($rule[0]) && is_string($rule[0]))
			$this->${$rule[0]} = '';

		$rules = array_merge($rules, $profile->rules());	 */

		$rules[] = array('email, firstname, lastname, mobile, city, interests, secureCode, terms_conditions', 'required');
		$rules[] = array('mobile', 'PcSimplePhoneValidator', 'minNumDigits'=>'10' , 'maxNumDigits'=>'10');

		$rules[] = array('city', 'numerical', 'integerOnly'=>true, 'min'=>1, 'tooSmall'=>'{attribute} can not be blank');
		$rules[] = array('password, verifyPassword', 'required');
		$rules[] = array('password', 'compare', 'compareAttribute'=>'verifyPassword', 'message' => Yii::t("b","Retype password is incorrect."));
		$rules[] = array('is_newsletter, terms_conditions', 'numerical', 'integerOnly'=>true);
		if($this->enableCaptcha())
			//$rules[] = array('secureCode', 'captcha', 'allowEmpty'=>CCaptcha::checkRequirements());
			$rules[] = array('validacion',
					'application.extensions.recaptcha.EReCaptchaValidator',
					'privateKey'=>'6Lc_4OISAAAAAJ7kZni6bm_Q6Ki9MANzoY8FPmN4');
		return $rules;
	}



	public function attributeLabels()
	{
		return array(
				'secureCode'=>'Secure Code',
				'is_newsletter'=>'Yes, i would like to subscribe for offers?',
				'validacion'=>Yii::t('demo', 'Enter both words separated by space:'),
		);
	}
	public static function enableCaptcha()
	{
		return true;
	}
	public static function genRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$string ='';
		for ($p = 0; $p < $length; $p++)
		{
			$string .= $characters[mt_rand(0, strlen($characters)-1)];
		}
		return $string;
	}
}