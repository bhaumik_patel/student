<?php

Yii::import('application.models._base.BaseLocalityMaster');

class LocalityMaster extends BaseLocalityMaster
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function behaviors()
	{
		return array(
				'CTimestampBehavior' => array(
						'class' => 'zii.behaviors.CTimestampBehavior',
						'createAttribute' => 'created_at',
						'updateAttribute' => 'updated_at',
				),
				/*
				 'TimezoneBehavior' => array(
				 		'class' => 'application.components.behaviors.BNDTimezoneBehavior',
				 		'attributes' => array(
				 				'created_at'
				 		)
				 ),
		*/
		);
	}
	public function getLocalityId($name, $city_id, $zipcode='')
	{
		if($model = $this->find("locality_name='".$name."' AND city_id='".$city_id."'")) {
			return $model->id;
		}else {
			if(trim($name)=='') return false;
			$model = new LocalityMaster();
			$model->locality_name 	= $name;
			$model->city_id 		= $city_id;
			if($zipcode!='') {
				$model->locality_zipcode = $zipcode;
			}
			$model->status = 0;
			if($model->save()) {
				return $model->id;
			}
		}
		return false;
	}
}