<?php

Yii::import('application.models._base.BaseDreamStudent');

class DreamStudent extends BaseDreamStudent {

    public $adminssion_date;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function label($n = 1) {
        return Yii::t('app', 'Student|Students', $n);
    }

    public function beforeSave() {
        if ($this->isNewRecord)
            $this->created_at = new CDbExpression('NOW()');
        $this->updated_at = new CDbExpression('NOW()');
        //$this->address    = $_POST['DreamStudent']['address'];
        // p($_POST);
        $this->inquiry_date = CustomFunction::dateFormatymd($this->inquiry_date);
        return parent::beforeSave();
    }
	

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.branch_id', $this->branch_id);
        $criteria->compare('current_batch_id', $this->current_batch_id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.contact_no', $this->contact_no, true);
        $criteria->compare('t.comments', $this->comments, true);
        $criteria->compare('t.inquiry_date', $this->inquiry_date, true);
        $criteria->compare('t.created_at', $this->created_at, true);
        $criteria->compare('t.updated_at', $this->updated_at, true);
        $criteria->compare('t.status', $this->status);

        $criteria->select = 't.*, dsb.adminssion_date';
        $criteria->join = 'LEFT JOIN dream_student_batch dsb ON dsb.student_id = t.id AND dsb.batch_id = t.current_batch_id';

        if (AdminModule::getUserDataByKey('user_type') == 'admin') {
           $criteria->addCondition("t.branch_id='" . AdminModule::getUserDataByKey('branch_id') . "'");
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => UtilityHtml::getPageSize())
        ));
    }

    public function searchBatch() {
        $criteria = new CDbCriteria;

        $criteria->addCondition('dsb.batch_id', Yii::app()->request->getParam('batch_id'));

        //$criteria->compare('id', $this->id, true);
        $criteria->compare('branch_id', $this->branch_id);
        $criteria->compare('current_batch_id', $this->current_batch_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('contact_no', $this->contact_no, true);
        $criteria->compare('comments', $this->comments, true);
        $criteria->compare('inquiry_date', $this->inquiry_date, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => UtilityHtml::getPageSize())
        ));
    }

    public function rules() {
        return array(
            array('branch_id,name, address,contact_no, comments, inquiry_date', 'required'),
            array('status', 'numerical', 'integerOnly' => true),
            array('branch_id, current_batch_id', 'length', 'max' => 10),
            array('name', 'length', 'max' => 255),
            //array('contact_no', 'length', 'max'=>15),
            array('contact_no', 'match', 'pattern' => '/^([0-9\(\)\/\+ \-]*)$/'),
            array('created_at, updated_at', 'safe'),
            array('branch_id, current_batch_id, created_at, updated_at, status', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, branch_id, current_batch_id, name,address, contact_no, comments, inquiry_date, created_at, updated_at, status', 'safe', 'on' => 'search'),
        );
    }

    public function getAdmissionDate() {
        if ($sb = DreamStudentBatch::model()->find("student_id = '$this->id' AND batch_id = '$this->current_batch_id'")) {
            return $sb->adminssion_date;
        }
        return false;
    }

    /* public function searchcontactlist() {
      $criteria = new CDbCriteria;
      $criteria->compare('branch_id', $this->branch_id);
      $criteria->compare('current_batch_id', $this->current_batch_id, true);
      // $criteria->compare('nominee1', $this->nominee1, true);
      $criteria->select = 't.name,t.contact_no,t.contact_no,bba.batch_name,drb.branch_name';
      $criteria->join = 'LEFT JOIN dream_branch drb ON drb.id = t.branch_id ';
      $criteria->join .= 'LEFT JOIN dream_batch bba ON bba.id = t.current_batch_id';
      //$criteria->join .= 'JOIN dream_batch bba ON bba.id = t.current_batch_id';
      //$criteria->condition = 'bd.bank_details_id = bbd.bank_details_id';

      // $criteria->condition = 'products_categories.categories_id = :value';
      // $criteria->params = array(":value" => "C1");

      $data = $this->findAll($criteria);
      return new CActiveDataProvider($this, array(
      'criteria' => $criteria,
      'pagination'=>array('pageSize'=>UtilityHtml::getPageSize())
      ));
      } */
}
