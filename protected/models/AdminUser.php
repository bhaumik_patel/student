<?php

Yii::import('application.models.._base.BaseAdminUser');

/**
 * Enter description here ...
 * @author: Bhaumik PHP Guru
 * @date: Dec 12, 2012  7:36:35 PM
 */
class AdminUser extends BaseAdminUser
{

	public $rpassword;
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function rules() {
		return array(
		//array('created_at, user_roles', 'required'),
		//array('user_type, firstname, lastname, email, password,is_active,username','required' ),
		//array('firstname, lastname, email, username,password,user_type,is_active, branch_id', 'required'),
		array('firstname, lastname, email, username,password,user_type,is_active', 'required'),
		array('user_type', 'length', 'max'=>20),
		array('firstname, lastname', 'length', 'max'=>30),
		array('password', 'length', 'max'=>128,'min'=>5),
		array('lognum', 'length', 'max'=>5),
		array('email', 'length', 'max'=>100),
		array('email','email'),
		array('is_active', 'length', 'max'=>1),
		array('user_roles', 'length', 'max'=>255),
		array('username', 'length', 'max'=>250),
		array('updated_at, logdate, extra, branch_id', 'safe'),
		array(' created_at, updated_at,logdate',  'default', 'setOnEmpty' => true, 'value' => null),
		//array('user_type, firstname, lastname, email, password, updated_at, logdate, lognum, is_active, extra, username', 'default', 'setOnEmpty' => true, 'value' => null),
		array('id, user_type, firstname, lastname, email, password, created_at, updated_at, logdate, lognum, is_active, user_roles, extra, username', 'safe', 'on'=>'search'),
		);
	}
	
	
	/*public function afterFind()
	 {
		$this->created_at = strtotime($this->created_at);
		$this->created_at = date('m-d-Y', $this->created_at);

		//$this->updated_at = strtotime($this->updated_at);
		//$this->updated_at = date('m/d/Y', $this->updated_at);
		parent::afterFind ();
		}*/

	public function beforeSave() {
            if ($this->isNewRecord)
            $this->created_at = new CDbExpression('NOW()');
            $this->updated_at = new CDbExpression('NOW()');
            //$this->user_roles = '1';
            return parent::beforeSave();
	}

	public function makeMD5Password($passowrd)
	{
		if(!empty($passowrd)){
			$MD5pwd = md5($passowrd);
			return $MD5pwd;
		}
		return false;
	}


	public function getUserData($userid)
	{
		$resultset=AdminUser::model()->find("id='$userid'");;
		if(isset($resultset->attributes))
		{
			return $resultset->username;
		}else
		return ''; //$code;
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('app', 'ID'),
			'user_type' => Yii::t('app', 'User Type'),
			'firstname' => Yii::t('app', 'Firstname'),
			'lastname' => Yii::t('app', 'Lastname'),
			'email' => Yii::t('app', 'Email'),
			'branch_id' => null,
			'password' => Yii::t('app', 'Password'),
			'rpassword' => Yii::t('app', 'Retype Password'),	
			'created_at' => Yii::t('app', 'Created At'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'logdate' => Yii::t('app', 'Logdate'),
			'lognum' => Yii::t('app', 'Lognum'),
			'is_active' => Yii::t('app', 'Is Active'),
			'user_roles' => Yii::t('app', 'User Roles'),
			'extra' => Yii::t('app', 'Extra'),
			'username' => Yii::t('app', 'Username'),
			'branch' => null,
		);
	}
	
	public function getParentName($id)
	{
		$model = $this->findByPk($id);
		if($model!='')
		{
			return $model->fullname;
		}
		else
		{
			return $model;
		}
	}

	public static function getDefinedUserType()
	{
		return array(null=>'Please Select','admin'=>'Business Owner','superadmin'=>'Super Admin');
	}
	public static function getUserType($key)
	{
		$data = self::getDefinedUserType();
		if(isset($data[$key])) return $data[$key];
	}
	
	public static function label($n = 1) {
		return Yii::t('app', 'User|User', $n);
	}

}