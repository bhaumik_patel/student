<?php

Yii::import('application.models._base.BaseDreamBatch');

class DreamBatch extends BaseDreamBatch {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if ($this->isNewRecord)
            $this->created_at = new CDbExpression('NOW()');
        $this->updated_at = new CDbExpression('NOW()');
        $this->detail = $_POST['DreamBatch']['detail'];
        //p($_POST);exit;
        $this->days = implode(',', $_POST['DreamBatch_days']);
        $this->registration_fee = '';
        $this->batch_fee = '';
        $this->start_date = '';
        $this->end_date = '';
        return parent::beforeSave();
    }

    public static function label($n = 1) {
        return Yii::t('app', 'Batch|Batch', $n);
    }

    public function rules() {
        return array(
            array('branch_id,batch_name, time_from, time_to', 'required'),
            array('status', 'numerical', 'integerOnly' => true),
            array('branch_id, registration_fee, batch_fee', 'length', 'max' => 10),
            array('batch_name', 'length', 'max' => 255),
            array('days', 'length', 'max' => 100),
            array('registration_fee', 'match', 'pattern' => '/^([-+]?[0-9]*\.?[0-9]+)$/'),
            array('batch_fee', 'match', 'pattern' => '/^([-+]?[0-9]*\.?[0-9]+)$/'),
            array('created_at, updated_at', 'safe'),
            array('branch_id, created_at, updated_at, status', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, branch_id, batch_name, detail, time_from, time_to, days, registration_fee, batch_fee, start_date, end_date, created_at, updated_at, status', 'safe', 'on' => 'search'),
        );
    }

    public function search() {
        $dp = parent::search();
        if (AdminModule::getUserDataByKey('user_type') == 'admin') {
            $criteria = $dp->getCriteria();
            $criteria->addCondition("t.branch_id='" . AdminModule::getUserDataByKey('branch_id') . "'");
        }
        return $dp;
    }

    public static function getFilterArray() {
        $criteria = new CDBCriteria();
        $criteria->addCondition("status='1'");
		//$criteria->order = "batch_name";
        if (AdminModule::getUserDataByKey('user_type') == 'admin') {
            $criteria->addCondition("t.branch_id='" . AdminModule::getUserDataByKey('branch_id') . "'");
        } 
        return $batches = DreamBatch::model()->findAll($criteria);
    }

}

