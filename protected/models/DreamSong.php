<?php

Yii::import('application.models._base.BaseDreamSong');

class DreamSong extends BaseDreamSong
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function beforeSave() {
		if ($this->isNewRecord)
		$this->created_at = new CDbExpression('NOW()');

		$this->updated_at = new CDbExpression('NOW()');

		return parent::beforeSave();
	}
	public static function label($n = 1) {
		return Yii::t('app', 'Dream Song|Dream Song', $n);
	}
}