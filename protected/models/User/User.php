<?php

Yii::import('application.models.User._base.BaseUser');

class User extends BaseUser
{
	public $fullname;
	public $status;
	public $age;
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public static function representingColumn() {
        return 'fullname';
    }

	


	public function search($user_id=null) {
		$user_id=Yii::app()->getUser()->getId();
		//p($user_id);

		$criteria = new CDbCriteria;
		
		

		$criteria->compare('id', $this->id, true);
		$criteria->compare('user_type', $this->user_type, true);
		$criteria->compare('firstname', $this->firstname, true);
		$criteria->compare('lastname', $this->lastname, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('username', $this->username, true);
		$criteria->compare('password', $this->password, true);
		$criteria->compare('created_at', $this->created_at, true);
		$criteria->compare('updated_at', $this->updated_at, true);
		$criteria->compare('logdate', $this->logdate, true);
		$criteria->compare('lognum', $this->lognum);
		$criteria->compare('status', $this->status);
		$criteria->compare('extra', $this->extra, true);
		
		$criteria->order = 'id DESC';
		if(isset($user_id))
		{
			$resultset=User::model()->find("id='$user_id'");
			if(isset($resultset->attributes))
			{
				$data=$resultset->attributes;
				//p($data);
				//ECHO $data['user_type'];
				if($data['user_type']=="user") {
					$criteria->compare('id', $user_id);
				}
			}
		}
		//$criteria->compare('id', '<>'.$user_id);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	 
	public function getUserData($userid)
	{
		$resultset=User::model()->find("id='$userid'");;
		if(isset($resultset->attributes))
		{
			return $resultset->username;
		}else
			return ''; //$code;
	}
}

