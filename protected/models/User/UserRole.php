<?php

Yii::import('application.models.User._base.BaseUserRole');

class UserRole extends BaseUserRole
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function getFrontUserRoleList()
	{
		$data = array();
		$roleData = UserRole::model()->findAll('module_id=2');
		foreach ($roleData as $row) {
			$data[$row->id] = $row->role_name;
		}
		return $data;
	}
}