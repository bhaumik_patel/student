<?php

Yii::import('application.models._base.BaseDreamStudentBatch');

class DreamStudentBatch extends BaseDreamStudentBatch {

    Public $adminssion_date;
    public $unpaid_fee;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if ($this->isNewRecord)
            $this->created_at = new CDbExpression('NOW()');
        $this->updated_at = new CDbExpression('NOW()');
        $this->branch_id = $this->student->branch_id;
        $this->unpaid_amount = $this->fee_amount;

        $this->joining_date = '';
        $this->adminssion_date = CustomFunction::dateFormatymd($this->adminssion_date);
		
		
        return parent::beforeSave();
    }

    public function afterSave() {
        //$next_payment_date = $this->getNextPayDate();
        //$next_payment_date = CustomFunction::dateFormatymd($next_payment_date);
        DreamStudentPayment::model()->updatePaymentCalc();
        return parent::beforeSave();
    }

    public static function label($n = 1) {
        return Yii::t('app', 'Student Admission|Student Admission', $n);
    }

    public function attributeLabels() {
        $ar = parent::attributeLabels();
        $ar['registration_fee'] = Yii::t('app', 'Reg. Fee');
        $ar['paid_registration'] = Yii::t('app', 'Reg. Paid');
        $ar['unpaid_registration'] = Yii::t('app', 'Reg. Unpaid');

        return $ar;
    }

    public function rules() {
        return array(
            array('adminssion_date,batch_id,fee_amount', 'required'),
			array('roll_no', 'checkUniqueRollNo'),
			array('is_paid_registration, status', 'numerical', 'integerOnly' => true),
            array('student_id, batch_id, registration_fee, fee_amount, paid_amount', 'length', 'max' => 10),
            array('created_at, updated_at', 'safe'),
            array('student_id, roll_no, registration_fee, is_paid_registration, is_paid_fee, paid_amount, created_at, updated_at, status', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, roll_no, student_id, branch_id,batch_id, adminssion_date, joining_date, registration_fee, is_paid_fee, is_paid_registration, fee_amount, paid_amount, created_at, updated_at, status', 'safe', 'on' => 'search'),
        );
    }
	public function checkUniqueRollNo($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			if($res = DreamStudentBatch::model()->find("roll_no='$this->roll_no' AND batch_id='$this->batch_id' AND (id != '$this->id' AND student_id != '$this->student_id')")) {
				$this->addError('roll_no','Roll No. already assigned to '.$res->student);
			}
			if($res = DreamStudentBatch::model()->find("batch_id='$this->batch_id' AND (id != '$this->id' AND student_id = '$this->student_id')")) {
				$this->addError('batch_id','Student already Register in this batch!');
			}
		}
	}
    public function search() {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id, true);
		$criteria->compare('roll_no', $this->roll_no, true);
        $criteria->compare('student_id', $this->student_id);
        $criteria->compare('batch_id', $this->batch_id);
        $criteria->compare('branch_id', $this->branch_id);
        $criteria->compare('adminssion_date', $this->adminssion_date, true);
        $criteria->compare('next_payment_date', $this->next_payment_date, true);
        $criteria->compare('joining_date', $this->joining_date, true);
        $criteria->compare('registration_fee', $this->registration_fee, true);
        $criteria->compare('is_paid_registration', $this->is_paid_registration);
        $criteria->compare('paid_registration', $this->paid_registration, true);
        $criteria->compare('unpaid_registration', $this->unpaid_registration, true);
        $criteria->compare('fee_amount', $this->fee_amount, true);
        $criteria->compare('is_paid_fee', $this->is_paid_fee);
        $criteria->compare('paid_amount', $this->paid_amount, true);
        $criteria->compare('unpaid_amount', $this->unpaid_amount, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('status', $this->status);
		$criteria->order = 'roll_no';
        if (AdminModule::getUserDataByKey('user_type') == 'admin') {
            $criteria->addCondition("t.branch_id='" . AdminModule::getUserDataByKey('branch_id') . "'");
        }
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => UtilityHtml::getPageSize())
        ));
    }

    public function getFieldFormat($fld) {
        switch ($fld) {
            case 'paid_amount':
                return '<div class="' . (($this->fee_amount > $this->paid_amount) ? "" : "green-amt") . '">' . $this->$fld . '</div>';
                break;
            case 'unpaid_amount':
                return '<div class="' . (($this->fee_amount > $this->paid_amount) ? "red-amt" : "") . '">' . $this->unpaid_amount . '</div>';
                break;
            case 'fee_amount':
                return '<div class="orange-amt">' . $this->$fld . '</div>';
                break;
            case 'paid_registration':
                return '<div class="' . (($this->registration_fee > $this->paid_registration) ? "" : "green-amt") . '">' . $this->paid_registration . '</div>';
                break;
            case 'unpaid_registration':
                return '<div class="' . (($this->registration_fee > $this->paid_registration) ? "red-amt" : "") . '">' . $this->unpaid_registration . '</div>';
                break;
            case 'registration_fee':
                return '<div class="orange-amt">' . $this->registration_fee . '</div>';
                break;
        }
    }

    public function getNextPayDate() {
        //$nextpaydate =  date("Y-m-d",strtotime("+1 month", $this->adminssion_date));
        $time = strtotime($this->adminssion_date);
        $nextpaydate = date("d-m-Y", strtotime("+1 month", $time));
        $currentdate = date("d-m-Y");
        return $nextpaydate;
        //return '<div class="' . (($currentdate == $nextpaydate) ? "red-amt" : "") . '">' . $nextpaydate . '</div>';
    }

    public function getNextPayDateFormat() {
        // $time = strtotime($this->next_payment_date);
        $nextpaydate = date("d-m-Y", strtotime($this->next_payment_date));
        $currentdate = date("d-m-Y");
        // return $nextpaydate;
        return '<div class="' . (($currentdate == $nextpaydate) ? "red-amt" : "") . '">' . $nextpaydate . '</div>';
    }

    public function getTotalsPaid($ids) {
        $ids = implode(",", $ids);
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT SUM(paid_amount) FROM `dream_student_batch` where id in ($ids)");
        // return "Total Rs: ".$amount = $command->queryScalar();
        return "<div>Total Rs: <b>" . $amount = $command->queryScalar() . "</b></div>";
    }

    public function getTotalsunPaid($ids) {
        $ids = implode(",", $ids);
        $connection = Yii::app()->db;
        $command = $connection->createCommand("SELECT SUM(unpaid_amount) FROM `dream_student_batch` where id in ($ids)");
        return "<div>Total Rs: <b>" . $amount = $command->queryScalar() . "</b></div>";
    }
	function checkUserExist()
	{
		$connection = Yii::app()->db;		
		//$command = $connection->createCommand("SELECT id FROM `dream_student_batch` WHERE student_id='$this->student_id' AND batch_id='$this->batch_id' AND branch_id='".$this->student->branch_id."'");		
		$command = $connection->createCommand("SELECT id FROM `dream_student_batch` WHERE student_id='$this->student_id' AND branch_id='".$this->batch_id."'");		
		$id = $command->queryScalar();	
		if($id=='')
			return 0;
		return 1;
	}
	function getRollno()
	{
		$connection = Yii::app()->db;
		$command = $connection->createCommand("SELECT max(roll_no) + 1 FROM dream_student_batch WHERE batch_id='$this->batch_id' AND branch_id='".$this->student->branch_id."'");	
		$maxid = $command->queryScalar();
		if($maxid=='')
			$maxid = '1';
		return $maxid;
	}	
}