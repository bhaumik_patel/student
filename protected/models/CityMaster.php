<?php

Yii::import('application.models._base.BaseCityMaster');

class CityMaster extends BaseCityMaster
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function getHomePageList()
	{
		$criteria = new CDBCriteria();
		$criteria->addCondition("status=1 AND is_home_page=1 AND country_id='".GlobalData::getCountryId()."'");
		$criteria->order = 'pos DESC';
		return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
				'pagination'=>array('pageSize'=>50)));

	}
	public function findAllConutryCity()
	{
		$criteria = new CDBCriteria();
		$criteria->addCondition("status=1 AND country_id='".GlobalData::getCountryId()."'");
		$criteria->order = 'name ASC';
		return $this->findAll($criteria);
	}
	public function getDataById($id)
	{
		return $this->findByPk($id, "status=1 AND country_id='".GlobalData::getCountryId()."'");
	}
	
	
}