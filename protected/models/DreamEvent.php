<?php

Yii::import('application.models._base.BaseDreamEvent');

class DreamEvent extends BaseDreamEvent
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public static function label($n = 1) {
		return Yii::t('app', 'Event|Event', $n);
	}
	public function beforeSave()
	{
		if ($this->isNewRecord)
		$this->created_at = new CDbExpression('NOW()');

		$this->updated_at = new CDbExpression('NOW()');
                $this->start_date = CustomFunction::dateFormatymd($this->start_date);
                $this->end_date = CustomFunction::dateFormatymd($this->end_date);
               // echo "<pre>";print_r($_POST);exit;
		//$this->song_ids = $_POST['DreamEvent']['song_ids'];
		return parent::beforeSave();
	}
	public function rules() {
		return array(
			array('branch_id,name, details, contact_name, contact_no, fees, event_total_minute, start_date, end_date', 'required'),
			array('event_total_minute, is_editing_done, is_paid, status', 'numerical', 'integerOnly'=>true),
			array('branch_id, fees', 'length', 'max'=>10),
			array('name, contact_name', 'length', 'max'=>150),
			//array('contact_no', 'length', 'max'=>15),
			array('contact_no', 'match', 'pattern'=>'/^([0-9\(\)\/\+ \-]*)$/'),
			array('fees', 'match', 'pattern'=>'/^([-+]?[0-9]*\.?[0-9]+)$/'),
			array('event_total_minute', 'match', 'pattern'=>'/^([0-9]*)$/'),
			array('song_ids, created_at, updated_at', 'safe'),
			array('branch_id, song_ids, is_editing_done, created_at, updated_at, is_paid, status', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, branch_id, song_ids, name, details, contact_name, contact_no, fees, event_total_minute, is_editing_done, start_date, end_date, created_at, updated_at, is_paid, status', 'safe', 'on'=>'search'),
		);
	}
}