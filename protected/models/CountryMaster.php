<?php

Yii::import('application.models._base.BaseCountryMaster');

class CountryMaster extends BaseCountryMaster
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	public function getAllCountries()
	{
		$resultset=CountryMaster::model()->findAll("is_publish='1'");
		$countriesArr=array();
		foreach($resultset as $key=>$row) {
			$data=$row->attributes;
			$countriesArr[$data['id']]=$data;
		}
		return $countriesArr;
	}
	public function getCountryFromSuffix($code)
	{
		$model = CountryMaster::model()->find("country_code = '$code'");
		if($model && $model->country) {
			return (string) $model->country;
		}
	}
}