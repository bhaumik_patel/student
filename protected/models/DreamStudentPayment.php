<?php

Yii::import('application.models._base.BaseDreamStudentPayment');

class DreamStudentPayment extends BaseDreamStudentPayment {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeSave() {
        if ($this->isNewRecord)
            $this->created_at = new CDbExpression('NOW()');
        $this->updated_at = new CDbExpression('NOW()');
        //$this->year = $_POST['DreamStudentPayment_year'];
        //$this->month = $_POST['DreamStudentPayment_month'];
        $this->year = '';
        $this->month = '';
        //$this->pay_date = $_POST['DreamStudentPayment']['pay_date'];
        $this->pay_date = CustomFunction::dateFormatymd($_POST['DreamStudentPayment']['pay_date']);
        $this->stud_batch_id = $_POST['DreamStudentPayment']['stud_batch_id'];
        //p($_POST,0);         p($this->attributes);
        return parent::beforeSave();
    }

    public function updatePaymentCalc($sbid = 0) {
        $sql = "UPDATE dream_student_batch as t
            JOIN (SELECT t.id, sum(IF(dtp.amount_paid>0,dtp.amount_paid,0)) as total_paid, 
(t.fee_amount - sum(IF(dtp.amount_paid>0,dtp.amount_paid,0))) as total_unpaid, t.fee_amount, 
sum(IF(dtp.registration_paid>0,dtp.registration_paid,0)) as total_reg_paid, (t.registration_fee - sum(IF(dtp.registration_paid>0,dtp.registration_paid,0))) as total_reg_unpaid, t.registration_fee
            FROM dream_student_batch as t
            LEFT OUTER JOIN dream_student_payment as dtp ON dtp.stud_batch_id = t.id
            WHERE 1
            GROUP BY t.id) as res ON res.id = t.id
            SET 
            t.paid_amount = IF(res.total_paid IS NULL,0,res.total_paid), 
            t.unpaid_amount = IF(res.total_unpaid IS NULL,0,res.total_unpaid),
            t.paid_registration = IF(res.total_reg_paid IS NULL,0,res.total_reg_paid), 
            t.unpaid_registration = IF(res.total_reg_unpaid IS NULL,0,res.total_reg_unpaid);
            UPDATE dream_student_batch as t 
SET t.is_paid_registration=(IF(t.paid_registration>=t.registration_fee,1,0))
,t.is_paid_fee=(IF(t.paid_amount>=t.fee_amount,1,0));";
        Yii::app()->db->createCommand($sql)->execute();
    }

    public function afterSave() {
        $this->updatePaymentCalc($this->stud_batch_id);
        return parent::afterSave();
    }

    public function afterDelete() {
        $this->updatePaymentCalc($this->stud_batch_id);
        return parent::afterDelete();
    }

    public static function label($n = 1) {
        return Yii::t('app', 'Student Payment|Student Payment', $n);
    }

    public function rules() {
        return array(
            array('year, status', 'numerical', 'integerOnly' => true),
            //array('year, month', 'required'),
            array('amount_paid, registration_paid', 'checkValidAmount'),
            array('stud_id, batch_id, branch_id, amount_paid', 'length', 'max' => 10),
            array('created_at, updated_at', 'safe'),
            array('amount_paid', 'match', 'pattern' => '/^([-+]?[0-9]*\.?[0-9]+)$/'),
            array('stud_id, batch_id, branch_id, year, month, amount_paid,stud_batch_id, created_at, updated_at, registration_paid, status', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, stud_id, batch_id, branch_id, year, month,stud_batch_id, amount_paid, registration_paid, created_at, updated_at, status', 'safe', 'on' => 'search'),
        );
    }

    public function checkValidAmount($attribute, $params) {
        if (!$this->hasErrors()) {
            if($this->amount_paid > $this->studBatch->unpaid_amount)
                $this->addError('amount_paid', 'Amount paid is greater than remaining amount');
            if($this->registration_paid > $this->studBatch->unpaid_registration)
                $this->addError('registration_paid', 'Registration paid is greater than remaining fee');
            
            if($this->registration_paid<=0 && $this->amount_paid<=0) {
                $this->addError('amount_paid', 'Please enter valid Amount paid');
            }
        }
    }

    public function monthwiseReport() {
        $criteria = new CDbCriteria;
//SELECT sum(unpaid_amount),month(next_payment_date),sum(paid_amount),branch_id FROM dream_student_batch GROUP BY MONTH(next_payment_date),branch_id;
//SELECT sum(unpaid_amount),month(next_payment_date),sum(paid_amount) FROM dream_student_batch GROUP BY MONTH(next_payment_date);

        /* $criteria->compare('id', $this->id, true);
          $criteria->compare('stud_id', $this->stud_id);
          $criteria->compare('batch_id', $this->batch_id);
          $criteria->compare('branch_id', $this->branch_id);
          $criteria->compare('stud_batch_id', $this->stud_batch_id);
          $criteria->compare('pay_date', $this->pay_date, true);
          $criteria->compare('year', $this->year);
          $criteria->compare('month', $this->month, true);
          $criteria->compare('amount_paid', $this->amount_paid, true);
          $criteria->compare('created_at', $this->created_at, true);
          $criteria->compare('updated_at', $this->updated_at, true);
          $criteria->compare('status', $this->status); */

        $criteria->select = 'sum(unpaid_amount) as total_remaining_amt, month(next_payment_date) as month,sum(paid_amount) as total_paid_amt ,branch_id';
        // $criteria->condition = 'receiver_id = :receiverId';
        $criteria->group = 'MONTH(next_payment_date),branch_id';
        // $criteria->order = 'priority_id DESC, max_added_on DESC';
        //$criteria->params = array('receiverId' => $userId);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => UtilityHtml::getPageSize())
        ));
    }

    public function search() {
        $dp = parent::search();
        if (AdminModule::getUserDataByKey('user_type') == 'admin') {
            $criteria = $dp->getCriteria();
            $criteria->addCondition("t.branch_id='" . AdminModule::getUserDataByKey('branch_id') . "'");
        }
        return $dp;
    }
    
    public function updatePaymentbatch($modelbatch)
    {     
         $sql = "UPDATE dream_student_payment SET batch_id='".$modelbatch->batch_id."' WHERE stud_batch_id='".$modelbatch->id."'";
         Yii::app()->db->createCommand($sql)->execute();
    }
}
