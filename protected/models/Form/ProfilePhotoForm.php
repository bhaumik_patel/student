<?php
Yii::import('application.extensions.jcrop.*');
class ProfilePhotoForm extends CFormModel
{
	public $photo;
	
	public $cropID;
	public $cropX;
	public $cropY;
	public $cropW;
	public $cropH;
	
	
	public function rules()
	{
		$rules = parent::rules();

		/* FIXME: As soon as i grasp how i can dynamically add variables to a
		 class in PHP, i will enable this code snippet for flexibility:

		$profile = new YumProfile;
		$profile_rules = $profile->rules();
		foreach($profile_rules as $rule)
			if(isset($rule[0]) && is_string($rule[0]))
			$this->${$rule[0]} = '';

		$rules = array_merge($rules, $profile->rules());	 */

		return $rules;
	}
	public function initData()
	{
		$user = Yii::app()->user->getState('user');
		$this->photo 	 = UtilityHtml::getDataByKey($user, 'photo');
	
	}
	public function attributeLabels()
	{
		return array(

		);
	}
}