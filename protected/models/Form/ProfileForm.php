<?php
Yii::import('application.extensions.jcrop.*');
class ProfileForm extends CFormModel
{
	public $email;
	public $firstname;
	public $lastname;
	public $mobile;
	public $photo;
	
	public $cropID;
	public $cropX;
	public $cropY;
	public $cropW;
	public $cropH;
	
	// public $address;
	// public $locality;

	public $interests;
	public $city_id;

	public function rules()
	{
		$rules = parent::rules();

		/* FIXME: As soon as i grasp how i can dynamically add variables to a
		 class in PHP, i will enable this code snippet for flexibility:

		$profile = new YumProfile;
		$profile_rules = $profile->rules();
		foreach($profile_rules as $rule)
			if(isset($rule[0]) && is_string($rule[0]))
			$this->${$rule[0]} = '';

		$rules = array_merge($rules, $profile->rules());	 */

		$rules[] = array('email, firstname, lastname, mobile, city_id, interests', 'required');
		$rules[] = array('mobile', 'PcSimplePhoneValidator', 'minNumDigits'=>'10' , 'maxNumDigits'=>'10');

		$rules[] = array('city_id', 'numerical', 'integerOnly'=>true, 'min'=>1, 'tooSmall'=>'{attribute} can not be blank');
		return $rules;
	}
	public function initData()
	{
		$user = Yii::app()->user->getState('user');
		$this->firstname = UtilityHtml::getDataByKey($user, 'firstname');
		$this->lastname  = UtilityHtml::getDataByKey($user, 'lastname');
		$this->email	 = UtilityHtml::getDataByKey($user, 'email');
		$this->mobile 	 = UtilityHtml::getDataByKey($user, 'mobile');
		$this->photo 	 = UtilityHtml::getDataByKey($user, 'photo');
		$this->city_id 	 = UtilityHtml::getDataByKey($user, 'city_id');
		$this->interests = explode(",",UtilityHtml::getDataByKey($user, 'interest_category_ids'));
	}
	public function attributeLabels()
	{
		return array(
			'city_id'=>'City'
		);
	}
}