<?php

Yii::import('application.models._base.BaseStateMaster');

class StateMaster extends BaseStateMaster
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
	
	public function getStatename($code)
	{
		$resultset=StateMaster::model()->find("state_code='$code' AND country_code='AU'");;
		if(isset($resultset->attributes))
		{
			return $resultset->state;
		}else
			return $code;
	}
	public function getAllStates()
	{
		$resultset=StateMaster::model()->findAll("is_publish='1'");
		$countriesArr=array();
		foreach($resultset as $key=>$row) {
			$data=$row->attributes;
			$countriesArr[$data['id']]=$data;
		}	
		return $countriesArr;
	} 
}