<h1>
	<?php echo Yii::t("b", 'Registration'); ?>
</h1>
<div>
	<?php
	foreach(Yii::app()->user->getFlashes() as $key => $message) {
		echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	}
	?>
</div>
<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'registration-form-register-form',
			//'enableAjaxValidation' => true,
			'clientOptions'=>array('validateOnSubmit'=>true),
)); ?>

	<p class="note">
		Fields with <span class="required">*</span> are required.
	</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'firstname'); ?>
		<?php echo $form->textField($model,'firstname'); ?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'lastname'); ?>
		<?php echo $form->textField($model,'lastname'); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mobile'); ?>
		<span>+91</span>
		<?php echo $form->textField($model, 'mobile'); ?>
		<?php echo $form->error($model,'mobile'); ?>
	</div>

	<!-- row -->
	<div class="row">
		<?php echo $form->labelEx($model,'city'); ?>
		<?php echo $form->dropDownList($model, 'city', GlobalData::getAutoCityDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Choose a City..."),'style'=>'width: 200px;')); ?>
		<?php echo $form->error($model,'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'interests'); ?>
		<?php // echo $form->textField($model, 'interests'); ?>
		<?php echo $form->dropDownList($model, 'interests', GlobalData::getAutoCateogryDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Select Your Interest..."),'multiple'=>'multiple', 'style'=>'width: 400px;')); ?>
		<?php //echo $form->dropDownList($model, 'interests', GlobalData::getAutoCateogryDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Choose a Category..."),'style'=>'width: 400px;')); ?>

		<?php echo $form->error($model,'interests'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->textField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'verifyPassword'); ?>
		<?php echo $form->textField($model,'verifyPassword'); ?>
		<?php echo $form->error($model,'verifyPassword'); ?>
	</div>



	<?php if($model->enableCaptcha()): ?>
	<div class="row">
		<?php echo $form->labelEx($model,'secureCode'); ?>
		<div class="captcha">
			<?php uniqid(); $this->widget('CCaptcha'); ?>
			<div class="captcha-text">
				<?php echo $form->textField($model,'secureCode'); ?>
			</div>
		</div>
		<?php echo $form->error($model,'secureCode'); ?>
	</div>

	<div class="row">
		<?php $model->is_newsletter=1;?>
		<?php echo $form->checkbox($model,'is_newsletter'); ?>
		<?php echo $model->getAttributeLabel('is_newsletter')?>
		<?php echo $form->error($model,'is_newsletter'); ?>
	</div>

	<div class="row">
		<?php $model->terms_conditions=1; //echo $form->labelEx($model,'terms_conditions'); ?>
		<?php echo $form->checkbox($model,'terms_conditions'); ?>
		<?php echo Yii::t('b','Yes, I am agree for the ')?>
		<?php echo CHtml::link(Yii::t('b','Terms &amp; Conditions'),array('page/terms'))?>
		<?php echo $form->error($model,'terms_conditions'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>
	<?php endif;?>

	<?php $this->endWidget(); ?>

</div>
<!-- form -->


<script type="text/javascript"> 
$(".chzn-select-deselect").chosen({allow_single_deselect:true});
</script>
