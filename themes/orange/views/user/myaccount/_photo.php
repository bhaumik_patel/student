<?php $model = new ProfilePhotoForm();
$model->initData();?>
<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'profile-form',
			//'enableAjaxValidation' => true,
			'clientOptions'=>array('validateOnSubmit'=>true),
)); ?>
<div class="row">
		<?php echo $form->labelEx($model,'photo'); ?>
		<?php echo $form->hiddenField($model, 'photo'); ?>

		<?php  $this->widget('ext.EAjaxUpload.EAjaxUpload',
				array(
						'id'=>'photo',
						'config'=>array(
								'action'=>Yii::app()->createUrl('user/myaccount/uploadPhoto'),
								'allowedExtensions'=>array("jpg","png","gif"), //array("jpg","jpeg","gif","exe","mov" and etc...
								'sizeLimit'=>10*1024*1024,// maximum file size in bytes
								//'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
								'onComplete'=>"js:function(id, fileName, responseJSON){
								if($(\"#".GxHtml::activeId($model,'photos')."\").val().length>0) {
								var str = $(\"#".GxHtml::activeId($model,'photos')."\").val() + ','+ responseJSON.filename;
}else {
								var str = responseJSON.filename;
}
								$(\"#".GxHtml::activeId($model,'photos')."\").val(str);
								if($(\"#AdPostForm_photos\").val().split(',').length >= ".SystemConfig::getValue('allow_no_of_photos_in_ad').") {
								$('.qq-upload-button').hide();
}
}",
									
								//'messages'=>array(
               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
               //                  'emptyError'=>"{file} is empty, please select files again without it.",
               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
               //                 ),
               //'showMessage'=>"js:function(message){ alert(message); }"
              )
)); ?>






	

<?php /* 

<div>
    <?php //m($form); //echo CHtml::beginForm(null, 'POST')?>
<?php //echo CHtml::errorSummary($model); ?> 
    <? //$form in this example is of type AvatarForm, containing variables for the crop area's x, y, width and height, hence the corresponding widget form element parameters ?>
    <?php echo CHtml::activeHiddenField($model, 'cropID');?>
    <?php echo CHtml::activeHiddenField($model, 'cropX', array('value' => '0'));?>
    <?php echo CHtml::activeHiddenField($model, 'cropY', array('value' => '0'));?>
    <?php echo CHtml::activeHiddenField($model, 'cropW', array('value' => '100'));?>
    <?php echo CHtml::activeHiddenField($model, 'cropH', array('value' => '100'));?>
    <?php $previewWidth = 100; $previewHeight = 100;?>
    <?php $this->widget('ext.yii-jcrop.jCropWidget',array(
        'imageUrl'=>UtilityHtml::getTempUrl().'Chrysanthemum.jpg',
        'formElementX'=>'ProfileForm_cropX',
        'formElementY'=>'ProfileForm_cropY',
        'formElementWidth'=>'ProfileForm_cropW',
        'formElementHeight'=>'ProfileForm_cropH',
        'previewId'=>'avatar-preview', //optional preview image ID, see preview div below
        'previewWidth'=>$previewWidth,
        'previewHeight'=>$previewHeight,
        'jCropOptions'=>array(
	    	'ajaxUrl' => Yii::app()->createUrl('user/myaccount/crop'),
            'aspectRatio'=>1, 
            'boxWidth'=>400,
            'boxHeight'=>400,
            'setSelect'=>array(0, 0, 100, 100),
        ),
    )
    );
    ?>
 </div>
    <!-- Begin optional preview box -->
    <div style="position:relative; overflow:hidden; width:<?php echo $previewWidth?>px; height:<?php echo $previewHeight?>px; margin-top: 10px; margin-bottom: 10px;">
    	<img id="avatar-preview" src="<?php echo UtilityHtml::getTempUrl().'Chrysanthemum.jpg'?>" style="width: 0px; height: 0px; margin-left: 0px; margin-top: 0px;">
    </div>
    <!-- End optional preview box -->
 
    <?php echo CHtml::Submitbutton('Crop Avatar'); ?>
    <?php //echo CHtml::endForm()?>



		<?php echo $form->error($model,'photos'); ?>
	</div>
*/ ?>
	<?php $this->endWidget(); ?>
</div>
<!-- form -->
	