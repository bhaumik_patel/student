<h2>My Account &raquo; AD Manager</h2>
<?php 

$this->breadcrumbs = array(
		$model->label(2) => array('index'),
		Yii::t('app', 'Manage'),
);
?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => 'bjois-ad-grid',
		
		'dataProvider' => $model->searchByUser(),
		//'filter' => $model,
		'columns' => array(
				'id',

				array(
						'name'=>'category_id',
						'value'=>'GxHtml::valueEx($data->category)',
						'filter'=>GxHtml::listDataEx(BjoisCategory::model()->findAllAttributes(null, true)),
				),
				'ad_user_type',
				'ad_type',
				'title',
					
				array(
						'name' => 'is_approved',
						'value' => '($data->is_approved === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
						'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
				),
				'created_at',
				'updated_at',
				//'description',
				/*
				 'external_video_code',
'name',
'email',
'mobile',
'phone',
'address',
'locality',
'price',
'total_views',
'total_reply',
'total_likes',
'total_spam_report',
'avg_rate',
array(
		'name' => 'is_approved',
		'value' => '($data->is_approved === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
		'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
),
'created_at',
'updated_at',
'approved_at',
'valid_till',
'validity_in_minutes',
array(
		'name' => 'satus',
		'value' => '($data->satus === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
		'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
),
*/
				array(
            		'class' => 'CButtonColumn',
					'template'=>'{view}'
        ),
		),
)); ?>