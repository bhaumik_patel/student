<h2>My Account &raquo; Profile</h2>

<?php $model = new ProfileForm(); 
$model->initData();?>

<div>
	<?php
	foreach(Yii::app()->user->getFlashes() as $key => $message) {
		echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	}
	?>
</div>

<div class="form">

	<?php  $form=$this->beginWidget('GxActiveForm', array(
			'id'=>'profile-form',
			//'enableAjaxValidation' => true,
			'clientOptions'=>array('validateOnSubmit'=>true),
)); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'firstname'); ?>
		<?php echo $form->textField($model,'firstname'); ?>
		<?php echo $form->error($model,'firstname'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'lastname'); ?>
		<?php echo $form->textField($model,'lastname'); ?>
		<?php echo $form->error($model,'lastname'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'mobile'); ?>
		<span>+91</span><?php echo $form->textField($model,'mobile'); ?>
		<?php echo $form->error($model,'mobile'); ?>
	</div>
	<!-- row -->
	<div class="row">
		<?php echo $form->labelEx($model,'city_id'); ?>
		<?php echo $form->dropDownList($model, 'city_id', GlobalData::getAutoCityDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Choose a City..."),'style'=>'width: 200px;')); ?>
		<?php echo $form->error($model,'city_id'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model, 'interests'); ?>
		<?php // echo $form->textField($model, 'interests'); ?>
		<?php echo $form->dropDownList($model, 'interests', GlobalData::getAutoCateogryDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Select Your Interest..."),'multiple'=>'multiple', 'style'=>'width: 400px;')); ?>
		<?php //echo $form->dropDownList($model, 'interests', GlobalData::getAutoCateogryDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Choose a Category..."),'style'=>'width: 400px;')); ?>

		<?php echo $form->error($model,'interests'); ?>
	</div>
	
	<div id="update-save-info">
	</div>
	<?php echo CHtml::ajaxSubmitButton('Update', 
				$this->createUrl('/user/myaccount/AjaxSave'), 
				array('update'=>'#update-save-info',
					  'dataType'=>'json',
					  'type'=>'POST',
						'success'=>"function(resp) {
							$('#update-save-info').html(resp.msg);
							$('#profile-form input[type=\"text\"]').attr(\"readonly\", \"readonly\");
						 }",
					  ));?>
	<?php $this->endWidget(); ?>
</div>
<!-- form -->


<script>
$('#profile-form input[type="text"]').attr("readonly", "readonly");
$('#profile-form input[type="text"]').click(function() {
    $(this).removeAttr("readonly");
});
$('#profile-form input[type="text"]').focus(function() {
	$(this).removeAttr("readonly");
});
/*
$('#profile-form input[type="text"]').focusout(function() {
    $(this).attr("readonly", "readonly");
});
*/
$(".chzn-select-deselect").chosen({allow_single_deselect:true});
</script>
