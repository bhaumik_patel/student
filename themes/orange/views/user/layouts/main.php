<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bjois</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
</script>
</head>
<body>
	<?php echo $this->renderPartial('/layouts/header')?>
	<?php //echo $this->renderPartial('slider')?>

	<div id="templatemo_main" class="wrapper">
		<div class="left-col">
			<?php echo $this->renderPartial('/layouts/left')?>
		</div>
		<div class="main-col">
			<?php echo $content; ?>
		</div> 

		<div class="clear"></div>
	</div>

	<div id="templatemo_bottom_wrapper">
		<?php echo $this->renderPartial('/layouts/footer')?>
	</div>

	<div id="templatemo_footer_wrapper">
		<div id="templatemo_footer" class="wrapper">
			Copyright &copy; 2013
		</div>
	</div>

</body>
</html>