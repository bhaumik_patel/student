<ul class="left_link">
	<li><span id="left_1"><?php echo CHtml::link(Yii::t('b','My Profile'), Yii::app()->createUrl('/user/myaccount/index'))?></span></li>
	<li><span id="left_2"><?php echo CHtml::link(Yii::t('b','Change Password'), Yii::app()->createUrl('/user/myaccount/changePassword'))?></span></li>
	<li><span id="left_3"><?php echo CHtml::link(Yii::t('b','Manage Ad'), Yii::app()->createUrl('/user/myaccount/adList'))?></span></li>
</ul>