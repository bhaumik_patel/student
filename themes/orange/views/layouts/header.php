<div id="templatemo_header_wrapper">
	<div id="templatemo_header" class="wrapper">
    	<div id="site_title"><a href="<?php echo Yii::app()->homeUrl; ?>">BJois</a></div>
    	
        <div id="templatemo_menu" class="ddsmoothmenu">
        	<div class="account-link">
	    		<span><?php echo UserModule::getWelcomeText();?></span>
				<a href="<?php echo UserModule::getLoginUrl(); ?>"><?php echo UserModule::getLoginText(); ?></a>
				| <a href="<?php echo UserModule::getRegisterUrl(); ?>"><?php echo UserModule::getRegisterText(); ?></a>
	    	</div>
	    	<ul>	
                <li><a href="<?php echo Yii::app()->homeUrl; ?>" class="selected">Home</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('buser/register')?>">Register</a>
                <li><a href="<?php echo Yii::app()->createUrl('ad/create')?>">Post Ad</a>
                    <ul>
                        <li><a href="<?php echo Yii::app()->createUrl('ad/create',array('type'=>'sell'))?>"><?php echo Yii::t('b','I want to Sell')?></a>
						</li>
                        <li><a href="<?php echo Yii::app()->createUrl('ad/create',array('type'=>'sell'))?>"><?php echo Yii::t('b','I want to Buy')?></a></li>
                  	</ul>
                </li>
                <li><a href="#">Contact Us</a></li>
            </ul>
            <br style="clear: left" />
        </div> <!-- end of templatemo_menu -->
    </div>
</div>