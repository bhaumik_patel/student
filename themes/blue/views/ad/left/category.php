<div class="cat-block popular-block">
<h3><?php echo Yii::t('bk','Category') ?></h3>
	<?php   $this->widget('application.extensions.MTreeView.MTreeView',array(
	        'collapsed'=>true,
	        'animated'=>'fast',
	        //---MTreeView options from here
			//'ajaxOptions'=>array('update'=>'#adlist'),
	        'table'=>'bjois_category',//what table the menu would come from
	        'hierModel'=>'adjacency',//hierarchy model of the table
	        //'conditions'=>array('status=:status',array(':status'=>1)),//other conditions if any
			'fields'=>array(//declaration of fields
	            'text'=>'name',//no `text` column, use `title` instead
	            'alt'=>false,//skip using `alt` column
	            'id_parent'=>'parent_id',//no `id_parent` column,use `parent_id` instead
	            'task'=>false,
	            'icon'=>false,
	        	'tooltip'=>false,
	        	'position'=>'pos',
	            //'url'=> Yii::app()->createUrl('ad/search', array('cat_id'=>'id'))
	        	'url'=> array('/ad/search/', array('cat_id'=>'id','name'=>'name')),
	        ),
	    ));?>
</div>
<?php Yii::app()->clientScript->registerScript('treesel', '$(".treeview").find( "span.selected").closest("ul").parent().children("div").trigger("click");');?>
<script>
jQuery(function($) {
	$('.treeview').find( "span.selected").closest('ul').parent().children('div').trigger('click');
	//$('.treeview').find( "span.selected").closest('ul').parent().children('div').trigger('click');
});
</script>