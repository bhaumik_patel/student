<div class="cat-block popular-block type-block">
<h3><?php echo Yii::t('bk','Type')?></h3>
	<ul>
		<?php foreach(GlobalData::getAdTypeList() as $id=>$type):?>
			<li><a class="ad_type_link <?php  echo ((GlobalData::getCookie('ad_type')==$id))?'active':''?>" rel="<?php echo $id?>"><?php echo $type?></a></li>
		<?php endforeach;?>
	</ul>
</div>