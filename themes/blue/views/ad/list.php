<BR />
<div class="container1">
	<h4>
		<?php echo Yii::t('app', 'Search Result')?>
	</h4>
	<div class="search_setting">
		<?php $this->renderPartial('search_setting')?>
	</div>

	<?php $this->renderPartial('left')?>
	<div class="list-content">
		<?php $this->renderPartial('list/listdata');?>
	</div>
</div>



<?php Yii::app()->clientScript->registerScript('searchList',
"var ajaxUpdateTimeout;
				var ajaxRequest;
				$('.ad_type_link').click(function(){

				$.cookie('bjois_ad_type', $(this).attr('rel'));
				$('.ad_type_link').removeClass('active');
				$(this).addClass('active');

				$('#left_filter_form #ad_type').val($(this).attr('rel'));
				$.ajax({
				type: 'GET',
				dataType: 'json',
				url: '".Yii::app()->createUrl('index/setAdType')."',
				  data: $('#left_filter_form').serialize(),
				  success: function (result) {
						$('.search_setting').html(result.html);
						return false;
}
});

						ajaxRequest = '';
						clearTimeout(ajaxUpdateTimeout);
						ajaxUpdateTimeout = setTimeout(function () {
						$.fn.yiiListView.update(
						// this is the id of the CListView
						'adlist',
						{data: ajaxRequest}
						)
},
						// this is the delay
						300);
});
						$('.treeview a').click(function(e) {
						e.preventDefault();
						ele = $(this);
						$.cookie('bjois_cat_id', $(this).parents('li')[0].id);
						clearTimeout(ajaxUpdateTimeout);
						ajaxUpdateTimeout = setTimeout(function () {
						$.fn.yiiListView.update(
						// this is the id of the CListView
						'adlist',
						{data: ele.attr('href')}
						)
},
						// this is the delay
						300);
							
							
						return false;
});

						$('.city_home').change(function(){
						$.cookie('bjois_city', $(this).val());
						clearTimeout(ajaxUpdateTimeout);
						ajaxUpdateTimeout = setTimeout(function () {
						$.fn.yiiListView.update(
						// this is the id of the CListView
						'adlist',
						{data: ajaxRequest}
						)
},
						// this is the delay
						300);
});"
);?>