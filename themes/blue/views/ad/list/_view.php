<div class="row listview">
	<table class="viewtbl">
		<tbody>
			<tr>
				<td width="10%">
					<div class="image">
					<?php echo CHtml::image(UtilityHtml::getAdFileImageUrl($data->image), $data->title,array('width'=>100,'height'=>100, 'class'=>'img-thumbnail'))?>
					</div>
					<span class="locality">[<?php echo $data->locality?>]</span>
					
					<span class="city">[<?php echo $data->city?>]</span>
				</td>
				<td>
					<div class="col-left">
						<span class="title"><?php echo $data->title?></span>
						<div class="desc"><?php echo UtilityBjois::getSubDesc($data->description)?></div>
					</div>
					<div class="col-right">
						<?php if($data->price > 0):?>
						<span class="badge badge-important price"><span class="WebRupee">Rs.</span><?php echo Yii::app()->numberFormatter->formatCurrency($data->price, '')?></span>
						<?php endif;?>
						<div class="time"><span class="label label-info"><abbr class="timeago" title="<?php echo $data->updated_at?>"><?php echo Yii::app()->dateFormatter->formatDateTime($data->updated_at);?></abbr></span></div>
					</div>
					<div class="bottom">
						<div class="contact">
							<div class="label-data"><?php echo Yii::t('app','Contact')?>:</div>
							<div class="mobile data"><?php echo '+91 '. $data->mobile?></div>
							<div class="email data"><?php echo $data->email?></div>
						</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>