<?php 
Yii::app()->clientScript->registerScript('handle_ajax_function', "
function addLoadingClass(id,result)
{
    $('.loader-ajax').show();
}
function removeLoadingClass(id,result)
{
	$('.loader-ajax').hide();
}
");
?>
<div class="loader-ajax" style="display:none;"></div>
<?php 
		$dataProvider = BjoisAd::model()->searchFront();
		$this->widget('zii.widgets.CListView', array(
				'id' => 'adlist',
				'dataProvider' => $dataProvider,
				'itemView' => 'list/_view',
				'template' => '{items} {pager}', //{pager}
				'loadingCssClass'=> 'adlist-view-loading',
				'beforeAjaxUpdate'=>'addLoadingClass',
				'afterAjaxUpdate'=>'removeLoadingClass',
				/*
				'pager' => array(
						'class' => 'ext.infiniteScroll.IasPager',
						'rowSelector'=>'.row',
						'listViewId' => 'adlist',
						'header' => '',
						'loaderText'=>'Loading...',
						'options' => array('history' => false, 'triggerPageTreshold' => 1, 'trigger'=>'Load more'),
				)
				*/
		)
	       );?>
		<?php
		$this->widget('ext.timeago.JTimeAgo', array(
	    'selector' => '.timeago',

	));
	?>