<?php  $form = $this->beginWidget('GxActiveForm', array(
				'id' => 'left_filter_form',
		));
		echo CHtml::hiddenField('cat_id', Yii::app()->request->getParam('cat_id'), array('id'=>'cat_id'));
		echo CHtml::hiddenField('q', Yii::app()->request->getParam('q'), array('id'=>'q'));
		echo CHtml::hiddenField('ad_type', GlobalData::getCookie('ad_type'), array('id'=>'ad_type'));
		echo CHtml::hiddenField('city',GlobalData::getCookie('city'), array('id'=>'city'));
		?>
		<?php $this->endWidget();	?>

<div>
	
		<?php /* if(GlobalData::getCookie('ad_type')):?>
			<div id="s_ad_type" class="searched-text alert fade in alert-info"><?php echo GlobalData::getAdType(GlobalData::getCookie('ad_type')); ?></div>
		<?php endif; */?>
		<?php if(Yii::app()->request->getParam('q')):?>
			<div class="searched-text alert fade in alert-info"><a class="close" data-dismiss="alert">x</a><?php echo Yii::app()->request->getParam('q'); ?></div>
		<?php endif;?>
		<?php if(Yii::app()->request->getParam('cat_id')):?>
			<div class="searched-text alert fade in alert-info"><a class="close" data-dismiss="alert">x</a><?php echo GlobalData::getCategoryName(Yii::app()->request->getParam('cat_id')); ?></div>
		<?php endif;?>
		
		<?php /* if(GlobalData::getCookie('city')):?>
			<li><span id="city"><label>City:</label><?php echo GlobalData::getCity(GlobalData::getCookie('city')); ?></span></li>
		<?php endif; */?>
	
</div>