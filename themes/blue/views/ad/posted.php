
<div class="main_body">
	<h1>
		<?php echo Yii::t("b", 'Post Free Ad'); ?>
	</h1>
	<div class="message">
		<?php
		foreach(Yii::app()->user->getFlashes() as $key => $message) {
			echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
		}
		?>
	</div>
	<?php if(isset($success) && $success==1): ?>
		<div>Thank You! Your Ad posted successfully!</div>
		<p>Your ad will be live within next 24 hours. if you have any queries regarding it, you can send the mail to <a href="mailto:support@bjois.com">support@bjois.com</a></p>
	<?php else: ?>
		<div>Sorry! Your Ad can not be posted!</div>
		<p>if you have any queries regarding it, you can send the mail to <a href="mailto:support@bjois.com">support@bjois.com</a></p>
	<?php endif;?>
	<div class="new_add">
		<a class="new_add" href="<?php echo CController::createUrl('ad/create')?>">Post New Ad</a>
	</div>
</div>
