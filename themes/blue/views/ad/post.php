<div id="register_box">
	<h1>
		<?php echo Yii::t("b", 'Post Free Ad'); ?>
	</h1>
	<div>
		<?php
		foreach(Yii::app()->user->getFlashes() as $key => $message) {
			echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
		}
		?>
	</div>
	<div class="form">
		<?php  $form = $this->beginWidget('GxActiveForm', array(
				'id' => 'bjois-ad-form',
				//'enableAjaxValidation' => true,
				'clientOptions'=>array('validateOnSubmit'=>true),
		));
		?>

		<p class="note">
			<?php echo Yii::t('app', 'Fields with'); ?>
			<span class="required">*</span>
			<?php echo Yii::t('app', 'are required'); ?>
		</p>

		<?php //echo $form->errorSummary($model); ?>


		<?php //p(GlobalData::getCateogryDropDownList());?>
		<div class="row">
			<?php echo $form->labelEx($model,'category_id'); ?>
			<?php /* $this->widget('application.extensions.AutoComplete.AutoComplete',array(
					'name'=>CHtml::activeId($model,'category'),
					'hidden_id'=>CHtml::activeId($model,'category_id'),
					'source'=>GlobalData::getAutoCateogryDropDownList(),
					// additional javascript options for the autocomplete plugin
					'options'=>array(
			        'minLength'=>'1',
			    ),
					'htmlOptions'=>array(
			        'style'=>'height:20px;',
			    ),
			));
		*/
		?>
			<?php echo $form->dropDownList($model, 'category_id', GlobalData::getAutoCateogryDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Choose a Category..."),'style'=>'width: 300px;')); ?>
			<?php echo $form->error($model,'category_id'); ?>
		</div>

		<!-- row -->
		<div class="row row-radio">
			<?php echo $form->labelEx($model,'ad_user_type'); ?>
			<div class="radio-list">
				<?php echo $form->radioButtonList($model, 'ad_user_type', $model->getAdUserTypeList(), array('separator'=>' ')); ?>
				<?php echo $form->error($model,'ad_user_type'); ?>
			</div>
		</div>
		<!-- row -->
		<div class="row row-radio">
			<?php echo $form->labelEx($model,'ad_type'); ?>
			<div class="radio-list ad_type">
				<?php echo $form->radioButtonList($model, 'ad_type', $model->getAdTypeList(), array('separator'=>' ')); ?>
				<?php echo $form->error($model,'ad_type'); ?>
			</div>
		</div>
		<!-- row -->
		<div class="row">
			<?php echo $form->labelEx($model,'title'); ?>
			<?php echo $form->textField($model, 'title',array('class'=>'box')); ?>
			<?php echo $form->error($model,'title'); ?>
		</div>
		<!-- row -->
		<div class="row">
			<?php echo $form->labelEx($model,'description'); ?>
			<div class="div_ckeditor">
				<?php
				/* $this->widget('application.extensions.fckeditor.FCKEditorWidget',array(
				 "model"=>$model,                # Data-Model
				"attribute"=>'description',         # Attribute in the Data-Model
				"height"=>'400px',
				"width"=>'70%',
				"toolbarSet"=>'Basic',          # EXISTING(!) Toolbar (see: fckeditor.js)
				"fckeditor"=>Yii::app()->basePath."/../fckeditor/fckeditor.php",
				# Path to fckeditor.php
				"fckBasePath"=>Yii::app()->baseUrl."/fckeditor/",
				# Realtive Path to the Editor (from Web-Root)
				"config" => array("EditorAreaCSS"=>Yii::app()->baseUrl.'/css/index.css',),
				# Additional Parameter (Can't configure a Toolbar dynamicly)
) ); */ ?>



				<?php echo $form->textArea($model,'description',array('class'=>'ckeditor'))?>
				<?php echo $form->error($model,'description'); ?>
			</div>
		</div>





		<!-- row -->
		<div class="row">

			<?php
			$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'#AdPostForm_price',
    'currency'=>'INR',
    'config'=>array(
        'showSymbol'=>true,
        'symbolStay'=>true,
		'precision'=>'',
    )
));
?>

			<?php echo $form->labelEx($model,'price'); ?>
			<?php echo $form->textField($model, 'price',array('class'=>'box')); ?>
			<?php echo $form->error($model,'price'); ?>
		</div>
		<!-- row -->
		<div class="row row-900">
			<?php echo $form->labelEx($model,'photos'); ?>
			<?php  echo $form->hiddenField($model, 'photos'); ?>
			<?php  $this->widget('ext.EAjaxUpload.EAjaxUpload',
					array(
        'id'=>'photos',
        'config'=>array(
               'action'=>Yii::app()->createUrl('ad/upload'),
               'allowedExtensions'=>array("jpg","png","gif"),//array("jpg","jpeg","gif","exe","mov" and etc...
               'sizeLimit'=>10*1024*1024,// maximum file size in bytes
               //'minSizeLimit'=>10*1024*1024,// minimum file size in bytes
               'onComplete'=>"js:function(id, fileName, responseJSON){
        		if($(\"#".GxHtml::activeId($model,'photos')."\").val().length>0) {
        		var str = $(\"#".GxHtml::activeId($model,'photos')."\").val() + ','+ responseJSON.filename;
}else {
        		var str = responseJSON.filename;
}
        		$(\"#".GxHtml::activeId($model,'photos')."\").val(str);
        		if($(\"#AdPostForm_photos\").val().split(',').length >= ".SystemConfig::getValue('allow_no_of_photos_in_ad').") {
        		$('.qq-upload-button').hide();
}
}",
        		 
        		//'messages'=>array(
               //                  'typeError'=>"{file} has invalid extension. Only {extensions} are allowed.",
               //                  'sizeError'=>"{file} is too large, maximum file size is {sizeLimit}.",
               //                  'minSizeError'=>"{file} is too small, minimum file size is {minSizeLimit}.",
               //                  'emptyError'=>"{file} is empty, please select files again without it.",
               //                  'onLeave'=>"The files are being uploaded, if you leave now the upload will be cancelled."
               //                 ),
               //'showMessage'=>"js:function(message){ alert(message); }"
              )
)); ?>
			<?php echo $form->error($model,'photos'); ?>
		</div>
		<!-- row -->
		<div class="row">
			<?php echo $form->labelEx($model,'tags'); ?>
			<?php echo $form->textField($model, 'tags',array('class'=>'box')); ?>
			<?php echo $form->error($model,'tags'); ?>
		</div>



		<BR />
		<!-- row -->
		<div class="row">
			<?php echo $form->labelEx($model,'name'); ?>
			<?php echo $form->textField($model, 'name',array('class'=>'box')); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>

		<!-- row -->
		<div class="row">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php if(Yii::app()->user->id) { 
				echo $model->email; echo $form->hiddenField($model, 'email');
			}
			else echo $form->textField($model, 'email',array('class'=>'box')); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>

		<?php if(!Yii::app()->user->id):?>
		<!-- row -->
		<div class="row row-900">
			<?php echo $form->labelEx($model,'create_account'); ?>
			<?php echo $form->checkbox($model, 'create_account', array('class'=>'is_create_account')); ?>
			<?php echo $form->error($model,'create_account'); ?>
		</div>

		<!-- row -->
		<div class="row">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model, 'password',array('class'=>'box')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>
		<!-- row -->
		<?php endif;?>

		<div class="row">
			<?php echo $form->labelEx($model,'mobile'); ?>
			<span class="prefix">+91</span>
			<?php if(Yii::app()->user->id) { 
				echo $model->mobile; echo $form->hiddenField($model, 'mobile');
			}
			else echo $form->textField($model, 'mobile',array('class'=>'box')); ?>
			<?php echo $form->error($model,'mobile'); ?>
		</div>
		<!-- row -->

		<div class="row row-900">
			<a href="javascript:void(0);" id="add_landline"> (+) Add Phone No.</a>
		</div>
		<div class="row" id="landline_if" style="display: none;">
			<?php echo $form->labelEx($model,'phone'); ?>
			<span class="prefix">+91</span>
			<?php echo $form->textField($model, 'phone',array('class'=>'box')); ?>
			<?php echo $form->error($model,'phone'); ?>
		</div>
		<script>
$('#add_landline').click(function() {
	if($(this).html()==' (+) Add Phone No.') {
		$('#landline_if').fadeIn();
		$(this).html(' (-) Remove Phone No.');
	}else {
		$('#AdPostForm_landline').val('');
		$('#landline_if').fadeOut();
		$(this).html(' (+) Add Phone No.');
	}
});
</script>
		<!-- row -->
		<?php /* 
			<div class="row">
			<?php echo $form->labelEx($model,'state_id'); ?>
			<?php echo $form->dropDownList($model, 'state_id', GxHtml::listDataEx(StateMaster::model()->findAllAttributes(null, true))); ?>
			<?php echo $form->error($model,'state_id'); ?>
			</div>
	*/?>

		<!-- row -->
		<div class="row">
			<?php echo $form->labelEx($model,'city_id'); ?>
			<?php echo $form->dropDownList($model, 'city_id', GlobalData::getAutoCityDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Choose a City..."),'style'=>'width: 300px;')); ?>
			<?php echo $form->error($model,'city_id'); ?>
		</div>


		<!-- row -->
		<div class="row">
			<?php echo $form->labelEx($model,'locality'); ?>
			<?php echo $form->textField($model, 'locality',array('class'=>'box')); ?>
			<?php echo $form->error($model,'locality'); ?>
		</div>




		<div class="row">
			<?php echo $form->labelEx($model,'address'); ?>
			<?php echo $form->textField($model, 'address', array('maxlength' => 255,'class'=>'box')); ?>
			<?php echo $form->error($model,'address'); ?>
		</div>

		<div class="row buttons">
			<?php echo GxHtml::submitButton(Yii::t('app', 'Save'), array('class'=>'metro_btn_link')); ?>
		</div>
		<?php $this->endWidget();	?>

	</div>
	<!-- form -->

	<script type="text/javascript"> 
	//$(".chzn-select").chosen();
	$('#AdPostForm_password').parent('.row').hide(); 
	$(".chzn-select-deselect").chosen({allow_single_deselect:true});
	$(".is_create_account").click(function() {
		if($(this).attr('checked')=='checked')		$('#AdPostForm_password').parent('.row').show();
		else $('#AdPostForm_password').parent('.row').hide();
	}); 
</script>
</div>