<div style="width: 750px;">
<?php $dpMain = BjoisCategory::model()->getCategoryListFromParent(0);?>
<?php foreach($dpMain as $row):?>
<?php if($dpSub = BjoisCategory::model()->getCategoryListFromParent($row->id)):?>
<div class="cat-block">
	<h3 style="background-color: <?php echo $row->color?> "><?php echo CHtml::link(CHtml::encode($row->name), array('view', 'id'=>$row->id))?></h3>
	<ul>
		<?php foreach($dpSub as $rowSub):?>
		<li><?php echo CHtml::link(CHtml::encode($rowSub->name), array('view', 'id'=>$rowSub->id)); ?></li>
		<?php endforeach;?>
	</ul>
</div>
<?php endif;?>
<?php endforeach;?>
</div>