 
<div id="login_box">
<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
		'Login',
);
?>

<h2>Log In</h2>


<div class="form"><?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
				'validateOnSubmit'=>true,
		),
)); ?>

<p class="note">Fields with <span class="required">*</span> are
required.</p>

<div class="row"><?php echo $form->labelEx($model,'username'); ?> <?php echo $form->textField($model,'username', array('class'=>'box')); ?>
<?php echo $form->error($model,'username'); ?></div>

<div class="row"><?php echo $form->labelEx($model,'password'); ?> <?php echo $form->passwordField($model,'password',array('class'=>'box')); ?>
<?php echo $form->error($model,'password'); ?>
</div>

<div class="row rememberMe"><?php echo $form->checkBox($model,'rememberMe'); ?>
<?php echo $form->label($model,'rememberMe'); ?> <?php echo $form->error($model,'rememberMe'); ?>
</div>


<div class="clear"></div>
<div class="row buttons"><?php echo CHtml::submitButton('Login', array('class'=>'metro_btn_link')); ?></div>

<?php $this->endWidget(); ?></div>
<!-- form -->
</div>



