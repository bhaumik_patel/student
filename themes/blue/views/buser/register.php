<div id="register_box">
	<h2>
		<?php echo Yii::t("b", 'Registration'); ?>
	</h2>
	<div>
		<?php
		foreach(Yii::app()->user->getFlashes() as $key => $message) {
			echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
		}
		?>
	</div>
	<div class="form">

		<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'registration-form-register-form',
				//'enableAjaxValidation' => true,
				'clientOptions'=>array('validateOnSubmit'=>true),
)); ?>

		<p class="note">
			Fields with <span class="required">*</span> are required.
		</p>

		<?php //echo $form->errorSummary($model); ?>

		<div class="row">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email' ,array('class'=>'box')); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'firstname'); ?>
			<?php echo $form->textField($model,'firstname',array('class'=>'box')); ?>
			<?php echo $form->error($model,'firstname'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'lastname'); ?>
			<?php echo $form->textField($model,'lastname',array('class'=>'box')); ?>
			<?php echo $form->error($model,'lastname'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'mobile'); ?>
			<span class="prefix">+91</span>
			<?php echo $form->textField($model, 'mobile',array('class'=>'box mobile')); ?>
			<?php echo $form->error($model,'mobile'); ?>
		</div>

		<!-- row -->
		<div class="row">
			<?php echo $form->labelEx($model,'city'); ?>
			<?php echo $form->dropDownList($model, 'city', GlobalData::getAutoCityDropDownList(),array('class'=>'chzn-select-city', 'data-placeholder'=>Yii::t('b',"Choose a City..."))); ?>
			<?php echo $form->error($model,'city'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model, 'interests'); ?>
			<?php // echo $form->textField($model, 'interests'); ?>
			<?php echo $form->dropDownList($model, 'interests', GlobalData::getAutoCateogryDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Select Your Interest..."),'multiple'=>'multiple', 'style'=>'width: 300px;')); ?>
			<?php //echo $form->dropDownList($model, 'interests', GlobalData::getAutoCateogryDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Choose a Category..."),'style'=>'width: 400px;')); ?>

			<?php echo $form->error($model,'interests'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->textField($model,'password' ,array('class'=>'box')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'verifyPassword'); ?>
			<?php echo $form->textField($model,'verifyPassword' ,array('class'=>'box')); ?>
			<?php echo $form->error($model,'verifyPassword'); ?>
		</div>


		<BR />
		<?php if($model->enableCaptcha()): ?>
		<div class="row">
			<div class="captcha">
			<?php echo CHtml::activeLabel($model, 'validacion', array('class'=>'captch_label')); ?>
			<?php $this->widget('application.extensions.recaptcha.EReCaptcha', 
			   array('model'=>$model, 'attribute'=>'validacion',
			         'theme'=>'red', 'language'=>'es_ES', 
			         'publicKey'=>'6Lc_4OISAAAAAIE2phlU185VG1ApB3pvDcfP3VOu')) ?>
			<?php echo CHtml::error($model, 'validacion'); ?>
			
			
				<?php /* 
				<?php uniqid(); $this->widget('CCaptcha', array('showRefreshButton'=> false, 'clickableImage'=>true,
						'imageOptions'=>array('id'=>'cap_img', 'title'=>'Click to Change Code', 'style'=>'cursor:pointer;'))); ?>
				<div class="captcha-text">
					<?php echo $form->textField($model,'secureCode',array('class'=>'box cpatchabox')); ?>
				</div>
				*/ ?>
			</div>
			<?php //echo $form->error($model,'secureCode'); ?>
		</div>

		<div class="row chekbox_row">
			<?php $model->is_newsletter=1;?>
			<?php echo $form->checkbox($model,'is_newsletter'); ?>
			<?php echo $model->getAttributeLabel('is_newsletter')?>
			<?php echo $form->error($model,'is_newsletter'); ?>
		</div>

		<div class="row chekbox_row">
			<?php $model->terms_conditions=1; //echo $form->labelEx($model,'terms_conditions'); ?>
			<?php echo $form->checkbox($model,'terms_conditions'); ?>
			<?php echo Yii::t('b','Yes, I am agree for the ')?>
			<?php echo CHtml::link(Yii::t('b','Terms &amp; Conditions'),array('page/view' , array('name'=>'terms')))?>
			<?php echo $form->error($model,'terms_conditions'); ?>
		</div>

		<div class="row buttons">
			<?php echo CHtml::submitButton('Submit', array('class'=>'metro_btn_link')); ?>
		</div>
		<?php endif;?>

		<?php $this->endWidget(); ?>

	</div>
	<!-- form -->


<script type="text/javascript"> 
jQuery(function($) {
	$(".chzn-select-deselect").chosen({allow_single_deselect:true});
	$(".chzn-select-city").chosen({allow_single_deselect:true});
});
	$('#cap_img').trigger('click');
</script>
</div>
