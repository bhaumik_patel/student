<h1>
	<?php echo Yii::t("b", 'Confirmation required'); ?>
</h1>
<div class="message">
	<?php
	foreach(Yii::app()->user->getFlashes() as $key => $message) {
		echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	}
	?>
</div>
<span>Thank You!</span>
<p>
	We,
	<?php echo SystemConfig::getValue('site_name')?>
	have sent you the verification email to your given email address.
	Please check your email and click on the verify link to activate your
	account.

</p>
<p>You can access your account after verifying by email.</p>
<p>
	if you don't get any email from
	<?php echo SystemConfig::getValue('site_name')?>
	, Please click on the below link to resend the verification email.
</p>
<a href="<?php echo $resendLink?>">Resend Verification email</a>
