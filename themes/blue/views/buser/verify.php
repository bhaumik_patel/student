<h1>
	<?php echo Yii::t("b", 'Your Registration is verified'); ?>
</h1>
<div class="message">
	<?php
	foreach(Yii::app()->user->getFlashes() as $key => $message) {
		echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	}
	?>
</div>
<span>Thank You!</span>
<p>
	We are pleased to inform that Your account is verified to
	<?php echo SystemConfig::getValue('site_name')?>
	.
</p>
<p>You can access your account, manage your profiles, ad and more..</p>
