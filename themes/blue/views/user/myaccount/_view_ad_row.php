<div class="view">
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	 | 
	<?php echo CHtml::encode(GxHtml::valueEx($data->category)); ?>
	|
	<?php echo CHtml::encode($data->ad_user_type); ?>
	|
	<?php echo CHtml::encode($data->ad_type); ?>
	|
	<?php echo CHtml::encode($data->title); ?>
	|
	<?php echo CHtml::encode(GxHtml::valueEx(substr($data->description,0,255))); ?>
	|
	<?php echo CHtml::encode($data->status); ?>
</div>