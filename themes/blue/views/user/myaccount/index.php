<div id="right_1" class="tab_profile">
	<?php $this->renderPartial('_profile')?>
</div>
<div id="right_2" class="tab_profile">
	<?php //$this->renderPartial('_change_pwd')?>
</div>
<div id="right_3" class="tab_profile">
	<?php //$this->renderPartial('_ad_list')?>
</div>
<script>
	$('.tab_profile').hide();
	$('#right_1').fadeIn();
	$('#right_3').fadeIn();
	$('.left_link li span').click(function() {
		str = $(this).attr('id');
		$(this).parent('li').parent('ul').children('li').removeClass('active');
		$(this).parent('li').addClass('active');
		$('.tab_profile').fadeOut();
		$('#'+str.replace('left_','right_')).fadeIn();
	});
</script>
