<div id="register_box">
	<h2>My Account &raquo; AD Manager</h2>
	<?php 
	$this->breadcrumbs = array(
			$model->label(2) => array('index'),
			Yii::t('app', 'Manage'),
	);
	//Yii::app()->clientScript->scriptMap['jquery.js'] = false;
	?>


	<?php $this->widget('bootstrap.widgets.TbExtendedGridView', array(
			//$this->widget('zii.widgets.grid.CGridView', array(
			'id' => 'bjois-ad-grid',
			'template' => "{items}\n{extendedSummary}\n{pager}",
			'dataProvider' => $model->searchByUser(),
			'filter' => $model,
			'ajaxUrl'=> 'adList',
			'sortableRows'=>true,
			'sortableAction'=> '*',
			
				
			'columns' => array(
					'id',

					array(
							'name'=>'category_id',
							'value'=>'GxHtml::valueEx($data->category)',
							'filter'=>GxHtml::listDataEx(BjoisCategory::model()->findAllAttributes(null, true)),
					),
					array(
							'name'=>'ad_type',
							'value'=>'UtilityBjois::getAdType($data->ad_type)',
							'filter' => UtilityBjois::getAdtypes(),
					),
					'title',

					array(
							'name' => 'is_approved',
							'value' => '($data->is_approved == 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
							'filter' => array(0 => Yii::t('app', 'No'), 1 => Yii::t('app', 'Yes')),
					),
					'updated_at',
					
					array(
							'class' => 'CButtonColumn',
							'template'=>'{view}',
							'buttons'=>array
							(
									'view' => array
									(
											'url'=>'Yii::app()->createUrl(\'user/myaccount/ad\', array(\'id\'=>$data->id))',
									),
									 

							),
					),
			),
)); ?>
</div>
