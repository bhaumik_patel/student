<div id="register_box">
	<h2>My Account &raquo; Profile</h2>

	<?php $model = new ProfileForm(); 
$model->initData();?>
		
		<div style="display:none" class="loader-profile"><?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/loading.gif')?></div>
		<div class="row alert in alert-block fade alert-success"
			style="display: none;">
			<?php
			foreach(Yii::app()->user->getFlashes() as $key => $message) {
		echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	}
	?>
		</div>

		<div class="form">

			<?php  $form=$this->beginWidget('GxActiveForm', array(
					'id'=>'profile-form',
					//'enableAjaxValidation' => true,
					'clientOptions'=>array('validateOnSubmit'=>true),
)); ?>

			<div class="row">
				<?php echo $form->labelEx($model,'email'); ?>
				<?php echo $form->textField($model,'email' ,array('class'=>'box')); ?>
				<?php echo $form->error($model,'email'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'firstname'); ?>
				<?php echo $form->textField($model,'firstname' ,array('class'=>'box')); ?>
				<?php echo $form->error($model,'firstname'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'lastname'); ?>
				<?php echo $form->textField($model,'lastname',array('class'=>'box')); ?>
				<?php echo $form->error($model,'lastname'); ?>
			</div>
			<div class="row">
				<?php echo $form->labelEx($model,'mobile'); ?>
				<span class="prefix">+91</span>
				<?php echo $form->textField($model,'mobile',array('class'=>'box')); ?>
				<?php echo $form->error($model,'mobile'); ?>
			</div>
			<!-- row -->
			<div class="row">
				<?php echo $form->labelEx($model,'city_id'); ?>
				<?php echo $form->dropDownList($model, 'city_id', GlobalData::getAutoCityDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Choose a City..."),'style'=>'width: 200px;')); ?>
				<?php echo $form->error($model,'city_id'); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model, 'interests'); ?>
				<?php // echo $form->textField($model, 'interests'); ?>
				<?php echo $form->dropDownList($model, 'interests', GlobalData::getAutoCateogryDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Select Your Interest..."),'multiple'=>'multiple', 'style'=>'width: 400px;')); ?>
				<?php //echo $form->dropDownList($model, 'interests', GlobalData::getAutoCateogryDropDownList(),array('class'=>'chzn-select-deselect', 'data-placeholder'=>Yii::t('b',"Choose a Category..."),'style'=>'width: 400px;')); ?>

				<?php echo $form->error($model,'interests'); ?>
			</div>




			<div class="row buttons">
				<?php echo CHtml::ajaxSubmitButton('Update', 
						$this->createUrl('/user/myaccount/AjaxSave'),
						array('update'=>'#update-save-info',
					  'dataType'=>'json',
					  'type'=>'POST',
						'beforeSend'=>"js:function(){
								$('.loader-profile').fadeIn();
								$('.alert-block').hide();
}",
								'success'=>"function(resp) {
								$('.alert-block').html(resp.msg);
								$('.alert-block').fadeIn();
								$('.loader-profile').fadeOut();
								$('#profile-form input[type=\"text\"]').attr(\"readonly\", \"readonly\");
}",
					  ), array('class'=>'metro_btn_link'));?>
			</div>
			<?php $this->endWidget(); ?>
		</div>
		<!-- form -->


		<script>
$('#profile-form input[type="text"]').attr("readonly", "readonly");
$('#profile-form input[type="text"]').click(function() {
    $(this).removeAttr("readonly");
});
$('#profile-form input[type="text"]').focus(function() {
	$(this).removeAttr("readonly");
});
/*
$('#profile-form input[type="text"]').focusout(function() {
    $(this).attr("readonly", "readonly");
});
*/
$(".chzn-select-deselect").chosen({allow_single_deselect:true});
</script>
	</div>