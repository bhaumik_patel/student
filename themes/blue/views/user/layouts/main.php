<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bjois</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
</head>
<body>
<?php echo Yii::app()->bootstrap->registerCoreCss();?>
	<?php echo $this->renderPartial('/../layouts/header')?>

	<div class="contents">
		<div class="left-col">
			<?php echo $this->renderPartial('/layouts/left')?>
		</div>
		<div class="main-col">
			<?php echo $content; ?>
		</div>
	</div>
	<div class="clear"></div>
	<div class="footer">
		<?php echo $this->renderPartial('/../layouts/footer')?>
	</div>
</body>
</html>
