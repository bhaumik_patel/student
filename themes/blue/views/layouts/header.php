<div class="topbar">
	<div class="topbar_in">
		<div class="logo">
			<a class="img-logo" href="<?php echo Yii::app()->homeUrl; ?>"></a>
		</div> 

		<div class="search-area">
		<?php /* 
			<div class="type select-box">
				<select name="select1" id="select1" style="width: 50px;">
					<option>Sell</option>
					<option>Buy</option>
					<option>Offer</option>
					<option>Requirement</option>
				</select>
			</div>
			*/ ?>
			<div class="search-box">
				<?php  
				$form = $this->beginWidget('GxActiveForm', array(
				'id' => 'bjois-search-frm',
				'action'=>Yii::app()->createUrl('ad/search/'),
				'method'=>'get',
				//'enableAjaxValidation' => true,
				//'clientOptions'=>array('validateOnSubmit'=>true),
		));
			?>
				<div class="search_div">
				<?php /* 
					<input class="search_input" type="hidden" name="qtype"
						value="terms" /> <input class="search_input" type="text" name="q"
						id="q" value="Search Your Requirement" autocomplete="off"
						onclick="if(this.value=='Search Your Requirement'){this.value=''}"
						onblur="if(this.value==''){this.value='Search Your Requirement'}" />
						*/?>
						
					<?php $this->widget('bootstrap.widgets.TbTypeahead', array(
    'name'=>'q',
	'value'=>Yii::app()->request->getParam('q'),
   	'htmlOptions'=>array('onclick'=>"if(this.value=='Search Your Requirement'){this.value=''}",
   			"onblur"=>"if(this.value==''){this.value='Search Your Requirement'}",
			"class"=>"search_input", "id"=>"keyword_txt",
),
    'options'=>array(
        'source'=> UtilityBjois::getTopSearchTerms(), //array('Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'),
        'items'=>4,
        'matcher'=>"js:function(item) {
            return ~item.toLowerCase().indexOf(this.query.toLowerCase());
        }",
    ),
)); ?>
					
					
					
					<button class="metro_btn search_btn">
						<span>&nbsp;</span>
					</button>
				</div>
				<?php $this->endWidget();	?>
			</div>

			<div class="logo-city">
				<?php $city = (string)Yii::app()->request->cookies['bjois_city'];?>
				<?php echo CHTML::dropDownList('city_home', $city, GlobalData::getAutoCityDropDownList(),array('class'=>'city_home', 'data-placeholder'=>Yii::t('b',"Choose a City..."),'style'=>'width: 150px;')); ?>
			</div>
			<script type="text/javascript">
				

				
				 
	      </script>
	      	<?php /* 
			<div class="looking-block">
				<span class="looking-span">I'm Looking for</span>
				<?php echo CHtml::ajaxLink('Select',Yii::app()->createUrl('/index/catSelect'),
     array('type'=>'POST', 'update'=>'#preview-cat', 'complete'=>'afterAjax'));?>
			</div>
			*/ ?>
			<!--
        <div class="city-cat select-box">
          <select>
            <option>My City</option>
            <option>Ahmedabad</option>
            <option>Baroda</option>
          </select>
        </div>
        <div class="city-cat select-box">
          <select>
            <option>I'm looking for</option>
            <option>Computer</option>
            <option>Mobile</option>
          </select>
        </div>
        -->

		</div>

		<div class="link-box">
			<?php 

			$this->widget('bootstrap.widgets.TbButtonGroup', array(
		'size'=>'medium',
		'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
		'buttons'=>array(
				array('label' => 'Post Free Ad',
				'items'=>array(
						array('label'=>'Sell', 'url'=> Yii::app()->createUrl('ad/create',array('type'=>'sell'))),
						array('label'=>'Buy', 'url'=>Yii::app()->createUrl('ad/create',array('type'=>'buy'))),
						array('label'=>'Offer', 'url'=>Yii::app()->createUrl('ad/create',array('type'=>'offer'))),
						array('label'=>'Requirement', 'url'=>Yii::app()->createUrl('ad/create',array('type'=>'requiremnt'))),
				)),
		),
));
?>
			<?php /* 
<a
href="<?php echo Yii::app()->createUrl('ad/create',array('type'=>'sell'))?>"
class="metro_btn_link"><span>Post Free Ad</span> </a> <a
href="<?php echo Yii::app()->createUrl('ad/create',array('type'=>'buy'))?>"
class="metro_btn_link"><span>Post Free Requirement</span> </a>
		*/ ?>
		</div>
		<div class="login-area">
			<div class="account-link">
				<span><?php echo UserModule::getWelcomeText();?> </span> <a
					href="<?php echo UserModule::getLoginUrl(); ?>"><?php echo UserModule::getLoginText(); ?>
				</a> | <a href="<?php echo UserModule::getRegisterUrl(); ?>"><?php echo UserModule::getRegisterText(); ?>
				</a>
			</div>
		</div>
	</div>
</div>





<?php
$this->widget('application.extensions.fancybox.EFancyBox', array());
//create a link


//put fancybox on page
?>
<script>
jQuery(function($) {
	$(".select-box select").selectbox({
		effect: "fade"
	});
	function afterAjax()
	{
		$.fancybox({
	        href : '#preview-cat',
	        scrolling : 'no',
	        transitionIn : 'fade',
	        transitionOut : 'fade', 
	        //check the fancybox api for additonal config to add here   
	        onClosed: function() { $('#preview-cat').html(''); }, //empty the preview div
	});
	}

	$(".logo").click(function() {  window.location.href="<?php echo Yii::app()->homeUrl; ?>"; }); 
	$(".city_home").chosen({allow_single_deselect:true});


	//city selection
	$('.city_home').on('change', function(evt, params) {
		    $.ajax({
	    	  type: "POST",
	    	  dataType: "json",
	    	  url: "<?php echo Yii::app()->createUrl('index/setCity')?>",
	    	  data: { city: params.selected },
			  success: function (result) {
				  if($('.search_setting').length>0) {
				  	$('.search_setting').html(result.html);
				  }
				  	return false;  
			  }
	 	});
	});

	$('#bjois-search-frm .search_btn').click(function() {
		if($('#bjois-search-frm #keyword_txt').val().trim()=='Search Your Requirement') {
			return false;
		}	
	});
});
</script>

<div style="display: none;">
	<div id="preview-cat">
		<!-- space here will be updated by ajax -->
	</div>
</div>

