<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bjois</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<?php echo Yii::app()->bootstrap->registerCoreCss();?>
<script src="http://cdn.webrupee.com/js" type="text/javascript"></script>
</head>
<body lang="en">
	<?php echo $this->renderPartial('/layouts/header')?>

	<div class="contents">
		<?php echo $content; ?>
	</div>
	<div class="clear"></div>
	<a href="#" class="scrollup" title="Go Top">Scroll</a>

	<div class="footer">
		<?php echo $this->renderPartial('/layouts/footer')?>
	</div>
</body>
<script type="text/javascript">
    $(document).ready(function(){ 
 
        $(window).scroll(function(){
            if ($(this).scrollTop() > 100) {
                $('.scrollup').fadeIn();
            } else {
                $('.scrollup').fadeOut();
            }
        }); 
 
        $('.scrollup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        });
 
    });
</script>
</html>
