<?php if($topSearchs = BjoisSearchKeyword::model()->getLastSearchLog()):?>
      <div class="cat-block popular-block">
        <h3>Most Search Terms</h3>
        <ul>
      	<?php foreach ($topSearchs as $search):?>
      		<li class="pop<?php echo rand(0,5)?>"><?php echo Chtml::link($search->keyword, Yii::app()->createUrl('ad/search', array('q'=>$search->keyword,'qtype'=>'terms')))?></li>
      	<?php endforeach;?>
      	</ul>
      </div>
      <?php endif;?>