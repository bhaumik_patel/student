<div id="templatemo_middle_wrapper">
	<div id="templatemo_middle" class="wrapper">
        <div id="templatemo_slider">
        	<div id="carousel">
                <div class="panel">
                    
                    <div class="details_wrapper">
                        
                        <div class="details">
                        
                            <div class="detail">
                                <h2><a href="#">Dolor sit amet</a></h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec orci erat. Vestibulum id bibendum arcu. Curabitur eleifend felis sit amet orci eleifend in dignissim urna vestibulum. </p>
                                <a href="#" title="Read more" class="slider_more">Read more</a>
                            </div><!-- /detail -->
                            
                            <div class="detail">
                                <h2><a href="#">Lorem ipsum dolor</a></h2>
                                <p> Donec nec orci vel risus condimentum suscipit dapibus quis justo. Maecenas ipsum justo, imperdiet suscipit fringilla ut, varius ac leo.</p>
                                <a href="#" title="Read more" class="slider_more">Read more</a>
                            </div><!-- /detail -->
                            
                            <div class="detail">
                                <h2><a href="#">Aenean massa cum sociis</a></h2>
                                <p> Nam non lectus a nunc fringilla aliquet ac id lectus. Proin molestie iaculis lorem at convallis. Duis est nibh, dapibus eu tempor ac, varius in mauris.</p>
                                <a href="#" title="Read more" class="slider_more">Read more</a>
                            </div><!-- /detail -->
                            
                        
                        </div><!-- /details -->
                        
                    </div><!-- /details_wrapper -->
                    
                </div><!-- /panel -->

                    <a href="javascript:void(0);" class="previous" title="Previous" >Previous</a>
                    <a href="javascript:void(0);" class="next" title="Next">Next</a>
			
            <div id="slider-image-frame">
                <div class="backgrounds">
                    
                    <div class="item item_1">
                        <img src="images/slider/box.png" alt="image" />
                    </div><!-- /item -->
                    
                    <div class="item item_2">
                        <img src="images/slider/iMac.png" alt="image" />
                    </div><!-- /item -->
                    
                    <div class="item item_3">
                        <img src="images/slider/lock.png" alt="image" />
                    </div><!-- /item -->
                    
                </div><!-- /backgrounds -->
			</div>
		</div>
        </div> <!-- END of templatemo_slider -->
        
    </div>
    
</div>