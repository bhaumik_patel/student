<?php
if (AdminModule::getUserDataByKey('user_type') == 'superadmin') {
    $this->breadcrumbs = array(
            $model->label(2) => array('admin'),
            GxHtml::valueEx($model),
    );
}

/*$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);*/
if (AdminModule::getUserDataByKey('user_type') == 'superadmin') {
$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage Branch')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create Branch')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/update.png'), 'url'=>array('update', 'id' => $model->id),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Update Branch')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/delete.png'), 'url'=>array('create'),  'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?'),'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Delete Branch')), 
	);
}
?>
<!--h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1-->

<?php  $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'branch_name',
'address',
'contact_no',
array('name'=>'created_at', 'type'=>'html', 'value'=> CustomFunction::dateFormat($model->created_at)),
array('name'=>'updated_at', 'type'=>'html', 'value'=> CustomFunction::dateFormat($model->updated_at)),
array('name'=>'status', 'type'=>'html', 'value'=>  UtilityHtml::getStatusImageIcon($model->status)),
	),
));?>
<?php /*
<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo $model->label(2)?></h3>
   </div>
<div class="body" style="z-index: 690; ">

<?php  $form=$this->beginWidget('CActiveForm', array('id'=>'dream-branch-form','enableAjaxValidation'=>false,)); ?>
       
	   <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'id'); ?></span></label> 
              <?php echo $model->id;?>
	         <div class="clear" style="z-index: 670; "></div>
        </div> 
		
	   <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'branch_name'); ?></span></label> 
              <?php echo $model->branch_name;?>
	         <div class="clear" style="z-index: 670; "></div>
        </div>  
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'address'); ?></span></label> 
			  <?php echo $model->address; ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>       
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('contact_no')?></span></label> 
             <?php echo $model->contact_no;?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('created_at')?></span></label> 
              <?php echo $model->created_at;?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
               
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'updated_at'); ?></span></label>           
					<?php echo $model->updated_at;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
         <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'status'); ?></span></label> 
             <?php echo $model->status== "1" ? "Active": "InActive"?>
           <div class="clear" style="z-index: 670; "></div>
        </div>        
        <?php $this->endWidget();?>
</div>
</div>
*/?>

<?php /*?>
<h2><?php echo GxHtml::encode($model->getRelationLabel('dreamBatches')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->dreamBatches as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('dreamBatch/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('dreamEvents')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->dreamEvents as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('dreamEvent/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('dreamStudents')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->dreamStudents as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('dreamStudent/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentPayments')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->dreamStudentPayments as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('dreamStudentPayment/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>
<?php */?>