<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo $model->label(2)?></h3>
   <div class="shortcuts-icons" style="z-index: 700; "></div>
   </div>
<div class="body" style="z-index: 690; ">

<?php  $form=$this->beginWidget('CActiveForm', array('id'=>'dream-branch-form','enableAjaxValidation'=>false,)); ?>     
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'branch_name'); ?></span></label>              
			 <?php echo $form->textField($model, 'branch_name', array('maxlength' => 60)); ?>	    	
		   <?php echo $form->error($model,'branch_name',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
        
         <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'address'); ?></span></label>           
					<?php echo $form->textArea($model,'address',array('rows'=>6, 'cols'=>50,'maxlength'=>250)); ?>
					<?php echo $form->error($model,'address',array('class'=>'error_position')); ?>
					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
               
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'contact_no'); ?></span></label> 
             <?php echo $form->textField($model, 'contact_no', array('maxlength' => 15,'class'=>'st-forminput','size'=>32)); ?>
             <?php echo $form->error($model,'contact_no',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'status'); ?></span></label>             
			<?php echo $form->dropDownlist($model,'status',array('1'=>'Active','0'=>'InActive')); ?>	     
		    <?php echo $form->error($model,'status',array('class'=>'error_position')); ?>
           <div class="clear" style="z-index: 670; "></div>
        </div>
        
         <div class="button-box" style="z-index: 460;">
				<?php echo GxHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',  array('class'=>'st-button save_continue'))?>	
				<?php echo GxHtml::resetButton(Yii::t('app', 'Reset'),array('class'=>'st-button'));?>
		</div>
        <?php $this->endWidget();?>
</div>
</div>

<?php /* ?>
<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'dream-branch-form',
	'enableAjaxValidation' => true,
));
?>
	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'branch_name'); ?>
		<?php echo $form->textField($model, 'branch_name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'branch_name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textArea($model, 'address'); ?>
		<?php echo $form->error($model,'address'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'contact_no'); ?>
		<?php echo $form->textField($model, 'contact_no', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'contact_no'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamBatches')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamBatches', GxHtml::encodeEx(GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamEvents')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamEvents', GxHtml::encodeEx(GxHtml::listDataEx(DreamEvent::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudents')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudents', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentPayments')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudentPayments', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudentPayment::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form --><?php */?>