<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo $model->label(2)?></h3>
   <div class="shortcuts-icons" style="z-index: 700; "><a class="shortcut tips" href="#" original-title="Create GeneralSettings"><img src="<?php echo UtilityHtml::getAdminSkinUrl() ?>/img/icons/shortcut/question.png" width="25" height="25" alt="icon"></a></div>
   </div>
<div class="body" style="z-index: 690; ">
	<?php $form = $this->beginWidget('GxActiveForm', array(
		'id' => 'system-config-form',
		'enableAjaxValidation' => false,
	));
	?>
	<?php echo $form->errorSummary($model); ?>
	
		<?php /* 
				<div class="row">
				<?php echo $form->labelEx($model,'system_section_id'); ?>
				<?php echo $form->dropDownList($model, 'system_section_id', GxHtml::listDataEx(SystemSection::model()->findAllAttributes(null, true, 'status=1'))); ?>
				<?php echo $form->error($model,'system_section_id'); ?>
				</div><!-- row -->
				<div class="row">
				<?php echo $form->labelEx($model,'system_group_id'); ?>
				<?php echo $form->dropDownList($model, 'system_group_id', GxHtml::listDataEx(SystemGroup::model()->findAllAttributes(null, true, 'status=1'))); ?>
				<?php echo $form->error($model,'system_group_id'); ?>
				</div><!-- row -->
		
		<div class="st-form-line" style="z-index: 680; ">
			<label><span class="st-labeltext"><?php //echo $form->labelEx($model,'name'); ?></span></label>
			<?php echo $model->name; //$form->textField($model, 'name', array('maxlength' => 100)); ?>
			<?php echo $form->error($model,'name'); ?>
			<div class="clear" style="z-index: 670; "></div>
		</div>
		*/ ?>
		
		<div class="st-form-line" style="z-index: 680; ">
			<label><span class="st-labeltext"><?php echo SystemConfig::getConvertName($model->name); ?></span></label>
			<?php echo $form->textArea($model, 'value',array('rows'=>6, 'cols'=>50,'maxlength'=>150)); ?>
			<?php echo $form->error($model,'value'); ?>
			<div class="clear" style="z-index: 670; "></div>
		</div>
		
		<?php /* 
				<div class="row">
				<?php echo $form->labelEx($model,'input_type'); ?>
				<?php echo $form->textArea($model, 'input_type'); ?>
				<?php echo $form->error($model,'input_type'); ?>
				</div><!-- row -->
				
				<div class="row">
				<?php echo $form->labelEx($model,'input_options'); ?>
				<?php echo $form->textArea($model, 'input_options'); ?>
				<?php echo $form->error($model,'input_options'); ?>
				</div><!-- row -->
				
				<div class="row">
				<?php echo $form->labelEx($model,'status'); ?>
				<?php echo $form->dropDownList($model,'status', array('1'=>Yii::t('app','Active'),'0'=>Yii::t('app','Inactive'))); ?>
				<?php echo $form->error($model,'status'); ?>
				</div><!-- row -->
		*/ ?>
		
		<div class="st-form-line" style="z-index: 680; ">
			<label><span class="st-labeltext"><?php echo $form->labelEx($model,'position'); ?></span></label>
			<?php echo $form->textField($model, 'position'); ?>
			<?php echo $form->error($model,'position',array('class'=>'error_position')); ?>
			<div class="clear" style="z-index: 670; "></div>
		</div>

		<div class="button-box" style="z-index: 460;">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Save'),  array('class'=>'st-button save_continue'));
			  echo GxHtml::resetButton(Yii::t('app', 'Reset'),array('class'=>'st-button'));?>
		</div>
		<?php $this->endWidget();?>
		
</div><!-- form -->
</div>