<?php
$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	Yii::t('app', 'Manage'),
);

$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/question.png'), 'url'=>array('#'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage GeneralSetting')), 	 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/refresh.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Refresh')),
);
	
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('system-config-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php
$grid_id = 'system-config-grid';
$template = '{update}';
if($this->userType=='admin')
	$template = '{update}'; 
?>

<?php 
	$systsectData = GxHtml::listDataEx(SystemSection::model()->findAllAttributes(null, true, 'status=1'));
	$systsectData = array_replace_recursive(array(null=>'Please Select'), $systsectData);

	$systgroupData = GxHtml::listDataEx(SystemGroup::model()->findAllAttributes(null, true, 'status=1'));
	$systgroupData = array_replace_recursive(array(null=>'Please Select'), $systgroupData);
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(

	'id' => $grid_id,
	'itemsCssClass'=>'tablesorter',
	'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' .CHtml::dropDownList('pageSize', UtilityHtml::getPageSize(), UtilityHtml::getPageSizeArray(),array('class'=>'change-pageSize')) . ' Rows per page',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(

		/*
		array(
			'name'=> 'id',
		    'header'=>Yii::t("messages", 'Id'), 
			'value'=>'GxHtml::valueEx($data, \'id\')',
			//'headerHtmlOptions'=> array('style'=> 'width: 35px; margin-right:15px;'),
			),
		*/
		array(
				'name'=>'system_section_id',
				'header'=>Yii::t("messages", 'System Section'),
				'value'=>'GxHtml::valueEx($data, \'systemSection\')', 
				'filter'=>$systsectData,
				),
		array(
				'name'=>'system_group_id',
				'header'=>Yii::t("messages", 'System Group'),
				'value'=>'GxHtml::valueEx($data, \'systemGroup\')', 
				'filter'=>$systgroupData,
				),
		
		array(
				'name'=>'name',
				'header'=>Yii::t("messages", 'Name'),
				'value'=>'SystemConfig::getConvertName($data->name)',
				),
	
		array(
				'name'=>'value',
				'header'=>Yii::t("messages", 'Value'),
				'value'=>'GxHtml::valueEx($data, \'value\')',
				),
		/*
		array(
			'name'=>'status',
			'type' => 'html',
			'filter'=> UtilityHtml::getStatusArray(),
			'value'=> 'CHtml::tag("div",  array("style"=>"text-align: center" ) , CHtml::tag("img", array( "src" => UtilityHtml::getStatusImage(GxHtml::valueEx($data, \'status\')))))',
		),
		*/
		//'input_type',
		/*
		'input_options',
		'status',
		'position',
		*/
		
		array(
			'class' => 'CButtonColumn',
			'template'=>$template,
			
		),
	),
)); ?>

<?php Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }})
    });
EOD
,CClientScript::POS_READY); ?>