<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	//GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Update'),
);

$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/question.png'), 'url'=>array('#'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage GeneralSetting')), 	 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/refresh.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Refresh')),
	);
?>

<h1><?php //echo Yii::t('app', 'Update') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>
<h1><?php //echo Yii::t('app', 'Update SystemConfig')  ?></h1>
<?php
$this->renderPartial('_form', array(
		'model' => $model));
?>