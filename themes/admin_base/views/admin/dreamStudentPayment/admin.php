<?php
$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	Yii::t('app', 'Manage'),
);

/*$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);*/
$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/question.png'), 'url'=>array('#'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
	//array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage Payment')), 
	//array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create Payment')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/refresh.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Refresh')),
	);	
$grid_id = 'dream-student-payment-grid';
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('$grid_id', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php /*
<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->
*/?>
<?php 
$columns = array(
		'id',
		array(
				'name'=>'stud_id',
				'value'=>'GxHtml::valueEx($data->stud)',
				'filter'=>GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true,array('order'=>'name'))),
				),
		array(
				'name'=>'branch_id',
				'value'=>'GxHtml::valueEx($data->branch)',
				'filter'=>GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true,array('order'=>'branch_name'))),
				),
		array(
				'name'=>'batch_id',
				'value'=>'GxHtml::valueEx($data->batch)',
				'filter'=>GxHtml::listDataEx(DreamBatch::getFilterArray()),
				),
		/*'year',
		'month',*/
                array('name'=>'pay_date', 'type'=>'html', 'value'=> 'CustomFunction::dateFormat($data->pay_date)','filter'=> false,),
                'registration_paid',
		'amount_paid',
		/*
		'amount_paid',
		'created_at',
		'updated_at',
		array(
					'name' => 'status',
					'value' => '($data->status === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		*/
		/*array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'htmlOptions'=>array('width'=>'75px'),
		),*/
		array(
            'class' => 'CButtonColumn',
            'template' => '{view}{delete}',
            'header' => 'Action',
            'htmlOptions' => array('width' => '70px'),
        ),
	);
	if(AdminModule::getUserDataByKey('user_type')=='admin') {
		unset($columns[2]);
	}
	
	
	$this->widget('zii.widgets.grid.CGridView', array(
	'id' => $grid_id,
	'dataProvider' => $model->search(),
	'filter' => $model,
	'itemsCssClass'=>'tablesorter',
	'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize',UtilityHtml::getPageSize(),UtilityHtml::getPageSizeArray(),array('class'=>'change-pageSize')) . ' rows per page',
	'columns' => $columns
)); ?>


<?php Yii::app()->clientScript->registerScript('initPageSize', <<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }});
    });
EOD
        , CClientScript::POS_READY);
?>