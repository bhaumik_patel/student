<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	GxHtml::valueEx($model),
);

$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage Payment')), 
	//array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create Payment')), 
	//array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/update.png'), 'url'=>array('update', 'id' => $model->id),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Update Payment')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/delete.png'), 'url'=>array('create'),  'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?'),'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Delete Payment')), 
	);
/*$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);*/
?>

<!--h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1-->

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'stud',
			'type' => 'raw',
			'value' => $model->stud !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->stud)), array('dreamStudent/view', 'id' => GxActiveRecord::extractPkValue($model->stud, true))) : null,
			),
array(
			'name' => 'batch',
			'type' => 'raw',
			'value' => $model->batch !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->batch)), array('dreamBatch/view', 'id' => GxActiveRecord::extractPkValue($model->batch, true))) : null,
			),
array(
			'name' => 'branch',
			'type' => 'raw',
			'value' => $model->branch !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->branch)), array('dreamBranch/view', 'id' => GxActiveRecord::extractPkValue($model->branch, true))) : null,
			),
/*'year',
'month',*/
 array('name'=>'pay_date', 'type'=>'html', 'value'=> CustomFunction::dateFormat($model->pay_date)),
'amount_paid',
'registration_paid',
array('name'=>'created_at', 'type'=>'html', 'value'=> CustomFunction::dateFormat($model->created_at)),
array('name'=>'updated_at', 'type'=>'html', 'value'=> CustomFunction::dateFormat($model->updated_at)),
//array('name'=>'status', 'type'=>'html', 'value'=>  UtilityHtml::getStatusImageIcon($model->status)),
	),
)); ?>

