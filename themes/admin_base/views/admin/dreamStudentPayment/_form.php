<div class="simplebox grid740" style="z-index: 720; ">
    <div class="titleh" style="z-index: 710; ">
        <h3><?php echo $model->label(2) ?></h3>
        <div class="shortcuts-icons" style="z-index: 700; "></div>
    </div>
    <div class="body" style="z-index: 690; ">

        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'dream-student-payment-form', 'enableAjaxValidation' => false,)); ?>                 
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'name'); ?></span></label> 
            <?php
            $sbatchid = Yii::app()->request->getParam('sbatchid');
            $student_id = '';
            if ($sbatch_data = DreamStudentBatch::model()->findByPk($sbatchid)) {
                $student_id = $sbatch_data->student_id;
                if ($student_id) {
                    if ($student_data = DreamStudent::model()->findByPk($student_id)) {
                        //$batches = DreamBatch::model()->findAll('branch_id = ' . $student_data->branch_id);
                        echo $student_data->name;
                        echo $form->hiddenField($model, 'stud_id', array('value' => $student_id));
                        
                    }
                } else {
                    echo $form->dropDownList($model, 'stud_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)), array('empty' => Yii::t('fim', 'Select Student')));
                }
            }
            echo $form->hiddenField($model, 'stud_batch_id', array('value' => $sbatchid));
            /* if($payment_id!='')
              {
              $student_data = DreamStudent::model()->find('id='.$model->stud_id);
              echo $student_data->name;
              echo $form->HiddenField($model, 'stud_id',array('value'=>$model->stud_id));
              }
              else */
            //echo $form->dropDownList($model, 'stud_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)),array('empty'=>Yii::t('fim','Select Student')));
            ?>
            <?php //echo $form->dropDownList($model, 'stud_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)),array('empty'=>Yii::t('fim','Select Student'))); ?>
            <?php echo $form->error($model, 'stud_id', array('class' => 'error_nameposition')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>	
        <?php if($sbatch_data): ?>
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext">Student Admission Details</span></label>
            <div>
                <?php $this->renderPartial('../dreamStudentBatch/view', array('model'=>$sbatch_data))?>
            </div>
        </div>
        <?php endif;?>
        <?php echo $form->HiddenField($model, 'branch_id'); ?>
        <?php echo $form->HiddenField($model, 'batch_id'); ?>
        	

        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'registration_paid'); ?></span></label> 
            <?php echo $form->textField($model, 'registration_paid', array('maxlength' => 10)); ?>
            <?php echo $form->error($model, 'registration_paid', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'amount_paid'); ?></span></label> 
            <?php echo $form->textField($model, 'amount_paid', array('maxlength' => 10)); ?>
            <?php echo $form->error($model, 'amount_paid', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
        <?php /*
          <div class="st-form-line" style="z-index: 680; ">
          <label><span class="st-labeltext"><?php echo $form->labelEx($model,'year'); ?></span></label>
          <?php //echo $form->textField($model, 'year'); ?>
          <select id="DreamStudentPayment_year" name="DreamStudentPayment_year">
          <option selected value=''>--Select Year--</option>
          <?php
          $year = date("Y");
          $start = 1;$end = 27;
          for($i=$start;$i<$end;$i++)
          {?>
          <option value="<?php echo $year;?>" <?php if($model->year==$year){echo 'selected';}?>><?php echo $year;?></option>
          <?php
          $year++;
          }
          ?>
          </select>
          <?php echo $form->error($model,'year',array('class'=>'error_position')); ?>
          <div class="clear" style="z-index: 670; "></div>
          </div>

          <div class="st-form-line" style="z-index: 680; ">
          <label><span class="st-labeltext"><?php echo $form->labelEx($model,'month'); ?></span></label>
          <?php //echo $form->textField($model, 'month'); ?>
          <?php $month_arr =array('Janaury'=>'Janaury','February'=>'February','March'=>'March','April'=>'April','May'=>'May','June'=>'June','July'=>'July','August'=>'August','September'=>'September','October'=>'October','November'=>'November','December'=>'December');?>
          <select id='DreamStudentPayment_month' name="DreamStudentPayment_month">
          <option selected value=''>--Select Month--</option>
          <?php
          foreach ($month_arr as $key=>$value)
          {?>
          <option value="<?php echo $key;?>" <?php if($model->month==$value){echo 'selected';}?>><?php echo $value;?></option>
          <?php
          }
          ?>
          </select>
          <?php echo $form->error($model,'month',array('class'=>'error_position')); ?>
          <div class="clear" style="z-index: 670; "></div>
          </div> */ ?>

		
		  
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'pay_date'); ?></span></label> 
            <div class="input-append">
                <?php //echo $form->textField($model, 'inquiry_date');   ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'name' => 'pay_date',
                    'attribute' => 'pay_date',
                    'options' => array(
                        //'showAnim'=>'fold',
                        'dateFormat' => 'dd-mm-yy',                        
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;', 'value' => date('d-m-Y')
                    ),
                ));
                ?>
            </div>             
            <?php echo $form->error($model, 'pay_date', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
        
		<div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'Next_payment_date'); ?></span></label> 
            <div class="input-append">
                <?php //echo $form->textField($model, 'inquiry_date');   ?>
                <?php
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    //'model' => $model,
                    'name' => 'next_paydate',
                    'attribute' => 'next_paydate',
                    'options' => array(
                        //'showAnim'=>'fold',
                        'dateFormat' => 'dd-mm-yy',                        
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;', 'value' => date('d-m-Y')
                    ),
                ));
                ?>
            </div>             
            <?php echo $form->error($model, 'next_paydate', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>  
        
        <?php /*
          <div class="st-form-line" style="z-index: 680; ">
          <label><span class="st-labeltext"><?php echo $form->labelEx($model,'status'); ?></span></label>
          <?php echo $form->dropDownlist($model,'status',array('1'=>'Active','0'=>'InActive')); ?>
          <?php echo $form->error($model,'status',array('class'=>'error_position')); ?>
          <div class="clear" style="z-index: 670; "></div>
          </div>

         */ ?>     
        <div class="button-box" style="z-index: 460;">
            <?php echo GxHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'st-button save_continue')) ?>	
            <?php echo GxHtml::resetButton(Yii::t('app', 'Reset'), array('class' => 'st-button')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<script>
    $(document).ready(function () {

<?php if ($student_id != '') { ?>
            ajaxcall();
<?php } ?>
        $('#DreamStudentPayment_stud_id').change(function () {
            ajaxcall();
        });

        /*direct create from student batch*/
        $('#DreamStudentPayment_amount_paid').focusout(function () {
            amount_pay = parseFloat($('#DreamStudentPayment_amount_paid').val());
            total_fee_amount = parseFloat($('#total_fee_amount').val());
            total_paid_amount = parseFloat($('#total_paid_amount').val());
            /*if(total_paid_amount==total_paid_amount)
             {
             alert('Already Paid fees');
             return;
             }*/
            if (amount_pay != 'NaN')
            {
                remaning_amount = total_paid_amount + amount_pay;
            }
            $('#total_paid_amount').val(remaning_amount);
            //$('#stotal_paid_amount').html(remaning_amount);	
        });

    });
    function ajaxcall()
    {
        $('#batch_name').html('');
        $('#branch_name').html('');
        $.ajax({
            type: "POST",
            url: '<?php echo GxHtml::normalizeUrl(array('DreamStudentPayment/ajaxBranchBatch')) ?>',
            data: "stud_id=" + $('#DreamStudentPayment_stud_id').val(),
            dataType: "json",
            success: function (data) {
                if (data.success == 1) {
                    /* if (undefined != data.total_fee_amount)
                     {
                     if(data.total_fee_amount==data.total_paid_amount)
                     {
                     alert('Selected Student paid all amount.Please select other');
                     return;
                     }
                     }   */
                    $('#batch_name').html(data.batch_name);
                    $('#branch_name').html(data.branch_name);
                    $('#stotal_fee_amount').html(data.total_fee_amount);
                    $('#stotal_paid_amount').html(data.total_paid_amount);
                    $('#total_fee_amount').val(data.total_fee_amount);
                    $('#total_paid_amount').val(data.total_paid_amount);
                    $('#DreamStudentPayment_branch_id').val(data.branch_id);
                    $('#DreamStudentPayment_batch_id').val(data.batch_id);

                } else {

                }
            },
            error: function (xhr, tStatus, e) {
                if (!xhr) {
                    var html = "<div class=\"albox errorbox\" style=\"z-index: 500;\">";
                    html += "<h2>" + tStatus + " : " + e.message + "</h2><ul></ul></div>";
                } else {
                    var html = "<div class=\"albox errorbox\" style=\"z-index: 500;\">";
                    html += "<h2>Connection error - Please check the network or api details</h2><ul></ul></div>";
                }
                $('#error_msg').html(html);
                $('#error_msg').show();

                $('#loader_info').hide();

                return false;
            },
        });
    }
</script>
<?php /*
  <div class="form">
  <?php $form = $this->beginWidget('GxActiveForm', array(
  'id' => 'dream-student-payment-form',
  'enableAjaxValidation' => false,
  ));
  ?>

  <p class="note">
  <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
  </p>

  <?php echo $form->errorSummary($model); ?>

  <div class="row">
  <?php echo $form->labelEx($model,'stud_id'); ?>
  <?php echo $form->dropDownList($model, 'stud_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true))); ?>
  <?php echo $form->error($model,'stud_id'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'batch_id'); ?>
  <?php echo $form->dropDownList($model, 'batch_id', GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true))); ?>
  <?php echo $form->error($model,'batch_id'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'branch_id'); ?>
  <?php echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true))); ?>
  <?php echo $form->error($model,'branch_id'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'year'); ?>
  <?php echo $form->textField($model, 'year'); ?>
  <?php echo $form->error($model,'year'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'month'); ?>
  <?php echo $form->textField($model, 'month'); ?>
  <?php echo $form->error($model,'month'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'amount_paid'); ?>
  <?php echo $form->textField($model, 'amount_paid', array('maxlength' => 10)); ?>
  <?php echo $form->error($model,'amount_paid'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'created_at'); ?>
  <?php echo $form->textField($model, 'created_at'); ?>
  <?php echo $form->error($model,'created_at'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'updated_at'); ?>
  <?php echo $form->textField($model, 'updated_at'); ?>
  <?php echo $form->error($model,'updated_at'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'status'); ?>
  <?php echo $form->checkBox($model, 'status'); ?>
  <?php echo $form->error($model,'status'); ?>
  </div><!-- row -->


  <?php
  echo GxHtml::submitButton(Yii::t('app', 'Save'));
  $this->endWidget();
  ?>
  </div><!-- form -->
 */ ?>