<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
	Yii::t('app', 'Update'),
);

$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage Student Admission')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create Student  Admission')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/search.png'), 'url'=>array('view', 'id' => $model->id),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'View Student  Admission')), 
	array('label' => CHtml::image(Yii::app()->baseUrl. '/images/payment.png'), 'url' => array('dreamStudentPayment/create', 'sbatchid' => $model->id), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Student Payment')),

	//array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/delete.png'), 'url'=>array('create'),  'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?'),'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Delete Student Admission')), 
	);
	if(!(DreamStudentPayment::model()->find(array("condition"=>"stud_batch_id =  $model->id")))) {
		$this->menu[] = array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/delete.png'), 'url'=>array('create'),  'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?'),'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Delete Student'));
	}

?>

<!--h1><?php echo Yii::t('app', 'Update') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1-->

<?php
$this->renderPartial('_form', array(
		'model' => $model));
?>