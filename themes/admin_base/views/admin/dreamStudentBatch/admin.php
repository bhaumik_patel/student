<?php
$this->breadcrumbs = array(
    $model->label(2) => array('admin'),
    Yii::t('app', 'Manage'),
);
/* $this->menu = array(
  array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
  array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
  ); */
$this->menu = array(
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/question.png'), 'url' => array('#'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/dashboard.png'), 'url' => array('admin'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Manage Student Admission')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/plus.png'), 'url' => array('create'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Create Student Admission')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/refresh.png'), 'url' => array('admin'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Refresh')),
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('dream-student-batch-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<blockquote>
  You can not delete student admission, if the payment entry exist of that student admission. If you want to delete student admission entry, Please make sure the payment entry not exist of it!
</blockquote>
<?php /*
  <h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>
  <p>
  You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
  </p>
  <?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
  <div class="search-form">
  <?php $this->renderPartial('_search', array(
  'model' => $model,
  )); ?>
  </div><!-- search-form -->
 */ ?>
<?php
$columns = array(
    //'id',
	array(
        'name' => 'roll_no',
        'value' => '$data->roll_no',
		//'filter' =>true,
    ),
    array(
        'name' => 'student_id',
        'value' => 'GxHtml::valueEx($data->student)',
        'filter' => GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true, array('order' => 'name'))),
    ),
    array(
        'name' => 'branch_id',
        'value' => 'GxHtml::valueEx($data->branch)',
        'filter' => GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true, array('order' => 'branch_name'))),
    ),
    array(
        'name' => 'batch_id',
        'value' => 'GxHtml::valueEx($data->batch)',
        'filter' => GxHtml::listDataEx(DreamBatch::getFilterArray()),
    ),
    array('name' => 'adminssion_date', 'type' => 'html',
        'value' => 'CustomFunction::dateFormat($data->adminssion_date)',
        'filter' => false,
    //  'afterAjaxUpdate' => 'reinstallDatePicker',
    /* 'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
      'model' => $model,
      'name' => 'adminssion_date',
      'attribute'=>'adminssion_date',
      'options' => array(
      'dateFormat' => 'mm',
      ),
      'options'=>array(
      'id'=>'adminssion_date',
      ),
      'htmlOptions' => array(
      'style' => 'height:20px;'
      ),
      ),
      true), */
    ),
    array(
        'name' => 'next_payment_date',
        'header' => 'Next payment',
        'type' => 'html',
       // 'value' => '$data->next_payment_date',
'value'=>'($data->next_payment_date==\'0000-00-00\')? "-":CustomFunction::dateFormat($data->next_payment_date)',
        'filter' => false,
    /* 'filter' => $this->widget('zii.widgets.jui.CJuiDatePicker', array(
      'model' => $model,
      'name' => 'next_payment_date',
      'attribute'=>'next_payment_date',
      'options' => array(
      'dateFormat' => 'mm',
      ),
      'htmlOptions' => array(
      'style' => 'height:20px;'
      ),
      ),
      true), */
    ),
    /* array(
      'name' => 'joining_date',
      'filter' => false,
      ), */
    array(
        'name' => 'is_paid_registration',
        //'value' => '($data->is_paid_registration === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
        'type' => 'html',
        'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
        'value' => 'CHtml::tag("div",  array("style"=>"text-align: center" ) , CHtml::tag("img", array( "src" => UtilityHtml::getStatusImage(GxHtml::valueEx($data, \'is_paid_registration\')))))',
    ),
    array(
        'name' => 'registration_fee',
        'type' => 'html',
        'value' => '$data->getFieldFormat(\'registration_fee\')',
    //'footer'=>$model->getTotalsPaid($model->search()->getKeys()),
    ),
    array(
        'name' => 'paid_registration',
        'type' => 'html',
        'value' => '$data->getFieldFormat(\'paid_registration\')',
    //'footer'=>$model->getTotalsPaid($model->search()->getKeys()),
    ),
    array(
        'name' => 'unpaid_registration',
        'type' => 'html',
        'value' => '$data->getFieldFormat(\'unpaid_registration\')',
    //'footer'=>$model->getTotalsunPaid($model->search()->getKeys()),
    //'filter'=>false,
    ),
    array(
        'name' => 'is_paid_fee',
        //'value' => '($data->is_paid_registration === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
        'type' => 'html',
        'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
        'value' => 'CHtml::tag("div",  array("style"=>"text-align: center" ) , CHtml::tag("img", array( "src" => UtilityHtml::getStatusImage(GxHtml::valueEx($data, \'is_paid_fee\')))))',
    ),
    array(
        'name' => 'fee_amount',
        'type' => 'html',
        'value' => '$data->getFieldFormat(\'fee_amount\')',
    ),
    array(
        'name' => 'paid_amount',
        'type' => 'html',
        'value' => '$data->getFieldFormat(\'paid_amount\')',
    //'footer'=>$model->getTotalsPaid($model->search()->getKeys()),
    ),
    array(
        'name' => 'unpaid_amount',
        'header' => 'Remaining Fee',
        'type' => 'html',
        'value' => '$data->getFieldFormat(\'unpaid_amount\')',
    //'footer'=>$model->getTotalsunPaid($model->search()->getKeys()),
    //'filter'=>false,
    ),
    /* array(
      'name' => 'updated_at',
      'filter' => false,
      ), */
    array(
        'name' => 'status',
        'filter' => array(null => 'Please Select', '1' => 'Active', '0' => 'Inactive'),
        'type' => 'html',
        'value' => 'CHtml::tag("div",  array("style"=>"text-align: center" ) , CHtml::tag("img", array( "src" => UtilityHtml::getStatusImage(GxHtml::valueEx($data, \'status\')))))',
    ),
    /* array(
      'class' => 'CButtonColumn',
      'header' => 'Action',
      'htmlOptions' => array('width' => '75px'),
      ), */
    array(
        'class' => 'CButtonColumn',
        'template' => '{view}{update}{delete}{pay}',
        'buttons' => array
            (
			'delete' => array
			(
				'visible'=>'!(DreamStudentPayment::model()->find(array("condition"=>"stud_batch_id =  $data->id")))',
			),
            
            'pay' => array
                (
                'label' => 'Payment',
                'imageUrl' => Yii::app()->request->baseUrl . '/images/payment.png',
                'visible' => '($data->fee_amount > $data->paid_amount)?true:false',
                'url' => 'Yii::app()->createUrl("admin/dreamStudentPayment/create", array("sbatchid"=>$data->id))',
            ),
        ),
        'header' => 'Action',
        'htmlOptions' => array('width' => '70px'),
    ),
);
if (AdminModule::getUserDataByKey('user_type') == 'admin') {
    unset($columns[2]);
}
$grid_id = 'dream-student-batch-grid';
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'dream-student-batch-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'tablesorter',
    'summaryText' => 'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize', UtilityHtml::getPageSize(), UtilityHtml::getPageSizeArray(), array('class' => 'change-pageSize')) . ' rows per page',
    'filter' => $model,
    'columns' => $columns
));
?>

<?php

Yii::app()->clientScript->registerScript('initPageSize', <<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }});
    });
EOD
        , CClientScript::POS_READY);
?>


<?php Yii::app()->clientScript->registerScript('re-install-date-picker', "
    function reinstallDatePicker(id, data) {
        $('#adminssion_date').datepicker();
    }
"); ?>