<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'student_id'); ?>
		<?php echo $form->dropDownList($model, 'student_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'batch_id'); ?>
		<?php echo $form->dropDownList($model, 'batch_id', GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'adminssion_date'); ?>
		<?php echo $form->textField($model, 'adminssion_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'joining_date'); ?>
		<?php echo $form->textField($model, 'joining_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'registration_fee'); ?>
		<?php echo $form->textField($model, 'registration_fee', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'is_paid_registration'); ?>
		<?php echo $form->dropDownList($model, 'is_paid_registration', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'fee_amount'); ?>
		<?php echo $form->textField($model, 'fee_amount', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'paid_amount'); ?>
		<?php echo $form->textField($model, 'paid_amount', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'status'); ?>
		<?php echo $form->dropDownList($model, 'status', array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
