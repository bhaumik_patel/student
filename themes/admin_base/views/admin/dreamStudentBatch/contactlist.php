<?php

$this->breadcrumbs = array(
	//$model->label(2) => array('admin'),
	Yii::t('app', 'Contact List '),
);

/*$this->menu = array(
		array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	);*/
$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/refresh.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Refresh')),
	);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('dream-student-batch-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php /*
<h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>
<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p>
<?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<div class="search-form">
<?php $this->renderPartial('_search', array(
	'model' => $model,
)); ?>
</div><!-- search-form -->
*/?>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'dream-student-batch-grid',
	'dataProvider' => $model->search(),
	 'itemsCssClass'=>'tablesorter',
	'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize',UtilityHtml::getPageSize(),UtilityHtml::getPageSizeArray(),array('class'=>'change-pageSize')) . ' rows per page',
	'filter' => $model,
	'columns' => array(
		'id',
		array(
				'name'=>'student_id',
				'value'=>'GxHtml::valueEx($data->student)',
				'filter'=>GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'batch_id',
				'value'=>'GxHtml::valueEx($data->batch)',
				'filter'=>GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true)),
				),
		array(
				'name'=>'adminssion_date',
				'filter'=>false,
				),
		array(
				'name'=>'joining_date',
				'filter'=>false,
				),
		'batch_name',
		/*
		array(
					'name' => 'is_paid_registration',
					'value' => '($data->is_paid_registration === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		'fee_amount',
		'paid_amount',
		'created_at',
		'updated_at',
		array(
					'name' => 'status',
					'value' => '($data->status === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
					'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
		*/
		array(
			'class' => 'CButtonColumn',
			'header'=>'Action',
			'htmlOptions'=>array('width'=>'75px'),
		),
	),
)); ?>