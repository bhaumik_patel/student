<?php
$this->breadcrumbs = array(
    $model->label(2) => array('admin'),
    GxHtml::valueEx($model),
);

/* $this->menu=array(
  array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
  array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
  array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
  array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
  array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
  ); */
$this->menu = array(
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/dashboard.png'), 'url' => array('admin'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Manage Student Admission')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/plus.png'), 'url' => array('create'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Create Student Admission')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/update.png'), 'url' => array('update', 'id' => $model->id), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Update Student Admission')),
	array('label' => CHtml::image(Yii::app()->baseUrl. '/images/payment.png'), 'url' => array('dreamStudentPayment/create', 'sbatchid' => $model->id), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Student Payment')),
	
    //array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/delete.png'), 'url' => array('create'), 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Delete Student Admission')),
);
if(!(DreamStudentPayment::model()->find(array("condition"=>"stud_batch_id =  $model->id")))) {
		$this->menu[] = array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/delete.png'), 'url'=>array('create'),  'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?'),'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Delete Student'));
	}
?>

<!--h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1-->

<?php
$columns = array(
       array(
            'name' => 'Current Batch',
            'type' => 'raw',
            'value' => $model->batch !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->student->currentBatch)), array('dreamBatch/view', 'id' => GxActiveRecord::extractPkValue($model->student->currentBatch, true))) : null,
        ),
		array(
            'name' => 'Student Roll No',
            'title' => 'Student Roll No',
            'type' => 'raw',
            'value' => $model->roll_no,
        ),
        array(
            'name' => 'student',
            'type' => 'raw',
            'value' => $model->student !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->student)), array('dreamStudent/view', 'id' => GxActiveRecord::extractPkValue($model->student, true))) : null,
        ),
		
		
        
        array(
            'name' => 'branch',
            'type' => 'raw',
            'value' => $model->batch !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->branch)), array('dreamBranch/view', 'id' => GxActiveRecord::extractPkValue($model->branch, true))) : null,
        ),
        array('name' => 'adminssion_date', 'type' => 'html', 'value' => CustomFunction::dateFormat($model->adminssion_date)),
        array('name' => 'next_payment_date', 'type' => 'html', 
		'value'=>($model->next_payment_date=='0000-00-00')? "-":CustomFunction::dateFormat($model->next_payment_date)),
//'joining_date',
        array('name'=>'registration_fee','type'=>'raw','value'=>$model->getFieldFormat('registration_fee')),
        array('name'=>'paid_registration','type'=>'raw','value'=>$model->getFieldFormat('paid_registration')),
        array('name'=>'unpaid_registration','type'=>'raw','value'=>$model->getFieldFormat('unpaid_registration')),
        array('name'=>'fee_amount','type'=>'raw','value'=>$model->getFieldFormat('fee_amount')),
        array('name'=>'paid_amount','type'=>'raw','value'=>$model->getFieldFormat('paid_amount')),
        array('name'=>'unpaid_amount','type'=>'raw','value'=>$model->getFieldFormat('unpaid_amount')),
        array('name' => 'is_paid_registration', 'type' => 'html', 'value' => UtilityHtml::getStatusImageIcon($model->is_paid_registration)),
        array('name' => 'is_paid_fee', 'type' => 'html', 'value' => UtilityHtml::getStatusImageIcon($model->is_paid_fee)),
        array('name' => 'created_at', 'type' => 'html', 'value' => CustomFunction::dateFormat($model->created_at)),
        array('name' => 'updated_at', 'type' => 'html', 'value' => CustomFunction::dateFormat($model->updated_at)),
        array('name' => 'status', 'type' => 'html', 'value' => UtilityHtml::getStatusImageIcon($model->status)),
    );

if($model->student->current_batch_id != $model->batch_id) {
$columns[] = array(
		'name' => 'Batch (Last Payment for)',
		'type' => 'raw',
		'value' => $model->batch !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->batch)), array('dreamBatch/view', 'id' => GxActiveRecord::extractPkValue($model->batch, true))) : null,
	);
}
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => $columns,
));
?>