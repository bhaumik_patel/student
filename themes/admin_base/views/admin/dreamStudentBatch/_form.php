<div class="simplebox grid740" style="z-index: 720; ">
    <div class="titleh" style="z-index: 710; ">
        <h3><?php echo $model->label(2) ?></h3>
        <div class="shortcuts-icons" style="z-index: 700; "></div>
    </div>
    <div class="body" style="z-index: 690; ">
        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'dream-student-batch-form', 'enableAjaxValidation' => false,)); ?>     
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'student_id'); ?></span></label>              
            <?php
            $batches = DreamBatch::model()->findAllAttributes(null, true);
            $student_id = Yii::app()->request->getParam('sid');
            if ($model->student_id > 0) {
                $student_id = $model->student_id;
            }
            //$studentbatch_id = Yii::app()->request->getParam('id');
            if ($student_id) {
                if ($student_data = DreamStudent::model()->findByPk($student_id)) {
                    $batches = DreamBatch::model()->findAll('branch_id = ' . $student_data->branch_id);
                    echo $student_data->name;
                    echo $form->hiddenField($model, 'student_id', array('value' => $student_id));
                }
            } else {
                echo $form->dropDownList($model, 'student_id', GxHtml::listDataEx(DreamStudent::model()->findAll("status=:x ORDER BY name", array(':x'=>1))), array('empty' => Yii::t('fim', 'Select Student')));
            }
            ?>
            <?php echo $form->error($model, 'student_id', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>		

        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'batch_id'); ?></span></label>      

            <?php
            echo $form->dropDownList($model, 'batch_id', GxHtml::listDataEx($batches), array('empty' => Yii::t('fim', 'Select Batch')));
            /*
              $batch_id = Yii::app()->request->getParam('bid');
              if ($batch_id != '') {
              echo $form->dropDownList($model, 'batch_id', GxHtml::listDataEx(DreamBatch::model()->findAll(array('condition' => 'id=' . $batch_id))));
              } else {
              if ($studentbatch_id != '')
              echo $form->dropDownList($model, 'batch_id', GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true)), array('empty' => Yii::t('fim', 'Select Batch')));
              else
              echo $form->dropDownList($model, 'batch_id', array('prompt' => 'Select Batch'));
              }
             * 
             */
            ?>
            <?php echo $form->error($model, 'batch_id', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>

        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'adminssion_date'); ?></span></label> 
            <div class="input-append">
                <?php
//echo $form->textField($model, 'adminssion_date'); 
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'name' => 'adminssion_date',
                    'attribute' => 'adminssion_date',
                    'options' => array(
                        //'showAnim'=>'fold',
                        'dateFormat' => 'dd-mm-yy',
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;'
                    ),
                ));
                ?>
            </div>             
            <?php echo $form->error($model, 'adminssion_date', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>	
        <?php /*
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'joining_date'); ?></span></label> 
            <div class="input-append">
                <?php
// echo $form->textField($model, 'joining_date');
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'name' => 'joining_date',
                    'attribute' => 'joining_date',
                    'options' => array(
                        //'showAnim'=>'fold',
                        'dateFormat' => 'yy-mm-dd',
                    ),
                    'htmlOptions' => array(
                        'style' => 'height:20px;'
                    ),
                ));
                ?>
            </div>             
            <?php echo $form->error($model, 'joining_date', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>	
         */?>
         <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'Roll No#'); ?>
			<?php if($model->id=='') echo '(keep 0 to take next serial No.)' ?></span></label> 
            <?php echo $form->textField($model, 'roll_no', array('size' => 30, 'maxlength' => 10, 'readonly' => false)); ?>
            <?php echo $form->error($model, 'roll_no', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>

        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'Registration Fee Amount'); ?></span></label> 
            <?php echo $form->textField($model, 'registration_fee', array('size' => 30, 'maxlength' => 10, 'readonly' => false)); ?>
            <?php echo $form->error($model, 'registration_fee', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'Total Fee Amount'); ?></span></label> 
            <?php echo $form->textField($model, 'fee_amount', array('size' => 30, 'maxlength' => 10, 'readonly' => false)); ?>
            <?php echo $form->error($model, 'fee_amount', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
        <?php /*
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'is_paid_registration'); ?></span></label> 
            <?php echo $form->radioButtonList($model, 'is_paid_registration', array('1' => 'Yes', '0' => 'No'), array('separator' => '', 'labelOptions' => array('style' => 'display:inline'))); ?>
            <?php echo $form->error($model, 'is_paid_registration', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>	
         
         */?>     

        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'status'); ?></span></label>             
            <?php echo $form->dropDownlist($model, 'status', array('1' => 'Active', '0' => 'InActive')); ?>	     
            <?php echo $form->error($model, 'status', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>

        <div class="button-box" style="z-index: 460;">
            <?php echo GxHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'st-button save_continue')) ?>	
            <?php echo GxHtml::resetButton(Yii::t('app', 'Reset'), array('class' => 'st-button')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

<script>

    $(document).ready(function () {
<?php /*
        $('#DreamStudentBatch_batch_id').change(function () {
            $.ajax({
                type: "POST",
                url: '<?php echo GxHtml::normalizeUrl(array('DreamStudentBatch/ajaxBatch')) ?>',
                data: $('#dream-student-batch-form').serialize(),
                dataType: "json",
                success: function (data) {
                    if (data.success == 1) {
                        $('#DreamStudentBatch_fee_amount').val(data.data.batch_fee);
                        $('#DreamStudentBatch_registration_fee').val(data.data.registration_fee);
                    } else {

                    }
                },
                error: function (xhr, tStatus, e) {
                    if (!xhr) {
                        var html = "<div class=\"albox errorbox\" style=\"z-index: 500;\">";
                        html += "<h2>" + tStatus + " : " + e.message + "</h2><ul></ul></div>";
                    } else {
                        var html = "<div class=\"albox errorbox\" style=\"z-index: 500;\">";
                        html += "<h2>Connection error - Please check the network or api details</h2><ul></ul></div>";
                    }
                    $('#error_msg').html(html);
                    $('#error_msg').show();

                    $('#loader_info').hide();

                    return false;
                },
            });
            $('#DreamStudentBatch_fee_amount').val('0.00');
            $('#DreamStudentBatch_registration_fee').val('0.00');
        });
*/?>
        //direct create from student batch
        $('#DreamStudentBatch_student_id').change(function () {
            $.ajax({
                type: "POST",
                url: '<?php echo GxHtml::normalizeUrl(array('DreamStudentBatch/ajaxStudentBatch')) ?>',
                data: $('#dream-student-batch-form').serialize(),
                dataType: "json",
                success: function (data) {
                    if (data.success == 1) {
                        $('#DreamStudentBatch_batch_id').html(data.data);
                    } else {
                    }
                },
                error: function (xhr, tStatus, e) {
                    if (!xhr) {
                        var html = "<div class=\"albox errorbox\" style=\"z-index: 500;\">";
                        html += "<h2>" + tStatus + " : " + e.message + "</h2><ul></ul></div>";
                    } else {
                        var html = "<div class=\"albox errorbox\" style=\"z-index: 500;\">";
                        html += "<h2>Connection error - Please check the network or api details</h2><ul></ul></div>";
                    }
                    $('#error_msg').html(html);
                    $('#error_msg').show();
                    $('#loader_info').hide();
                    return false;
                },
            });
        });
    });
</script>

<?php
/* ?>
  <div class="form">
  <?php $form = $this->beginWidget('GxActiveForm', array(
  'id' => 'dream-student-batch-form',
  'enableAjaxValidation' => false,
  ));
  ?>

  <p class="note">
  <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
  </p>

  <?php echo $form->errorSummary($model); ?>

  <div class="row">
  <?php echo $form->labelEx($model,'student_id'); ?>
  <?php $student_id = Yii::app()->request->getParam('sid');?>
  <?php echo $form->dropDownList($model, 'student_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)),array('options' => array($student_id=>array('selected'=>true)),"disabled"=>"disabled")); ?>
  <?php echo $form->error($model,'student_id'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'batch_id'); ?>
  <?php $batch_id = Yii::app()->request->getParam('bid');?>
  <?php //echo $form->dropDownList($model, 'student_id', GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)),array('options' => array($student_id=>array('selected'=>true)),"disabled"=>"disabled")); ?>
  <?php echo $form->dropDownList($model, 'batch_id', GxHtml::listDataEx(DreamBatch::model()->findAll(array('condition'=>'branch_id=3')))); ?>
  <?php echo $form->error($model,'batch_id'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'adminssion_date'); ?>
  <?php echo $form->textField($model, 'adminssion_date'); ?>
  <?php echo $form->error($model,'adminssion_date'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'joining_date'); ?>
  <?php echo $form->textField($model, 'joining_date'); ?>
  <?php echo $form->error($model,'joining_date'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'registration_fee'); ?>
  <?php echo $form->textField($model, 'registration_fee', array('maxlength' => 10)); ?>
  <?php echo $form->error($model,'registration_fee'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'is_paid_registration'); ?>
  <?php echo $form->checkBox($model, 'is_paid_registration'); ?>
  <?php echo $form->error($model,'is_paid_registration'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'fee_amount'); ?>
  <?php echo $form->textField($model, 'fee_amount', array('maxlength' => 10)); ?>
  <?php echo $form->error($model,'fee_amount'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'paid_amount'); ?>
  <?php echo $form->textField($model, 'paid_amount', array('maxlength' => 10)); ?>
  <?php echo $form->error($model,'paid_amount'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'created_at'); ?>
  <?php echo $form->textField($model, 'created_at'); ?>
  <?php echo $form->error($model,'created_at'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'updated_at'); ?>
  <?php echo $form->textField($model, 'updated_at'); ?>
  <?php echo $form->error($model,'updated_at'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'status'); ?>
  <?php echo $form->checkBox($model, 'status'); ?>
  <?php echo $form->error($model,'status'); ?>
  </div><!-- row -->


  <?php
  echo GxHtml::submitButton(Yii::t('app', 'Save'));
  $this->endWidget();
  ?>
  </div><!-- form -->
  <?php */?>