<?php
$this->breadcrumbs = array(
    $model->label(2) => array('admin'),
    Yii::t('app', 'Manage'),
);

/* $this->menu = array(
  array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
  array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
  ); */
$this->menu = array(
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/question.png'), 'url' => array('#'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/dashboard.png'), 'url' => array('admin'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Manage Event')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/plus.png'), 'url' => array('create'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Create Event')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/refresh.png'), 'url' => array('admin'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Refresh')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('dream-event-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

<p>
You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
</p-->

<?php //echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
<!--div class="search-form">
<?php
$this->renderPartial('_search', array(
    'model' => $model,
));
?>
</div--><!-- search-form -->
<?php
$grid_id = 'dream-event-grid';
$songdata = CHtml::listData(DreamSong::model()->findAll(), 'id', 'song_name');

$columns = array(
    'id',
    array(
        'name' => 'branch_id',
        'value' => 'GxHtml::valueEx($data->branch)',
        'filter' => GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true)),
    ),
    //'contact_name',
    array('name' => 'contact_name', 'header' => 'Coordinator Name', 'value' => '$data->contact_name',),
    'contact_no',
    //'start_date',
    //'end_date',
    array('name' => 'start_date', 'type' => 'html', 'value' => 'CustomFunction::dateFormat($data->start_date)', 'filter' => false,),
    array('name' => 'end_date', 'type' => 'html', 'value' => 'CustomFunction::dateFormat($data->end_date)', 'filter' => false,),
    //'song_ids',

    /* array (
      'name'=>'song_ids',
      'filter'=> $this->widget('ext.EchMultiSelect.EchMultiSelect', array(
      'name'=>'DreamEvent_song_ids[]',
      'data' => $songdata,
      //'value'=>$selected_ids,
      'dropDownHtmlOptions'=> array(
      'style'=>'width:600px;',
      //'multiple'=>true,
      'id'=>'DreamEvent_song_ids',
      ),
      'options' => array(
      //'position'=>array('my'=>'left bottom', 'at'=>'left top'),
      'hide'=>true,
      'filter'=>true,
      'header'=> true,
      'show'=>true,
      //'autoOpen'=>true,
      ),
      )),
      ), */
    'fees',
    array(
        'name' => 'is_editing_done',
        'value' => '($data->is_editing_done === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
        'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
    ),
    //'name',
    //'details',

    /*
      'contact_no',
      'fees',
      'event_total_minute',
      array(
      'name' => 'is_editing_done',
      'value' => '($data->is_editing_done === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
      'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
      ),
      'start_date',
      'end_date',
      'created_at',
      'updated_at',
      array(
      'name' => 'is_paid',
      'value' => '($data->is_paid === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
      'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
      ),
      array(
      'name' => 'status',
      'value' => '($data->status === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
      'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
      ),
     */
    array(
        'class' => 'CButtonColumn',
        'header' => 'Action',
        'htmlOptions' => array('width' => '75px'),
    ),
);

if (AdminModule::getUserDataByKey('user_type') == 'admin') {
    unset($columns[1]);
}
?>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'dream-event-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass' => 'tablesorter',
    'summaryText' => 'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize', UtilityHtml::getPageSize(), UtilityHtml::getPageSizeArray(), array('class' => 'change-pageSize')) . ' rows per page',
    'columns' => $columns,
));
?>

<?php Yii::app()->clientScript->registerScript('initPageSize', <<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }});
    });
EOD
        , CClientScript::POS_READY);
?>