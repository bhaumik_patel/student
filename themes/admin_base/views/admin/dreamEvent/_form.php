<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo $model->label(2)?></h3>
   <div class="shortcuts-icons" style="z-index: 700; "></div>
   </div>
<div class="body" style="z-index: 690; ">

<?php  $form=$this->beginWidget('CActiveForm', array('id'=>'dream-event-form','enableAjaxValidation'=>false,)); ?>     
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'branch_id'); ?></span></label>              
			 <?php DreamBranch::model()->getBranchField($form, $model, $model->branch_id) //echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAll('status=1')),array('empty'=>Yii::t('fim','Select Branch'))); ?>
		     
			 <?php echo $form->error($model,'branch_id',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'song_Names'); ?></span></label> 
             <?php echo $form->textArea($model, 'song_ids',array('rows'=>6, 'cols'=>50)); ?>					
            <?php echo $form->error($model,'song_ids',array('class'=>'error_position')); ?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
               
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'name'); ?></span></label> 
             <?php echo $form->textField($model, 'name', array('size'=>40,'maxlength' => 150)); ?>
             <?php echo $form->error($model,'name',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'details'); ?></span></label> 
             <?php echo $form->textArea($model, 'details',array('rows'=>6, 'cols'=>50,'maxlength'=>150)); ?>
             <?php echo $form->error($model,'details',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		 <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'coordinator_name'); ?></span></label> 
             <?php echo $form->textField($model, 'contact_name', array('size'=>40,'maxlength' => 150)); ?>
             <?php echo $form->error($model,'contact_name',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		 <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'contact_no'); ?></span></label> 
             <?php echo $form->textField($model, 'contact_no', array('size'=>40,'maxlength' => 15)); ?>
             <?php echo $form->error($model,'contact_no',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'fees'); ?></span></label> 
             <?php echo $form->textField($model, 'fees', array('size'=>30,'maxlength' => 10)); ?>
             <?php echo $form->error($model,'fees',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'event_total_minute'); ?></span></label> 
            <?php echo $form->textField($model, 'event_total_minute', array('size'=>30)); ?>
             <?php echo $form->error($model,'event_total_minute',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'is_editing_done'); ?></span></label> 
			 <?php echo  $form->radioButtonList($model,'is_editing_done',array('1'=>'Yes','0'=>'No'),array('separator'=>'', 'labelOptions'=>array('style'=>'display:inline'))); ?>
             <?php echo $form->error($model,'is_editing_done',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo 'Event date'; ?></span></label> 
            <div class="input-append">		
			<?php //echo $form->textField($model, 'start_date', array('size'=>40)); 
			$this->widget('ext.dateRangePicker.JDateRangePicker',array(
                                'options'=>array('dateFmt'=>'dd-MM-yyyy'),
				'name'=>CHtml::activeName($model,'start_date'),
				'value'=>CustomFunction::dateFormat($model->start_date),
				//'onChange' => 'setEndDate("end_date", this)',
				'name2'=>CHtml::activeName($model,'end_date'),
                                'value2'=>CustomFunction::dateFormat($model->end_date),
                                'options2'=>array('dateFmt'=>'dd-MM-yyyy'),
			));
			?>
			</div>
			 <?php echo $form->error($model,'start_date',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<?php /*
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'end_date'); ?></span></label> 
            <?php //echo $form->textField($model, 'end_date', array('size'=>40)); ?>
             
			 <?php //echo $form->textField($model, 'start_date', array('size'=>40)); 
			$this->widget('zii.widgets.jui.CJuiDatePicker',array(
    'name'=>'end_date',
    // additional javascript options for the date picker plugin
    'options'=>array(
        //'showAnim'=>'fold',
    ),
    'htmlOptions'=>array(
        'style'=>'height:20px;'
    ),
));
			?><?php echo $form->error($model,'end_date',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		*/?>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'is_paid'); ?></span></label> 
             <?php //echo $form->checkBox($model, 'is_paid'); ?>
			 <?php echo  $form->radioButtonList($model,'is_paid',array('1'=>'Yes','0'=>'No'),array('separator'=>'', 'labelOptions'=>array('style'=>'display:inline'))); ?>
             <?php echo $form->error($model,'is_paid',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'status'); ?></span></label>             
			<?php echo $form->dropDownlist($model,'status',array('1'=>'Active','0'=>'InActive')); ?>	     
		    <?php echo $form->error($model,'status',array('class'=>'error_position')); ?>
           <div class="clear" style="z-index: 670; "></div>
        </div>
        
         <div class="button-box" style="z-index: 460;">
				<?php echo GxHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',  array('class'=>'st-button save_continue'))?>	
				<?php echo GxHtml::resetButton(Yii::t('app', 'Reset'),array('class'=>'st-button'));?>
		</div>
        <?php $this->endWidget();?>
</div>
</div>
<?php /*?>
<div class="form">
<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'dream-event-form',
	'enableAjaxValidation' => false,
));
?>
	<!--p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p-->

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'branch_id'); ?>
		<?php echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'branch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'song_ids'); ?>
		<?php echo $form->textArea($model, 'song_ids'); ?>
		<?php echo $form->error($model,'song_ids'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 150)); ?>
		<?php echo $form->error($model,'name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'details'); ?>
		<?php echo $form->textArea($model, 'details'); ?>
		<?php echo $form->error($model,'details'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'contact_name'); ?>
		<?php echo $form->textField($model, 'contact_name', array('maxlength' => 150)); ?>
		<?php echo $form->error($model,'contact_name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'contact_no'); ?>
		<?php echo $form->textField($model, 'contact_no', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'contact_no'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'fees'); ?>
		<?php echo $form->textField($model, 'fees', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'fees'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'event_total_minute'); ?>
		<?php echo $form->textField($model, 'event_total_minute'); ?>
		<?php echo $form->error($model,'event_total_minute'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'is_editing_done'); ?>
		<?php echo $form->checkBox($model, 'is_editing_done'); ?>
		<?php echo $form->error($model,'is_editing_done'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'start_date'); ?>
		<?php echo $form->textField($model, 'start_date'); ?>
		<?php echo $form->error($model,'start_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'end_date'); ?>
		<?php echo $form->textField($model, 'end_date'); ?>
		<?php echo $form->error($model,'end_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'is_paid'); ?>
		<?php echo $form->checkBox($model, 'is_paid'); ?>
		<?php echo $form->error($model,'is_paid'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->
<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->
<?php */?>