<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo $model->label(2)?></h3>
   <div class="shortcuts-icons" style="z-index: 700; "></div>
   </div>
<div class="body" style="z-index: 690; ">

<?php  $form=$this->beginWidget('CActiveForm', array('id'=>'dream-batch-form','enableAjaxValidation'=>false,)); ?>     
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'branch_id'); ?></span></label>              
			 <?php DreamBranch::model()->getBranchField($form, $model, $model->branch_id) //echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAll("t.`status`=1 OR t.id='$model->branch_id'")),array('empty'=>Yii::t('fim','Select Branch'))); ?>
			 <?php echo $form->error($model,'branch_id',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
        
         <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'batch_name'); ?></span></label> 
             <?php echo $form->textField($model, 'batch_name', array('size'=>40,'maxlength' => 255)); ?>
             <?php echo $form->error($model,'batch_name',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'detail'); ?></span></label> 
             <?php echo $form->textArea($model, 'detail',array('rows'=>6, 'cols'=>50)); ?>
             <?php echo $form->error($model,'detail',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
               
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'time_from'); ?></span></label> 
             <?php //echo $form->textField($model, 'time_from', array('size'=>30)); ?>
			  <?php
					 $this->widget('ext.jui_timepicker.JTimePicker', array(
						'model'=>$model,
						 'attribute'=>'time_from',
						 // additional javascript options for the date picker plugin
						 'options'=>array(
							 'showPeriod'=>true,
							 ),
						 'htmlOptions'=>array('size'=>20,'maxlength'=>8 ),
					 ));
					?>
             <?php echo $form->error($model,'time_from',array('class'=>'error_position')); ?>       
				
		<div class="clear" style="z-index: 670; "></div>
        </div>	 

		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'time_to'); ?></span></label> 
             <?php //echo $form->textField($model, 'time_to', array('size'=>30)); ?>
             <?php
					 $this->widget('ext.jui_timepicker.JTimePicker', array(
						'model'=>$model,
						 'attribute'=>'time_to',
						 // additional javascript options for the date picker plugin
						 'options'=>array(
							 'showPeriod'=>true,
							 ),
						 'htmlOptions'=>array('size'=>20,'maxlength'=>8 ),
					 ));
					?>
			 <?php echo $form->error($model,'time_to',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>	 		
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'days'); ?></span></label> 
             <?php //echo $form->textField($model, 'days', array('size'=>40,'maxlength' => 100)); ?>
             <?php
                $selected_ids = explode(',',$model->days);
                $data = array('Sunday'=>'Sunday','Monday'=>'Monday','Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','thursday'=>'thursday','Friday'=>'Friday','Saturday'=>'Saturday');
                $this->widget('ext.EchMultiSelect.EchMultiSelect', array(
                        'name'=>'DreamBatch_days[]',						
                        'data' => $data,
                        'value'=>$selected_ids,
                        'dropDownHtmlOptions'=> array(
                                'style'=>'width:160px;',
                                'id'=>'DreamBatch_days',
                        ),
                        'options' => array(
                                'hide'=>true,
                                //'filter'=>true,
                                'header'=> true,
                                'show'=>true, 
                   ),
                ));
                ?>
             <?php echo $form->error($model,'days',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
			  
        <?php /*
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'registration_fee'); ?></span></label> 
             <?php echo $form->textField($model, 'registration_fee', array('size'=>10)); ?>
             <?php echo $form->error($model,'registration_fee',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'batch_fee'); ?></span></label> 
             <?php echo $form->textField($model, 'batch_fee', array('size'=>10)); ?>
             <?php echo $form->error($model,'batch_fee',array('class'=>'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
	<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo 'Batch date'; ?></span></label> 
            <div class="input-append">			
			<?php //echo $form->textField($model, 'start_date', array('size'=>40)); 
			$this->widget('ext.dateRangePicker.JDateRangePicker',array(
				'name'=>CHtml::activeName($model,'start_date'),
				'value'=>$model->start_date,
				//'onChange' => 'setEndDate("end_date", this)',
				'name2'=>CHtml::activeName($model,'end_date'),
				'value2'=>$model->end_date,
			));
			?>
			</div>
	</div>
        */?>
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'status'); ?></span></label>             
			<?php echo $form->dropDownlist($model,'status',array('1'=>'Active','0'=>'InActive')); ?>	     
		    <?php echo $form->error($model,'status',array('class'=>'error_position')); ?>
           <div class="clear" style="z-index: 670; "></div>
        </div>
        
         <div class="button-box" style="z-index: 460;">
				<?php echo GxHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',  array('class'=>'st-button save_continue'))?>	
				<?php echo GxHtml::resetButton(Yii::t('app', 'Reset'),array('class'=>'st-button'));?>
		</div>
        <?php $this->endWidget();?>
</div>
</div>
<?php /*?>
<div class="form">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'dream-batch-form',
	'enableAjaxValidation' => false,
));
?>

	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>

	<?php echo $form->errorSummary($model); ?>

		<div class="row">
		<?php echo $form->labelEx($model,'branch_id'); ?>
		<?php echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true))); ?>
		<?php echo $form->error($model,'branch_id'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'batch_name'); ?>
		<?php echo $form->textField($model, 'batch_name', array('maxlength' => 255)); ?>
		<?php echo $form->error($model,'batch_name'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'detail'); ?>
		<?php echo $form->textArea($model, 'detail'); ?>
		<?php echo $form->error($model,'detail'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'time_from'); ?>
		<?php echo $form->textField($model, 'time_from'); ?>
		<?php echo $form->error($model,'time_from'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'time_to'); ?>
		<?php echo $form->textField($model, 'time_to'); ?>
		<?php echo $form->error($model,'time_to'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'days'); ?>
		<?php echo $form->textField($model, 'days', array('maxlength' => 15)); ?>
		<?php echo $form->error($model,'days'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'registration_fee'); ?>
		<?php echo $form->textField($model, 'registration_fee', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'registration_fee'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'batch_fee'); ?>
		<?php echo $form->textField($model, 'batch_fee', array('maxlength' => 10)); ?>
		<?php echo $form->error($model,'batch_fee'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'start_date'); ?>
		<?php echo $form->textField($model, 'start_date'); ?>
		<?php echo $form->error($model,'start_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'end_date'); ?>
		<?php echo $form->textField($model, 'end_date'); ?>
		<?php echo $form->error($model,'end_date'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model, 'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model, 'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
		</div><!-- row -->
		<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->checkBox($model, 'status'); ?>
		<?php echo $form->error($model,'status'); ?>
		</div><!-- row -->

		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudents')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudents', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudent::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentBatches')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudentBatches', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudentBatch::model()->findAllAttributes(null, true)), false, true)); ?>
		<label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentPayments')); ?></label>
		<?php echo $form->checkBoxList($model, 'dreamStudentPayments', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudentPayment::model()->findAllAttributes(null, true)), false, true)); ?>

<?php
echo GxHtml::submitButton(Yii::t('app', 'Save'));
$this->endWidget();
?>
</div><!-- form -->
<?php */?>