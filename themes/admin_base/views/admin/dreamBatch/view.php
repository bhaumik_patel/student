<?php

$this->breadcrumbs = array(
	$model->label(2) => array('admin'),
	GxHtml::valueEx($model),
);
$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage Batch')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create Batch')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/update.png'), 'url'=>array('update', 'id' => $model->id),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Update Batch')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/delete.png'), 'url'=>array('create'),  'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?'),'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Delete Batch')), 
	);	

?>

<!--h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1-->

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
array(
			'name' => 'branch',
			'type' => 'raw',
			'value' => $model->branch !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->branch)), array('dreamBranch/view', 'id' => GxActiveRecord::extractPkValue($model->branch, true))) : null,
			),
'batch_name',
'detail',
'time_from',
'time_to',
'days',
/*'registration_fee',
'batch_fee',
'start_date',
'end_date',*/
array('name'=>'created_at', 'type'=>'html', 'value'=> CustomFunction::dateFormat($model->created_at)),
array('name'=>'updated_at', 'type'=>'html', 'value'=> CustomFunction::dateFormat($model->updated_at)),
array('name'=>'status', 'type'=>'html', 'value'=>  UtilityHtml::getStatusImageIcon($model->status)),
	),
)); ?>
<?php /*
<h2><?php echo GxHtml::encode($model->getRelationLabel('dreamStudents')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->dreamStudents as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('dreamStudent/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentBatches')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->dreamStudentBatches as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('dreamStudentBatch/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?><h2><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentPayments')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->dreamStudentPayments as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('dreamStudentPayment/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>
*/?>