<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('branch_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->branch)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('current_batch_id')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->currentBatch)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name')); ?>:
	<?php echo GxHtml::encode($data->name); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('contact_no')); ?>:
	<?php echo GxHtml::encode($data->contact_no); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('comments')); ?>:
	<?php echo GxHtml::encode($data->comments); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('inquiry_date')); ?>:
	<?php echo GxHtml::encode($data->inquiry_date); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('created_at')); ?>:
	<?php echo GxHtml::encode($data->created_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('updated_at')); ?>:
	<?php echo GxHtml::encode($data->updated_at); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('status')); ?>:
	<?php echo GxHtml::encode($data->status); ?>
	<br />
	*/ ?>

</div>