
<div class="body" style="z-index: 690; ">

    <?php $form = $this->beginWidget('CActiveForm', array('id' => 'dream-student-form', 'enableAjaxValidation' => false,)); ?>     
    <div class="st-form-line" style="z-index: 680; "> 
        <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'branch_id'); ?></span></label>              
        <?php DreamBranch::model()->getBranchField($form, $model, $model->branch_id) //echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAll("status=1 OR id='$model->branch_id'")),array('empty'=>Yii::t('fim','Select Branch'))); ?>

        <?php /*
        echo CHtml::dropDownList('DreamStudent_branch_id', '', GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true)), array(
            'ajax' => array(
                'type' => 'POST', //request type
                'url' => CController::createUrl('dreamStudent/Dynamicbatches'), //url to call.
                //Style: CController::createUrl('currentController/methodToCall')
                'update' => '#DreamStudent_current_batch_id', //selector to update
            //'data'=>'js:javascript statement' 
            //leave out the data key to pass all form values through
        )));
         
         */
        ?>

        <?php echo $form->error($model, 'branch_id', array('class' => 'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
    </div>
    
    <?php /*
    <div class="st-form-line" style="z-index: 680; "> 
        <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'current_batch_id'); ?></span></label>              
        <?php echo CHtml::dropDownList('DreamStudent_current_batch_id', '', array(), array('prompt' => 'Select Batch')); ?>
        <?php //echo CHtml::dropDownList('batch_id','', array());?>
        <?php echo $form->error($model, 'current_batch_id', array('class' => 'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
    </div>
    */ ?>
    <div class="st-form-line" style="z-index: 680; "> 
        <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'name'); ?></span></label> 
        <?php echo $form->textField($model, 'name', array('size' => 40, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'name', array('class' => 'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
    </div>

    <div class="st-form-line" style="z-index: 680; "> 
        <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'contact_no'); ?></span></label> 
        <?php echo $form->textField($model, 'contact_no', array('size' => 40, 'maxlength' => 15)); ?>
        <?php echo $form->error($model, 'contact_no', array('class' => 'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
    </div>
    
    <div class="st-form-line" style="z-index: 680; "> 
        <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'address'); ?></span></label> 
        <?php echo $form->textArea($model, 'address', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'address', array('class' => 'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
    </div>

    <div class="st-form-line" style="z-index: 680; "> 
        <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'Follow Up'); ?></span></label> 
        <?php echo $form->textArea($model, 'comments', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'comments', array('class' => 'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
    </div>

    <div class="st-form-line" style="z-index: 680; "> 
        <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'inquiry_date'); ?></span></label> 
        <div class="input-append">
            <?php //echo $form->textField($model, 'inquiry_date');  ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'name' => 'inquiry_date',
                'attribute' => 'inquiry_date',
                'options' => array(
                    //'showAnim'=>'fold',
                    'dateFormat' => 'dd-mm-yy',
                ),
                'htmlOptions' => array(
                    'style' => 'height:20px;'
                ),
            ));
            ?>
        </div>             
            <?php echo $form->error($model, 'inquiry_date', array('class' => 'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
    </div>

    <div class="st-form-line" style="z-index: 680; "> 
        <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'status'); ?></span></label>             
<?php echo $form->dropDownlist($model, 'status', array('1' => 'Active', '0' => 'InActive')); ?>	     
        <?php echo $form->error($model, 'status', array('class' => 'error_position')); ?>
        <div class="clear" style="z-index: 670; "></div>
    </div>

    <div class="button-box" style="z-index: 460;">
<?php echo GxHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'st-button save_continue')) ?>	
        <?php echo GxHtml::resetButton(Yii::t('app', 'Reset'), array('class' => 'st-button')); ?>
    </div>
        <?php $this->endWidget(); ?>
</div>
</div>

<?php
/* ?>
  <div class="form">


  <?php $form = $this->beginWidget('GxActiveForm', array(
  'id' => 'dream-student-form',
  'enableAjaxValidation' => true,
  ));
  ?>

  <p class="note">
  <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
  </p>

  <?php echo $form->errorSummary($model); ?>

  <div class="row">
  <?php echo $form->labelEx($model,'branch_id'); ?>
  <?php echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true))); ?>
  <?php echo $form->error($model,'branch_id'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'current_batch_id'); ?>
  <?php echo $form->dropDownList($model, 'current_batch_id', GxHtml::listDataEx(DreamBatch::model()->findAllAttributes(null, true))); ?>
  <?php echo $form->error($model,'current_batch_id'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'name'); ?>
  <?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
  <?php echo $form->error($model,'name'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'contact_no'); ?>
  <?php echo $form->textField($model, 'contact_no', array('maxlength' => 15)); ?>
  <?php echo $form->error($model,'contact_no'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'comments'); ?>
  <?php echo $form->textArea($model, 'comments'); ?>
  <?php echo $form->error($model,'comments'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'inquiry_date'); ?>
  <?php echo $form->textField($model, 'inquiry_date'); ?>
  <?php echo $form->error($model,'inquiry_date'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'created_at'); ?>
  <?php echo $form->textField($model, 'created_at'); ?>
  <?php echo $form->error($model,'created_at'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'updated_at'); ?>
  <?php echo $form->textField($model, 'updated_at'); ?>
  <?php echo $form->error($model,'updated_at'); ?>
  </div><!-- row -->
  <div class="row">
  <?php echo $form->labelEx($model,'status'); ?>
  <?php echo $form->checkBox($model, 'status'); ?>
  <?php echo $form->error($model,'status'); ?>
  </div><!-- row -->

  <label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentBatches')); ?></label>
  <?php echo $form->checkBoxList($model, 'dreamStudentBatches', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudentBatch::model()->findAllAttributes(null, true)), false, true)); ?>
  <label><?php echo GxHtml::encode($model->getRelationLabel('dreamStudentPayments')); ?></label>
  <?php echo $form->checkBoxList($model, 'dreamStudentPayments', GxHtml::encodeEx(GxHtml::listDataEx(DreamStudentPayment::model()->findAllAttributes(null, true)), false, true)); ?>

  <?php
  echo GxHtml::submitButton(Yii::t('app', 'Save'));
  $this->endWidget();
  ?>
  </div><!-- form -->
  <?php */?>