<?php
$this->breadcrumbs = array(
    $model->label(2) => array('admin'),
    Yii::t('app', 'Manage'),
);

/* $this->menu = array(
  array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
  array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
  ); */
$this->menu = array(
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/question.png'), 'url' => array('#'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/dashboard.png'), 'url' => array('admin'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Manage Student')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/plus.png'), 'url' => array('create'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Create Student')),
    array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/refresh.png'), 'url' => array('admin'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Refresh')),
);
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('dream-student-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<blockquote>
  You can not delete student, if the payment entry exist of that student. If you want to delete student entry, Please make sure the payment entry not exist of it!
  <BR/> Also, If you delete student entry, it will delete the branch's admission entry if their payment is not exists.
</blockquote>
<!--br/><div style="text-align:center;font-size:28px;"><h1>Dream World Student Record</h1></div><br/-->
<?php /* ?>
  <h1><?php echo Yii::t('app', 'Manage') . ' ' . GxHtml::encode($model->label(2)); ?></h1>

  <p>
  You may optionally enter a comparison operator (&lt;, &lt;=, &gt;, &gt;=, &lt;&gt; or =) at the beginning of each of your search values to specify how the comparison should be done.
  </p>

  <?php echo GxHtml::link(Yii::t('app', 'Advanced Search'), '#', array('class' => 'search-button')); ?>
  <div class="search-form">
  <?php $this->renderPartial('_search', array(
  'model' => $model,
  )); ?>
  </div><!-- search-form -->
  <?php */ ?>
<?php $grid_id = 'dream-student-grid'; ?>

<?php


$columns = array(
        /* array(
          'name'=>'id',
          'htmlOptions'=>array('width'=>'30px'),
          ), */
        'id',
        array(
            'name' => 'branch_id',
            'value' => 'GxHtml::valueEx($data->branch)',
            'filter' => GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true,array('order'=>'branch_name'))),
        ),
        'name',
        /* array(
          'name'=>'name',
          'htmlOptions'=>array('width'=>'90px'),
          ), */
        'contact_no',
        /* array(
          'name'=>'contact_no',
          'htmlOptions'=>array('width'=>'90px'),
          ), */
        array(
            'name' => 'current_batch_id',
            'value' => 'GxHtml::valueEx($data->currentBatch)',
            'filter' => GxHtml::listDataEx(DreamBatch::getFilterArray()),
            'htmlOptions' => array('width' => '80px'),
        ),
        array('name'=>'inquiry_date', 'type'=>'html', 'value'=> 'CustomFunction::dateFormat($data->inquiry_date)','filter'=> false,),
        /*array(
            'name' => 'inquiry_date',
            'filter' => false,
        //'htmlOptions'=>array('width'=>'70px'),
        ),*/
        array(
            'name' => 'comments',
            //'filter' => false,
            'header' => 'Follow up',
        //'htmlOptions'=>array('width'=>'70px'),
        ),
        
        /* array(
          'name'=>'comments',
          'htmlOptions'=>array('width'=>'90px'),
          ), */
        //array('name'=>'studentName',  'value'=>$data->student->s_name), 
        array('name'=>'adminssion_date', 'type'=>'html', 'value'=> 'CustomFunction::dateFormat($data->adminssion_date)','filter'=> false,),
        
        /*
          'created_at',
          'updated_at',
          array(
          'name' => 'status',
          'value' => '($data->status === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
          'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
          ),
         */
        /* array(

          ), */
        array(
            'class' => 'CButtonColumn',
			
            'template' => '{view}{update}{delete}{reply}',
            'buttons' => array
                (
				'delete' => array
                    (
                    'visible'=>'!(DreamStudentPayment::model()->find(array("condition"=>"stud_id =  $data->id")))',
                    ),            
                'reply' => array
                    (
                    'label' => 'Admission',
                    'visible'=>'$data->current_batch_id==""',
                    'imageUrl' => Yii::app()->request->baseUrl . '/images/admission.png',
                    //'url' => 'Yii::app()->createUrl("admin/dreamStudentBatch/create", array("sid"=>$data->id,"bid"=>$data->current_batch_id))',
					'url' => 'Yii::app()->createUrl("admin/dreamStudentBatch/create", array("sid"=>$data->id))',
					),
            ),
            'header' => 'Action',
            'htmlOptions' => array('width' => '70px'),
        ),
    );
	if(AdminModule::getUserDataByKey('user_type')=='admin') {
		unset($columns[1]);
	}
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'dream-student-grid',
	'ajaxUpdate'=>true,
    'dataProvider' => $model->search(),
    'filter' => $model,
    'itemsCssClass' => 'tablesorter',
    'summaryText' => 'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize', UtilityHtml::getPageSize(), UtilityHtml::getPageSizeArray(), array('class' => 'change-pageSize')) . ' rows per page',
    'columns' => $columns
));
?>

<?php 

Yii::app()->clientScript->registerScript('initPageSize', <<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }});
    });
EOD
        , CClientScript::POS_READY);
?>