<h1 class="h1-tag">Viking<font face="normal">bro</font> Help Index</h1>
<ul class="list-plus">
       <li><a href="<?php echo CController::createUrl('/admin/wiki/page?page_name=Dashboard');?>"><?php echo Yii::t('vk','Home')?></a></li>
       <li><a href="<?php echo CController::createUrl('/admin/wiki/page?page_name=SystemUser');?>"><?php echo Yii::t('vk','System User')?></a></li>
       <li><a href="<?php echo CController::createUrl('/admin/wiki/page?page_name=BridgeProfile');?>"><?php echo Yii::t('vk','Bridge Profile')?></a></li>
       <li><a href="<?php echo CController::createUrl('/admin/wiki/page?page_name=BridgeConfig');?>"><?php echo Yii::t('vk','Bridge Config')?></a></li>
       <li><a href="<?php echo CController::createUrl('/admin/wiki/page?page_name=BridgeOrder');?>"><?php echo Yii::t('vk','Bridge Order')?></a></li>
       <li><a href="<?php echo CController::createUrl('/admin/wiki/page?page_name=GeneralSetting');?>"><?php echo Yii::t('vk','General Setting')?></a></li>
</ul>
