<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />
<link rel="shortcut icon" href="<?php echo Yii::app()->getBaseUrl() ?>/favicon.ico" type="image/x-icon"/>
</head>
<body>
    <div class="loginform">
    	<div class="title"> <!-img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-admin1.png" width="300"/--></div>
        <div class="body">
        <?php /* 
       	  <form id="form1" name="form1" method="post" action="index.html">
          	<label class="log-lab">Username</label>
            <input name="textfield" type="text" class="login-input-user" id="textfield" value="Admin"/>
          	<label class="log-lab">Password</label>
            <input name="textfield" type="password" class="login-input-pass" id="textfield" value="Password"/>
            <input type="submit" name="button" id="button" value="Login" class="button"/>
       	  </form>
       	  */ ?>
       	  <?php
	    foreach(Yii::app()->admin->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }
		?>
     	<?php echo $content; ?> 
        </div>
    </div>
</body>
<?php /* 

<body  class="loginpage">
  <div id="header">
  		<div class="content">
	    	<div id="logo">
      <?php // echo CHtml::encode(Yii::app()->name); ?>
      <div class="header-left">
      <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-admin.png" alt="" height="30" /> 
      	<span class="main-text">Admin Panel</span>
      </div>
  </div>
		  </div>
  </div>
  <!-- header -->
  <div class="login_content">
  <div class="content">
	  <?php
	    foreach(Yii::app()->admin->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }
		?>
     <?php echo $content; ?> 
     </div>
  </div>
</div>
  <!-- footer -->
  <div id="footer"> Copyright &copy; <?php echo date('Y'); ?> by <?php echo SystemConfig::getValue('site_name')?>. All Rights Reserved.<br/>
  </div>
  
<!-- page -->
</body>
*/?>
</html>