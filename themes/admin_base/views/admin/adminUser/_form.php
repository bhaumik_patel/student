<div class="simplebox grid740" style="z-index: 720; ">
    <div class="titleh" style="z-index: 710; ">
        <h3><?php echo $model->label(2) ?></h3>

    </div>
    <div class="body" style="z-index: 690; ">
        <?php $form = $this->beginWidget('CActiveForm', array('id' => 'user-form', 'enableAjaxValidation' => false,)); ?>
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"> <?php echo $form->labelEx($model, 'user_type'); ?></span></label> 
            <?php
            if (Yii::app()->admin->getId() == $model->id) {
                echo AdminUser::getUserType($model->user_type);
            } else {
                echo $form->dropDownlist($model, 'user_type', AdminUser::getDefinedUserType(), array('readonly' => 'true', 'disabled' => false));
            }
            ?>
            <?php echo $form->error($model, 'user_type', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>  

        <!--
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'branch'); ?></span></label>              

            <?php
            if (Yii::app()->admin->getId() == $model->id) {
                echo $model->branch;
            } else {
                echo $form->dropDownList($model, 'branch_id', GxHtml::listDataEx(DreamBranch::model()->findAll("t.`status`=1")), array('empty' => Yii::t('fim', 'Select Branch')));
            }
            ?>
            <?php echo $form->error($model, 'branch_id', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>-->

        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'username'); ?></span></label> 
            <?php // if($this->getAction()->id == 'create'): ?>
            <?php echo $form->textField($model, 'username', array('size' => 40, 'maxlength' => 30)); ?>
            <?php // else: ?>
            <?php //echo $model->username; ?>
            <?php //endif; ?>
            <?php echo $form->error($model, 'username', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>

        <?php if ($this->getAction()->id == 'create'): ?>
            <div class="st-form-line" style="z-index: 680; "> 
                <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'password'); ?></span></label> 
                <?php echo $form->passwordField($model, 'password', array('size' => 40, 'maxlength' => 150, 'value' => '')); ?>
                <?php echo $form->error($model, 'password', array('class' => 'error_position')); ?>
                <div class="clear" style="z-index: 670; "></div>
            </div>
        <?php endif; ?>


        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('firstname') ?></span></label> 
            <?php echo $form->textField($model, 'firstname', array('size' => 32, 'maxlength' => 30, 'class' => 'st-forminput')); ?>
            <?php echo $form->error($model, 'firstname', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('lastname') ?></span></label> 
            <?php echo $form->textField($model, 'lastname', array('size' => 32, 'maxlength' => 30)); ?>
            <?php echo $form->error($model, 'lastname', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('email') ?></span></label> 
            <?php echo $form->textField($model, 'email', array('size' => 50, 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'email', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>

        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext">  <?php echo $form->labelEx($model, 'extra'); ?></span></label>           
            <?php echo $form->textArea($model, 'extra', array('rows' => 6, 'cols' => 50, 'maxlength' => 150)); ?>
            <?php echo $form->error($model, 'extra', array('class' => 'error_position')); ?>

            <div class="clear" style="z-index: 670; "></div>
        </div>

        <div class="st-form-line" style="z-index: 680; "> 
            <label><span class="st-labeltext"><?php echo $form->labelEx($model, 'is_active'); ?></span></label> 
            <?php if (Yii::app()->admin->getId() == $model->id): ?>
                <?php echo UtilityHtml::getStatusImageIcon($model->is_active); ?>
            <?php else: ?>
                <?php echo $form->dropDownlist($model, 'is_active', array('1' => 'Active', '0' => 'InActive'), array('class' => 'uniform')); ?>
            <?php endif; ?>
            <?php echo $form->error($model, 'is_active', array('class' => 'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>


        <div class="button-box" style="z-index: 460;">
            <?php echo GxHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'st-button save_continue')) ?>	
            <?php echo GxHtml::resetButton(Yii::t('app', 'Reset'), array('class' => 'st-button')); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

