<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo "Change Password";?></h3>
   <div class="shortcuts-icons" style="z-index: 700; "> <a class="shortcut tips" href="#" original-title="Create User"><img src="<?php echo UtilityHtml::getAdminSkinUrl() ?>/img/icons/shortcut/question.png" width="25" height="25" alt="icon"></a> </div>
   </div>
<div class="body" style="z-index: 690; ">   
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pwd-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model, Yii::t('app','Please fix the following input errors:')); ?>
	
	<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'password'); ?></span></label> 
             <?php echo $form->passwordField($model,'password',array('size'=>40,'maxlength'=>40,'value'=>'')); ?>
	         <?php echo $form->error($model,'password',array('class'=>'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
    </div>
	
	
	<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'password_repeat'); ?></span></label> 
             <?php echo $form->passwordField($model,'password_repeat',array('size'=>40,'maxlength'=>40,'value'=>'')); ?>
	         <?php echo $form->error($model,'password_repeat',array('class'=>'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
    </div>
    
    
     <div class="button-box" style="z-index: 460;">
				<?php echo CHtml::submitButton(Yii::t('app','Change Password'),array('class'=>'st-button save_continue')); ?>	
				<?php echo GxHtml::resetButton(Yii::t('app', 'Reset'),array('class'=>'st-button'));?>
	</div>
	
<?php $this->endWidget(); ?>
</div>
</div><!-- form -->