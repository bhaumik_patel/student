<?php


if(UtilityHtml::getDataByKey(Yii::app()->admin->getState('admin'), 'user_type') == 'superadmin')
	$this->breadcrumbs = array('User'=>array('admin'));
$this->breadcrumbs[] =	'Update Profile';

if (Yii::app()->admin->getId() != $model->id) {

    $this->menu = array(
        array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/question.png'), 'url' => array('#'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
        array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/dashboard.png'), 'url' => array('admin'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Manage User')),
        array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/plus.png'), 'url' => array('create'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Create User')),
        array('label' => CHtml::image(UtilityHtml::getAdminSkinUrl() . '/img/icons/shortcut/refresh.png'), 'url' => array('admin'), 'itemOptions' => array('class' => 'shortcut tips', 'original-title' => 'Refresh')),
    );
}
/* if(Yii::app()->admin->id==$model->id) {
  $this->menu=array(
  array('label'=>'View User', 'url'=>array('view', 'id'=>$model->id)),
  array('label'=>'Create User', 'url'=>array('create')),
  array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
  array('label'=>'Manage User', 'url'=>array('admin')),
  array('label'=>'Change Password', 'url'=>array('change', 'id'=>$model->id)),
  );
  }else {
  $this->menu=array(
  array('label'=>'View User', 'url'=>array('view', 'id'=>$model->id)),
  array('label'=>'Create User', 'url'=>array('create')),
  array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
  array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
  array('label'=>'Manage User', 'url'=>array('admin')),
  array('label'=>'Change Password', 'url'=>array('change', 'id'=>$model->id)),
  );
  } */
?>



<?php echo $this->renderPartial('_form', array('model' => $model)); ?>