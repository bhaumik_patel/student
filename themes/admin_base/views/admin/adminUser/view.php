<?php
	$this->breadcrumbs=array(
		'User'=>array('admin'),
		$model->username,
	);
	$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/question.png'), 'url'=>array('#'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage User')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create User')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/refresh.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Refresh')),
	);
	/*if(Yii::app()->admin->id==$model->id) {
		$this->menu=array(
		array('label'=>'View User', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Create User', 'url'=>array('create')),
		array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Manage User', 'url'=>array('admin')),
		array('label'=>'Change Password', 'url'=>array('change', 'id'=>$model->id)),
	);
		
	}else {
		
		$this->menu=array(
		array('label'=>'View User', 'url'=>array('view', 'id'=>$model->id)),
		array('label'=>'Create User', 'url'=>array('create')),
		array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
		array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
		array('label'=>'Manage User', 'url'=>array('admin')),
		array('label'=>'Change Password', 'url'=>array('change', 'id'=>$model->id)),
		);
	}*/
?>
<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
'id',
'firstname',
'lastname',
//'user_type',
'email',
'username',
'password',
//'created_at',
array('name'=>'created_at', 'type'=>'html', 'value'=> CustomFunction::dateFormat($model->created_at)),
array('name'=>'updated_at', 'type'=>'html', 'value'=> CustomFunction::dateFormat($model->updated_at)),
//'updated_at',
//'logdate',
//'lognum',
array('name'=>'is_active', 'type'=>'html', 'value'=>  UtilityHtml::getStatusImageIcon($model->is_active)),
//'user_roles',
'extra',
    ),
)); ?>
<?php /*
<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo $model->label(2)?></h3>
   </div>
<div class="body" style="z-index: 690; ">

<?php  $form=$this->beginWidget('CActiveForm', array('id'=>'user-form','enableAjaxValidation'=>false,)); ?>
       <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'user_type'); ?></span></label> 
              <?php echo $model->user_type;?>
	         <div class="clear" style="z-index: 670; "></div>
        </div>  
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'username'); ?></span></label> 
			  <?php echo $model->username; ?>
        <div class="clear" style="z-index: 670; "></div>
        </div>       
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('firstname')?></span></label> 
             <?php echo $model->firstname;?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('lastname')?></span></label> 
              <?php echo $model->lastname;?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $model->getAttributeLabel('email')?></span></label> 
             <?php echo $model->email;?>
        <div class="clear" style="z-index: 670; "></div>
        </div>
        
         <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'is_active'); ?></span></label> 
             <?php echo $model->is_active == "1" ? "Active": "InActive"?>
           <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'created_at'); ?></span></label>           
					<?php echo $model->created_at;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
		
		<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'updated_at'); ?></span></label>           
					<?php echo $model->updated_at;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">  <?php echo $form->labelEx($model,'extra'); ?></span></label>           
					<?php echo $model->extra;?>					            
        <div class="clear" style="z-index: 670; "></div>
        </div>
        <?php $this->endWidget();?>
</div>
</div>
*/?>

