<?php
$this->breadcrumbs=array(
	'User'=>array('admin'),
	'Manage',
);

$this->menu = array(
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/question.png'), 'url'=>array('#'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage User')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create User')), 
	array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/refresh.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Refresh')),
	);		
 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php
$grid_id = 'user-grid';
$template = '{update}{delete}';
if($this->userType=='admin')
	$template = '{update} {delete}';  
?>
<?php  $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>$grid_id,
	'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize',UtilityHtml::getPageSize(),UtilityHtml::getPageSizeArray(),array('class'=>'change-pageSize')) . ' rows per page',
	'dataProvider'=>$model->search(),
	'itemsCssClass'=>'tablesorter',
	'filter'=>$model,
	'beforeAjaxUpdate'=>'function(id, options){
		var strUrl = options.url;
		var regex = /id=([0-9]+)&/;
		if(regex.test(strUrl) == true) {
			var ar = strUrl.match(regex);
			if(ar[1]){
				if(ar[1] == '.Yii::app()->admin->id.') {
					alert("You can not delete your own account!");
					return false;
				}
			}
		}
		
	}',	
	'columns'=>array(
		'id',
			/*array(	
				'name'=>'user_type',
				//'header'=>Yii::t("messages", 'Vendor'),
				'value'=>'AdminUser::getUserType($data->user_type)',
		 		'filter'=> AdminUser::getDefinedUserType(),//GxHtml::listDataEx($model,'user_type'),
				),*/
               /* array(
				'name'=>'branch_id',
				'value'=>'GxHtml::valueEx($data->branch)',
				'filter'=>GxHtml::listDataEx(DreamBranch::model()->findAllAttributes(null, true)),
				),*/
		'username',
		//'user_type',
		array(
					'name' => 'user_type',
					'value' => '($data->user_type === "admin") ? Yii::t(\'app\', \'Business Owner\') : $data->user_type',
					'filter' => AdminUser::getDefinedUserType(),
					),
        'firstname',
		'lastname',
		//'email',
				
				array(
			'name' => 'email',
			'type' => 'raw',
			'value' => 'CHtml::link(CHtml::encode($data->email), "mailto:".CHtml::encode($data->email))',
		),
		//'is_active',
				array(
					'name'=>'is_active',
					'filter'=> array(null=>'Please Select','1'=>'Active','0'=>'Inactive'), 
					'type' => 'html',
					'value'=> 'CHtml::tag("div",  array("style"=>"text-align: center" ) , CHtml::tag("img", array( "src" => UtilityHtml::getStatusImage(GxHtml::valueEx($data, \'is_active\')))))',
		),
		//'created_at',
		//'updated_at',
		/*
		'password',
		
		'logdate',
		'lognum',
		'extra',
		*/
		/*array(
			'buttons'=>array
			(
					'Change Password' => array
					(   
						'imageUrl'=>Yii::app()->request->baseUrl.'/images/change-password-icon.png',
						'url'=>'Yii::app()->createUrl(\'admin/adminUser/change\', array(\'id\'=>$data->id))',
					),
			),
		),*/
		array(
			
		'class'=>'CButtonColumn',
		'header'=>'Action',	
		'htmlOptions'=>array('width'=>'75px'),
    	'template'=>'{update}{delete}  {Change Password}',
		'buttons'=>array
		(
		        'Change Password' => array
		        (   
		     		'imageUrl'=>Yii::app()->request->baseUrl.'/images/change-password-icon.png',
		         	'url'=>'Yii::app()->createUrl(\'admin/adminUser/change\', array(\'id\'=>$data->id))',
		        ),
		        
         
	),
		),
	),
)); ?>

<?php Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
    $('.change-pageSize').live('change', function() {
        $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }})
    });
EOD
,CClientScript::POS_READY); ?>