
<div class="simplebox grid740" style="z-index: 720; ">
   <div class="titleh" style="z-index: 710; ">
   <h3><?php echo $model->label(2)?></h3>
   <div class="shortcuts-icons" style="z-index: 700; "> <a class="shortcut tips" href="#" original-title="Create User"><img src="<?php echo UtilityHtml::getAdminSkinUrl() ?>/img/icons/shortcut/question.png" width="25" height="25" alt="icon"></a> </div>
   </div>
<div class="body" style="z-index: 690;">


<?php $form = $this->beginWidget('GxActiveForm', array(
	'id' => 'bjois-users-form',
	'enableAjaxValidation' => false,
));
?>

	<?php echo $form->errorSummary($model); ?>
	 <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'user_roles'); ?></span></label>
             <?php echo $form->dropDownList($model, 'user_roles', UserRole::model()->getFrontUserRoleList(), array('prompt'=>'Please Select Role')); ?>
	         <?php echo $form->error($model,'user_roles',array('class'=>'error_position')); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
	
	
	
	   
	<div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo  $form->labelEx($model,'firstname'); ?></span></label> 
              <?php echo $form->textField($model, 'firstname', array('maxlength' => 30));?>
	         <?php echo $form->error($model,'firstname'); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>  
	

       <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo  $form->labelEx($model,'lastname'); ?></span></label> 
              <?php  echo $form->textField($model, 'lastname', array('maxlength' => 30));?>
	         <?php echo $form->error($model,'lastname');  ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
        
        
         <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'email');?></span></label> 
              <?php  echo $form->textField($model, 'email', array('maxlength' => 255));?>
	         <?php echo $form->error($model,'email');  ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>  
         	
		 <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'password');?></span></label> 
              <?php echo $form->passwordField($model, 'password', array('maxlength' => 255));?>
	         <?php  echo $form->error($model,'password');  ?>
            <div class="clear" style="z-index: 670; "></div>
        </div> 
        
         <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php  echo $form->labelEx($model,'phone');?></span></label> 
              <?php echo $form->textField($model, 'phone', array('maxlength' => 25));?>
	         <?php echo $form->error($model,'phone'); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>
        
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo $form->labelEx($model,'last_login_date');?></span></label> 
              <?php echo $form->textField($model, 'last_login_date');?>
	         <?php echo$form->error($model,'last_login_date'); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>       
         
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"> <?php echo  $form->labelEx($model,'total_loggedin');?></span></label> 
              <?php echo  $form->textField($model, 'total_loggedin', array('maxlength' => 10)); ?>
	         <?php echo  $form->error($model,'total_loggedin')?>
            <div class="clear" style="z-index: 670; "></div>
        </div>     
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext">	<?php echo $form->labelEx($model,'extra'); ?></span></label> 
        	<?php echo $form->textArea($model, 'extra'); ?>
	    	<?php echo $form->error($model,'extra'); ?>
            <div class="clear" style="z-index: 670; "></div>
        </div>   
        
        <div class="st-form-line" style="z-index: 680; "> 
             <label><span class="st-labeltext"><?php echo $form->labelEx($model,'status'); ?></span></label> 
        	<?php echo $form->checkBox($model, 'status'); ?>
			<?php echo $form->error($model,'status'); ?>	
            <div class="clear" style="z-index: 670; "></div>
        </div>

		<div class="button-box" style="z-index: 460;">
				<?php echo GxHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',  array('class'=>'st-button save_continue'))?>	
				<?php echo GxHtml::resetButton(Yii::t('app', 'Reset'),array('class'=>'st-button'));?>
		</div>
<?php
$this->endWidget();
?>
</div></div>
 
<!-- form -->