<?php
$this->breadcrumbs = array(
		$model->label(2) => array('admin'),
		Yii::t('app', 'Manage'),
);

/*$this->menu = array(
 array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
		array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
);*/




$this->menu = array(
		array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/question.png'), 'url'=>array('#'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.')),
		array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/dashboard.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Manage User')),
		array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/plus.png'), 'url'=>array('create'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Create User')),
		array('label'=>CHtml::image(UtilityHtml::getAdminSkinUrl().'/img/icons/shortcut/refresh.png'), 'url'=>array('admin'),  'itemOptions'=>array('class'=>'shortcut tips' ,'original-title'=>'Refresh')),
);
$grid_id = 'bjois-users-grid';

Yii::app()->clientScript->registerScript('search', "
		$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
});
		$('.search-form form').submit(function(){
		$.fn.yiiGridView.update('bjois-users-grid', {
		data: $(this).serialize()
});
		return false;
});


		$('.deleteall-button').click(function(){

		var atLeastOneIsChecked = $('input[name=\"".$grid_id."_c0[]\"]:checked').length > 0;

				if (!atLeastOneIsChecked)
				{
				alert('Please select atleast one to Delete');
}
				else if (window.confirm('Are you sure you want to Delete?'))
				{
				document.getElementById('campaign-search-form').action='index.php?r=admin/bjoisUser/deleteall';
				document.getElementById('campaign-search-form').submit();
}
});
				");
?>



<div class="row buttons">
	<?php echo CHtml::button('Delete',array('name'=>'btndeleteall','class'=>'st-button deleteall-button')); ?>
</div>


<?php $this->widget('zii.widgets.grid.CGridView', array(
		'id' => $grid_id,
		'selectableRows'=>2,
		'dataProvider' => $model->search(),
		'filter' => $model,
		'summaryText'=>'Displaying {start}-{end} of {count} result(s). ' . CHtml::dropDownList('pageSize',UtilityHtml::getPageSize(),UtilityHtml::getPageSizeArray(),array('class'=>'change-pageSize')) . ' rows per page',
		'columns' => array(

		array(
				'value'=>'$data->id',
				'class'=>'CCheckBoxColumn',
				
				//'filter'=>false,
				//'order'=>false,

		),


		'id',
		'firstname',
		'lastname',
		'email',
		'password',
		'phone',
		/*
		 'last_login_date',
'total_loggedin',
'toatl_ads',
'total_clicks',
'total_ad_reply',
'toatl_ad_files',
'total_likes',
'total_amount_ads',
'user_roles',
'extra',
'created_at',
'updated_at',
array(
					'name' => 'status',
		'value' => '($data->status === 0) ? Yii::t(\'app\', \'No\') : Yii::t(\'app\', \'Yes\')',
		'filter' => array('0' => Yii::t('app', 'No'), '1' => Yii::t('app', 'Yes')),
					),
*/
				array(
			'class' => 'CButtonColumn',
		),
	),
		'itemsCssClass'=>'tablesorter',
)); ?>
<?php Yii::app()->clientScript->registerScript('initPageSize',<<<EOD
    $('.change-pageSize').live('change', function() {
    $.fn.yiiGridView.update('$grid_id',{ data:{ pageSize: $(this).val() }});
});
EOD
,CClientScript::POS_READY); ?>