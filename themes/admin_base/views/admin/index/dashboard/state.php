<div class="grid360-<?php echo $pos?>" style="z-index: 420;">
      <!-- start statistics codes -->
	   <div class="simplebox" style="z-index: 410;">
		  <div class="titleh" style="z-index: 400;">
			 <h3>Statistics <?php echo $title?></h3>
		  </div>
		<div class="body" style="z-index: 390;">
			<ul class="statistics">
				<li> <?php echo Yii::t('vk','Total Category')?>
					<p><span class="green"></span><span class="blue">0</span></p>
				</li>
				<li> <?php echo Yii::t('vk','Total Product')?>
					<p><span class="red">0</span></p>
				</li>
				<li><?php echo Yii::t('vk','Total Customer')?>
					<p><span class="green">0</span><span class="blue"></span><span class="red"></span></p>
				</li>
				<li><?php echo Yii::t('vk','Total Order')?>
					<p><span class="blue"></span><span class="red">0</span></p>
				</li>
			</ul>
		</div>
	</div>
	<!-- end statistics codes -->
</div>