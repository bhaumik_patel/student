<!--div class="grid740" style="z-index: 690; ">
        <a href="<?php echo CController::createUrl('dreamStudentPayment/MonthwiseReport') ?>" class="dashbutton tips" original-title="<?php echo Yii::t('dwda','Month wise - Reports')?>"><img src="<?php echo UtilityHtml::getAdminSkinUrl1() ?>/vk_systemuser.png" alt="icon"><b><?php echo Yii::t('dwda','Month Wise Report')?></b></a>
        <a href="<?php echo CController::createUrl('systemConfig/admin') ?>" class="dashbutton tips" original-title="<?php echo Yii::t('dwda','System Setting - Manage system configuration')?>"><img src="<?php echo UtilityHtml::getAdminSkinUrl1() ?>/vk_setting.png" alt="icon"><b><?php echo Yii::t('dwda','System Setting')?></b></a>
     <div class="clear" style="z-index: 680; "></div>
</div-->

 
<?php
//Branch / Batch wise Daily Collection start
$connection = Yii::app()->db;
if(isset($_POST['curr_date']) && $_POST['curr_date']!='')
{
	$currdate = CustomFunction::dateFormatymd($_POST['curr_date']);
}
else
$currdate = date('Y-m-d');
//echo AdminModule::getUserDataByKey('user_type');
$condition = '';
if(AdminModule::getUserDataByKey('user_type') == 'admin')
{
	$branch_id = AdminModule::getUserDataByKey('branch_id');
	$condition = ' AND t.branch_id='.$branch_id;
}
//AdminModule::getUserDataByKey('branch_id');
$command = $connection->createCommand("SELECT t.pay_date,t.branch_id, br.branch_name, b.batch_name,t.batch_id, sum(t.amount_paid + t.registration_paid) as total_paid
FROM dream_student_payment as t
LEFT JOIN dream_branch as br ON br.id = t.branch_id 
LEFT JOIN dream_batch as b ON b.id = t.batch_id 
WHERE t.pay_date = '$currdate' $condition
GROUP BY t.batch_id ORDER BY t.branch_id");


$datarows = $command->queryAll();
//$totalbranch = count($branchrows);
?>
<div id="dailyreport">
<br/><div><h2 class="h2-tag">Branch / Batch Date Wise Collection</h2></div>
<br/>
<div class="curdate">
<form method="post" action="<?php echo Yii::app()->baseUrl;?>/admin/index/report" id="dream-dashboard">     
<table align="" cellspacing="0" cellpadding="0" border="0" id="example" class="" style="width:35%;">                        
	<tr class="gradeA even">
				<td><b>Select Date</b></td>
				<td class="center">
	
<?php
$dcurrdate = CustomFunction::dateFormat($currdate);
$this->widget('zii.widgets.jui.CJuiDatePicker', array(
	//'model' => $model,
	'name' => 'curr_date',
	'value'=>$dcurrdate,
	'attribute' => 'curr_date',
	'options' => array(		
		//'showAnim'=>'fold',
		'dateFormat' => 'dd-mm-yy',
	),
	'htmlOptions' => array(
		'style' => 'height:20px;'
	),
));
?>
			</td>
			<td><input type="submit" value="View" name="yt0" class="st-button save_continue"></td>
</tr>
</table>

</form>
<br/><br/>
</div>
<?php
if(count($datarows)>0) { ?>
	
<?php      
$prev = '';
$totalpaid = 0;
foreach($datarows as $k=>$data) {
	if($prev != $data['branch_name'])  { 
		if($k>0) {
			//$data['total_paid']=0;
		?>
			<tr class="gradeA even"  style="color:#178BFA;font-weight:bold;">
				<td align="right">Total (Rs)</td>
				<td class="center" /><?php echo number_format($totalpaid,2)?></td>
			</tr>
			</table>		
		<?php
		$totalpaid = 0;
		}
	?>
		<BR />
		<table cellspacing="0" cellpadding="0" border="0" id="example" class="display data-table" style="width:100%;">                        
			<thead>
			<tr class="gradeA odd" align="center">
				<td class="header ui-state-default" rowspan="1" colspan="1" style="width: 200px;">
					<div class=""><b>Branch Name</b></div>
				</td>
				<td class="header ui-state-default" rowspan="1" colspan="1" style="width: 175px;">
					<div class=""><b><?php echo $data['branch_name'];?></b></div>
				</td>
			</tr>
			</thead>
			<tbody>
			<tr class="gradeA even">
				<td><b>Batch</b></td>
				<td class="center"><b>Total Collection</b></td>
			</tr>
	<?php
		$prev = $data['branch_name'];
	}
	?>
	<tr class="">
		<td><?php echo $data['batch_name']?></td>
		<td class="center"><?php echo $data['total_paid']?></td>
		<?php $totalpaid = $totalpaid + $data['total_paid']?>
	</tr>		
<?php 		
};
?>
<tr class="gradeA even"  style="color:#178BFA;font-weight:bold;">
	<td align="right">Total (Rs)</td>
	<td class="center"><?php echo number_format($totalpaid,2)?></td>
</tr>
</table>
<?php } 
else {echo "<span style='padding-left:20px;'><br/>No records found</span>";}
?> 
</div>
<div class="clear"></div>
<br/>

<?php
//Branch / Batch wise Summary start
$command = $connection->createCommand('SELECT t.branch_id, br.branch_name, t.batch_id, b.batch_name, sum(t.paid_amount + t.paid_registration) as total_paid, sum(t.unpaid_amount + t.unpaid_registration) as total_unpaid, sum(t.fee_amount + t.registration_fee) as total_fee,sum(t.paid_registration) as total_regi_paid,
sum(t.unpaid_registration) as total_regi_unpaid, sum(t.registration_fee) as total_regi_fee,
sum(t.paid_amount) as total_sim_paid,sum(t.unpaid_amount) as total_sim_unpaid, 
sum(t.fee_amount) as total_sim_fee,
count(t.id) as no_of_student
FROM dream_student_batch as t
LEFT JOIN dream_branch as br ON br.id = t.branch_id 
LEFT JOIN dream_batch as b ON b.id = t.batch_id 	
WHERE 1 AND t.status=\'1\''.$condition.'
GROUP BY t.batch_id  order by br.branch_name;');

$branchrows = $command->queryAll();
//echo "<pre>";print_r($branchrows);echo "</pre>";
//$totalbranch = count($branchrows);

if(count($branchrows)>0) {
?>
<div id="batchwise">
<div><h2 class="h2-tag">Branch / Batch wise Summary</h2></div>

<?php 
$prev = '';
$feetotal = $paidtotal = $remaintotal = 0;
$fee_rtotal = $paid_rtotal = $remain_rtotal = 0;
$fee_stotal = $paid_stotal = $remain_stotal = 0;
$noofstudent = 0;
foreach($branchrows as $k=>$data) {
	//echo "<pre>";print_r($data);echo "</pre>";
	if($prev != $data['branch_name'])  { 
		if($k>0) {?>
		<tr class="gradeA even"  style="color:#178BFA;font-weight:bold;">
			<td align="right">Total (Rs)</td>
			<td class="center"><?php echo number_format($fee_rtotal,2)?></td>
			<td class="center"><?php echo number_format($paid_rtotal,2)?></td>
			<td class="center"><?php echo number_format($remain_rtotal,2)?></td>
			<td class="center"><?php echo number_format($fee_stotal,2)?></td>
			<td class="center"><?php echo number_format($paid_stotal,2)?></td>
			<td class="center"><?php echo number_format($remain_stotal,2)?></td>
			<td class="center"><?php echo number_format($feetotal,2)?></td>
			<td class="center"><?php echo number_format($paidtotal,2)?></td>
			<td class="center"><?php echo number_format($remaintotal,2)?></td>			
			<td class="center"><?php echo $noofstudent?></td>			
		</tr>
		</table>
		<?php
		$feetotal = $paidtotal = $remaintotal = 0;
		$fee_rtotal = $paid_rtotal = $remain_rtotal = 0;
		$fee_stotal = $paid_stotal = $remain_stotal = 0;
		$noofstudent = 0;
		}
	?>
		<BR />
		<table cellspacing="0" cellpadding="0" border="0" id="example" class="display data-table" style="width:100%;">                        
			<thead>
			<tr class="gradeA odd" align="center">
				<td class="header ui-state-default headerSortDown" rowspan="1" colspan="1" style="width: 200px;">
					<div class="DataTables_sort_wrapper"><b>Branch Name</b></div>
				</td>
				<td class="header ui-state-default" rowspan="1" colspan="10" style="width: 175px;">
					<div class="DataTables_sort_wrapper"><b><?php echo $data['branch_name'];?></b></div>
				</td>
			</tr>
			</thead>
			<tbody>
			<tr class="gradeA even">
				<td><b>Batch</b></td>
				<td class="center"><b>Total Reg. Fee</b></td>
				<td class="center"><b>Total Reg.Paid</b></td>
				<td class="center"><b>Remaining Reg. Fee</b></td>
				<td class="center"><b>Total Fee</b></td>
				<td class="center"><b>Total Fee Paid</b></td>
				<td class="center"><b>Remaining Fee</b></td>
				<td class="center"><b>Total Amount</b></td>
				<td class="center"><b>Total Paid Amount</b></td>
				<td class="center"><b>Remaining Fee Amount</b></td>
				<td class="center"><b>No Of Student</b></td>
			</tr>
	<?php
		$prev = $data['branch_name'];
	}
	?>
	<tr class="">
		<td><?php echo $data['batch_name']?></td>
		<td class="center"><?php echo $data['total_regi_fee']?></td>
		<td class="center"><?php echo $data['total_regi_paid']?></td>
		<td class="center"><?php echo $data['total_regi_unpaid']?></td>
		<td class="center"><?php echo $data['total_sim_fee']?></td>
		<td class="center"><?php echo $data['total_sim_paid']?></td>
		<td class="center"><?php echo $data['total_sim_unpaid']?></td>
		<td class="center"><?php echo $data['total_fee']?></td>
		<td class="center"><?php echo $data['total_paid']?></td>
		<td class="center"><?php echo $data['total_unpaid']?></td>
		<td class="center"><?php echo $data['no_of_student']?></td>
		<?php
		 $feetotal += $data['total_fee'];
		 $paidtotal += $data['total_paid'];
		 $remaintotal += $data['total_unpaid'];
		 $fee_rtotal += $data['total_regi_fee'];
		 $paid_rtotal += $data['total_regi_paid'];
		 $remain_rtotal += $data['total_regi_unpaid'];
		 $fee_stotal += $data['total_sim_fee'];
		 $paid_stotal += $data['total_sim_paid'];
		 $remain_stotal += $data['total_sim_unpaid'];
		 $noofstudent += $data['no_of_student'];
		 ?>
	</tr>		 
<?php 		
}
?>
<tr class="gradeA even"  style="color:#178BFA;font-weight:bold;">
	<td align="right">Total (Rs)</td>
	<td class="center"><?php echo number_format($fee_rtotal,2)?></td>
	<td class="center"><?php echo number_format($paid_rtotal,2)?></td>
	<td class="center"><?php echo number_format($remain_rtotal,2)?></td>
	<td class="center"><?php echo number_format($fee_stotal,2)?></td>
	<td class="center"><?php echo number_format($paid_stotal,2)?></td>
	<td class="center"><?php echo number_format($remain_stotal,2)?></td>
	<td class="center"><?php echo number_format($feetotal,2)?></td>
	<td class="center"><?php echo number_format($paidtotal,2)?></td>
	<td class="center"><?php echo number_format($remaintotal,2)?></td>
	<td class="center"><?php echo $noofstudent?></td>	
</tr>
</table>
<?php } ?>
</div><!-- end batchwise div --> 
<div class="clear"></div>
<br/>


<?php 
//Annual Paid Fee Summary
$command = $connection->createCommand("SELECT YEAR(t.pay_date) as year, t.batch_id, br.branch_name, b.batch_name, 
SUM(IF(MONTH(t.pay_date)=1, (t.amount_paid),0)) m1, 
SUM(IF(MONTH(t.pay_date)=2, (t.amount_paid),0)) m2, 
SUM(IF(MONTH(t.pay_date)=3, (t.amount_paid),0)) m3, 
SUM(IF(MONTH(t.pay_date)=4, (t.amount_paid),0)) m4, 
SUM(IF(MONTH(t.pay_date)=5, (t.amount_paid),0)) m5, 
SUM(IF(MONTH(t.pay_date)=6, (t.amount_paid),0)) m6, 
SUM(IF(MONTH(t.pay_date)=7, (t.amount_paid),0)) m7, 
SUM(IF(MONTH(t.pay_date)=8, (t.amount_paid),0)) m8, 
SUM(IF(MONTH(t.pay_date)=9, (t.amount_paid),0)) m9, 
SUM(IF(MONTH(t.pay_date)=10, (t.amount_paid),0)) m10, 
SUM(IF(MONTH(t.pay_date)=11, (t.amount_paid),0)) m11, 
SUM(IF(MONTH(t.pay_date)=12, (t.amount_paid),0)) m12,
sum((t.amount_paid)) as total_paid 

FROM dream_student_payment t 
LEFT JOIN dream_branch as br ON br.id = t.branch_id 
LEFT JOIN dream_student_batch as sbt ON sbt.id = t.stud_batch_id 
LEFT JOIN dream_batch as b ON b.id = t.batch_id 
WHERE 1 AND sbt.status='1' $condition
GROUP BY t.batch_id, YEAR(t.pay_date)
ORDER BY YEAR(t.pay_date) ASC, br.branch_name;");


$rows = $command->queryAll();


$command = $connection->createCommand("SELECT YEAR(t.pay_date) as year, 
SUM(IF(MONTH(t.pay_date)=1, (t.amount_paid),0)) m1, 
SUM(IF(MONTH(t.pay_date)=2, (t.amount_paid),0)) m2, 
SUM(IF(MONTH(t.pay_date)=3, (t.amount_paid),0)) m3, 
SUM(IF(MONTH(t.pay_date)=4, (t.amount_paid),0)) m4, 
SUM(IF(MONTH(t.pay_date)=5, (t.amount_paid),0)) m5, 
SUM(IF(MONTH(t.pay_date)=6, (t.amount_paid),0)) m6, 
SUM(IF(MONTH(t.pay_date)=7, (t.amount_paid),0)) m7, 
SUM(IF(MONTH(t.pay_date)=8, (t.amount_paid),0)) m8, 
SUM(IF(MONTH(t.pay_date)=9, (t.amount_paid),0)) m9, 
SUM(IF(MONTH(t.pay_date)=10, (t.amount_paid),0)) m10, 
SUM(IF(MONTH(t.pay_date)=11, (t.amount_paid),0)) m11, 
SUM(IF(MONTH(t.pay_date)=12, (t.amount_paid),0)) m12,
sum((t.amount_paid)) as total_paid 

FROM dream_student_payment t 
LEFT JOIN dream_branch as br ON br.id = t.branch_id 
LEFT JOIN dream_batch as b ON b.id = t.batch_id 
WHERE 1 $condition
GROUP BY YEAR(t.pay_date)
ORDER BY YEAR(t.pay_date) ASC, br.branch_name;");

$totalrows = $command->queryAll();
//echo "<pre>";print_r($totalrows);echo "</pre>";
if(count($rows)>0) {
?>
<div id="anualfeereport">
<div><h2 class="h2-tag">Annual Paid Fee Summary</h2></div>

<?php 
$prev = 0;
$pbranch = '';
$p=0;
foreach($rows as $k=>$data) {
	if($prev != $data['year'])  { 
		if($k>0){ 		
	?>
		<tr class="gradeA even"  style="color:#178BFA;font-weight:bold;">
			<td colspan="2" class="center"><b>Total (Rs)</b></td>			
			<td class="center"><?php echo number_format($totalrows[$p]['m1'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m2'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m3'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m4'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m5'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m6'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m7'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m8'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m9'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m10'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m11'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['m12'],2)?></td>
			<td class="center"><?php echo number_format($totalrows[$p]['total_paid'],2)?></td>
		</tr>	
		<?php $p++;
		echo '</table>';
		}?>
		<BR />
		<table cellspacing="0" cellpadding="0" border="0" id="example" class="display data-table">                        
			<thead>
			<tr class="gradeA odd" align="center">
				<td class="header ui-state-default headerSortDown" rowspan="1" colspan="2" style="width: 86px;">
				<div class="DataTables_sort_wrapper"><b>Year</b></div>
				</td>
				<td class="header ui-state-default" rowspan="1" colspan="12" style="width: 175px;">
				<div class="DataTables_sort_wrapper"><?php echo $data['year'];?></div>
				</td>
				<td class="header ui-state-default" rowspan="1" colspan="1" style="width: 175px;">
				<div class="DataTables_sort_wrapper"><b>Total (Rs)</b></div>
				</td>
			</tr>
			</thead>
			<tbody>
			<tr class="gradeA even">
				<td><b>Branch</b></td>
				<td><b>Batch</b></td>
				<td class="center"><b>January</b></td>
				<td class="center"><b>February</b></td>
				<td class="center"><b>March</b></td>
				<td class="center"><b>April</b></td>
				<td class="center"><b>May</b></td>
				<td class="center"><b>June</b></td>
				<td class="center"><b>July</b></td>
				<td class="center"><b>August</b></td>
				<td class="center"><b>September</b></td>
				<td class="center"><b>Octomber</b></td>
				<td class="center"><b>November</b></td>
				<td class="center"><b>December</b></td>
				<td class="center"><b>&nbsp;</b></td>
			</tr>
	<?php
		$prev = $data['year'];
	}
	?>
	<tr class="">
		<td><?php if($pbranch!=$data['branch_name']) {echo $data['branch_name'];}?></td>
		<td><?php echo $data['batch_name']?></td>
		<td class="center"><?php echo $data['m1']?></td>
		<td class="center"><?php echo $data['m2']?></td>
		<td class="center"><?php echo $data['m3']?></td>
		<td class="center"><?php echo $data['m4']?></td>
		<td class="center"><?php echo $data['m5']?></td>
		<td class="center"><?php echo $data['m6']?></td>
		<td class="center"><?php echo $data['m7']?></td>
		<td class="center"><?php echo $data['m8']?></td>
		<td class="center"><?php echo $data['m9']?></td>
		<td class="center"><?php echo $data['m10']?></td>
		<td class="center"><?php echo $data['m11']?></td>
		<td class="center"><?php echo $data['m12']?></td>
		<td class="center"><?php echo $data['total_paid']?></td>
	</tr>	
<?php 		
	$pbranch = $data['branch_name'];
}
?>
<tr class="gradeA even"  style="color:#178BFA;font-weight:bold;">
	<td colspan="2" class="center"><b>Total (Rs)</b></td>			
	<td class="center"><?php echo number_format($totalrows[$p]['m1'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m2'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m3'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m4'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m5'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m6'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m7'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m8'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m9'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m10'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m11'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['m12'],2)?></td>
	<td class="center"><?php echo number_format($totalrows[$p]['total_paid'],2)?></td>
</tr>
</table>
<?php } ?> 
</div>