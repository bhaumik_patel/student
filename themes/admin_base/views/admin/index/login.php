<?php
$this->pageTitle=Yii::app()->name . ' - Login';
/*$this->breadcrumbs=array(
	'Login',
);*/
?>
<!-- <div class="repairersreg"><h1><a href="Repairersreg/create"> Repairer Registration </a></h1></div> -->


<div class="form">
<!--<p>Please fill out the following form with your login credentials:</p>-->

<?php
foreach(Yii::app()->admin->getFlashes() as $key => $message) {
	echo '<div class="albox errorbox"><div class="flash-' . $key . '">' . $message . "</div></div>\n";
}
?>		


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableAjaxValidation'=>false,
 
	'enableClientValidation'=>true,

      'clientOptions' => array(
		      'validateOnSubmit'=>true,
		      //'validateOnChange'=>true,
		      //'validateOnType'=>false,
          ),
)); ?>
	
	<p class="note">
		<?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
	</p>
	<br />

	<div class="row">
		<?php echo $form->labelEx($model,'username', array('class'=>'log-lab')); ?>
		<?php echo $form->textField($model,'username', array('class'=>'login-input-user')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password', array('class'=>'log-lab')); ?>
		<?php echo $form->passwordField($model,'password', array('class'=>'login-input-pass')); ?>
		<?php echo $form->error($model,'password'); ?>
		<p class="hint">
			
		</p>
	</div>

	<div class="row rememberMe">
    	<div class="remember-me-div fl">
				<?php echo $form->checkBox($model,'rememberMe'); ?>
                <?php echo $form->label($model,'rememberMe'); ?>
                <?php echo $form->error($model,'rememberMe'); ?>
        </div>
	</div>
	 <br/>
	<!--div class="row">
    	<div class="remember-me-div fl">
               	<p class="forgot-password-div fl"> 
		        	<a href="Site/forgot"> Forgot Password? </a>
		        </p>
		 </div>
	</div-->
	 <br/>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Login', array('class'=>'button')); ?>
	</div>
	 

<?php $this->endWidget(); ?>
</div><!-- form -->
