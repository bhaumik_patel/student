<?php
$this->pageTitle=Yii::app()->name . ' - Login';
/*$this->breadcrumbs=array(
	'Login',
);*/
?>
<!-- <div class="repairersreg"><h1><a href="Repairersreg/create"> Repairer Registration </a></h1></div> -->

 <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
						<?php
						foreach(Yii::app()->admin->getFlashes() as $key => $message) {
							echo '<div class="albox errorbox"><div class="flash-' . $key . '">' . $message . "</div></div>\n";
						}
						?>		


						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'login-form',
							'enableAjaxValidation'=>false,
						 
							'enableClientValidation'=>true,

							  'clientOptions' => array(
									  'validateOnSubmit'=>true,
									  //'validateOnChange'=>true,
									  //'validateOnType'=>false,
								  ),
						)); ?>
                        <form role="form">
                            <fieldset>

                                <div class="form-group">
                                    <?php echo $form->textField($model,'username', array('class'=>'login-input-user form-control', 'placeholder'=>'Username',  'type'=>'email', 'autofocus')); ?>
									<?php echo $form->error($model,'username'); ?>
                                </div>
                                <div class="form-group">
                                    <?php echo $form->passwordField($model,'password', array('class'=>'login-input-pass form-control', 'placeholder'=>'Password',  'type'=>'password', 'autofocus')); ?>
									<?php echo $form->error($model,'password'); ?>
									<p class="hint">
										
									</p>
                                </div>
                                <div class="checkbox">
									<label>
									<?php echo $form->checkBox($model,'rememberMe'); ?>
										Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
								<?php echo CHtml::submitButton('Login', array('class'=>'btn btn-lg btn-success btn-block')); ?>
                            </fieldset>
                        <?php $this->endWidget(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div><!-- form -->
