<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->getBaseUrl() ?>/css/admin_dev.css" />
        <link rel="shortcut icon" href="<?php echo Yii::app()->getBaseUrl() ?>/favicon.ico" type="image/x-icon"/> 
    </head>
    <body>

        <div class="wrapper">
            <!-- START HEADER -->
            <div id="header">
                <!-- logo -->
                <div class="logo">	<a href="<?php echo AdminModule::getUrl() ?>"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-admin.png" width="300" alt="logo"/></a>
                        <!--small>Version: <?php echo SystemConfig::getValue('system_version') ?></small-->
                </div>

                <?php //Yii::app()->bootstrap->register(); ?>

                <!-- quick menu -->
                <script>
                    $(document).ready(function () {
                        $('#quickmenu-right a').click(function () {
                            $.ajax({
                                type: "POST",
                                url: '<?php echo GxHtml::normalizeUrl(array('index/changeSkin')) ?>',
                                data: 'skin=' + $(this).attr('skin'),
                                dataType: "json",
                                success: function (data) {
                                    if (data.success == 1) {
                                        window.location.reload();
                                    } else {

                                    }
                                },
                                failure: function (errMsg) {
                                    alert(errMsg);
                                }
                            });
                        });
                    });
                </script>

                <!-- skin box -->
                <div id="skin_quickmenu">
                    <a href="#" class="display">
                        <?php /*
                          <img src="<?php echo UtilityHtml::getAdminSkinUrl() ?>/img/simple-profile-img.jpg" width="33" height="33" alt="profile"/>
                         */ ?><b>Skin</b> <span class="box<?php echo SystemConfig::getValue('admin_skin') . '-active' ?>">&nbsp;</span>
                    </a>
                    <div id="quickmenu-right" class="skinmenu">
                        <ul>
                            <li><a href="#" class="qbutton-left tips-left" title="Blue" skin="blue"><div class="blue box<?php echo ((SystemConfig::getValue('admin_skin') == 'blue') ? '-active' : '') ?>">&nbsp;</div></a></li>
                            <li><a href="#" class="qbutton-normal tips-left" title="Dark" skin="dark"><div class="dark box<?php echo ((SystemConfig::getValue('admin_skin') == 'dark') ? '-active' : '') ?>">&nbsp;</div></a></li>
                            <li><a href="#" class="qbutton-normal tips-left" title="Green" skin="green"><div class="green box<?php echo ((SystemConfig::getValue('admin_skin') == 'green') ? '-active' : '') ?>">&nbsp;</div></a></li>
                            <li><a href="#" class="qbutton-normal tips-left" title="Orange" skin="orange"><div class="orange box<?php echo ((SystemConfig::getValue('admin_skin') == 'orange') ? '-active' : '') ?>">&nbsp;</div></a></li>
                            <li><a href="#" class="qbutton-normal tips-left" title="Purple" skin="purple"><div class="purple box<?php echo ((SystemConfig::getValue('admin_skin') == 'purple') ? '-active' : '') ?>">&nbsp;</div></a></li>
                            <li><a href="#" class="qbutton-right tips-left" title="Sea Green" skin="seaGreen"><div class="seagreen box<?php echo ((SystemConfig::getValue('admin_skin') == 'seaGreen') ? '-active' : '') ?>">&nbsp;</div></a></li>
                        </ul>
                        <div class="clear"></div>
                    </div>
                </div>

                <!-- profile box -->
                <div id="profilebox">
                    <a href="#" class="display">
                        <?php /*
                          <img src="<?php echo UtilityHtml::getAdminSkinUrl() ?>/img/simple-profile-img.jpg" width="33" height="33" alt="profile"/>
                         */ ?><b>Logged in as</b>	<span><?php echo ucwords(UtilityHtml::getDataByKey(Yii::app()->admin->getState('admin'), 'user_type')); ?></span>
                    </a>
                    <div class="profilemenu">
                        <ul>
                            <li><A href="<?php echo $this->createUrl('adminUser/updateProfile') ?>"><?php echo Yii::t('vk', 'My Account'); ?></A></li>
                            <li><A href="<?php echo $this->createUrl('adminUser/ChangePass') ?>"><?php echo Yii::t('vk', 'Change Password'); ?></A></li>
                            <li><A href="<?php echo CHtml::encode(AdminModule::getLoginUrl()) ?>"><?php echo CHtml::encode(AdminModule::getLoginText()) ?></A></li>
                        </ul>
                    </div>


                </div>
                <!-- notifications -->
                <?php /*
                  <div id="notifications">
                  <a href="<?php echo AdminModule::getUrl()?>" class="qbutton-left tips" original-title="<?php echo Yii::t('vk','Dashboard')?>"><img src="<?php echo UtilityHtml::getAdminSkinUrl1() ?>/vk_dashboard.png" alt="<?php echo Yii::t('vk','Dashboard')?>" title="<?php echo Yii::t('vk','Dashboard')?>"/></a>
                  <a href="<?php echo CController::createUrl('AdminUser/admin') ?>" class="qbutton-normal tips" original-title="<?php echo Yii::t('vk','System Users')?>"><img src="<?php echo UtilityHtml::getAdminSkinUrl1() ?>/vk_users.png" alt="<?php echo Yii::t('vk','System Users')?>" title="<?php echo Yii::t('vk','System Users')?>"/></a>
                  <a href="<?php echo CController::createUrl('systemConfig/admin') ?>" class="qbutton-right tips" original-title="<?php echo Yii::t('vk','System Setting')?>"><img src="<?php echo UtilityHtml::getAdminSkinUrl1() ?>/vk_settings.png" alt="<?php echo Yii::t('vk','System Settings')?>" title="<?php echo Yii::t('vk','System Settings')?>"/></a>
                  <!--
                  <a href="#" class="qbutton-normal tips"><img src="<?php echo UtilityHtml::getAdminSkinUrl() ?>/img/icons/header/message.png" width="19" height="13" alt="message" /> <span class="qballon">23</span> </a>
                  <a href="#" class="qbutton-right"><img src="<?php echo UtilityHtml::getAdminSkinUrl() ?>/img/icons/header/support.png" width="19" height="13" alt="support" /> <span class="qballon">8</span> </a>
                  -->
                  <div class="clear"></div>
                  </div>
                 */ ?>
                <div class="clear"></div>
            </div>
            <!-- END HEADER -->
            <!-- START MAIN -->
            <div id="main">      
                <!-- START SIDEBAR -->
                <div id="sidebar">

                    <!-- start searchbox -->
                    <div id="searchbox">


                        <div class="in">
                            <h2 class="h2-tag">
                                <?php
                                if ($u = AdminUser::model()->findByPk(AdminModule::getUserDataByKey('id'))) {
                                    if ($u->user_type == 'admin') {
                                        echo $u->branch;
                                    } else {
                                        
                                    }
                                }
                                ?>
                            </h2>
                        </div>
                        <!-- 
                        <div class="in">
                          <form id="form1" name="form1" method="post" action="">
                                <input name="textfield" type="text" class="input" id="textfield" onfocus="$(this).attr('class','input-hover')" onblur="$(this).attr('class','input')"  />
                          </form>
                        </div>
                        -->
                    </div>
                    <!-- end searchbox -->

                    <!-- start sidemenu -->
                    <div id="sidemenu"><?php $this->widget('AdminMenu', JVAccessControlFilter::getAdminMenuItems()); ?></div>
                    <!-- end sidemenu -->

                </div>
                <!-- END SIDEBAR -->

                <!-- START PAGE -->
                <div id="page">
                    <div class="page_content">

                        <!-- start page title -->
                        <div class="page-title">

                            <div class="breadcrumb"><?php $this->widget('application.extensions.inx.AdminBreadcrumbs', array('links' => $this->breadcrumbs,)); ?></div>
                            <div class="in">

                                <div class="titlebar">	
                                    <h2><?php echo $this->pageTitle ?></h2>
                                </div>

                                <div class="shortcuts-icons">
                                    <?php
                                    $this->beginWidget('zii.widgets.CPortlet', array(
                                        'title' => '',
                                    ));
                                    $this->widget('zii.widgets.CMenu', array(
                                        'items' => $this->menu,
                                        'htmlOptions' => array('class' => 'operations'),
                                        'encodeLabel' => false,
                                    ));
                                    //'templateItem'=>'operation'
                                    $this->endWidget();
                                    ?>

                                </div>
                            </div>
                            <?php
                            if ($this->action->Id == 'admin' || $this->action->id=='report') {
                                $this->widget('ext.mPrint.mPrint', array(
                                    'title' => SystemConfig::getValue('site_name'), //the title of the document. Defaults to the HTML title
                                    'tooltip' => 'Print', //tooltip message of the print icon. Defaults to 'print'
                                    'text' => 'Print', //text which will appear beside the print icon. Defaults to NULL
                                    'element' => '.page_content', //the element to be printed.
                                    'exceptions' => array(//the element/s which will be ignored
                                        '#print_link', '.operations', '.filters', '.summary', '.view', '.update', '.delete', '.change'
                                    //'.search-form'
                                    ),
                                    'publishCss' => true, //publish the CSS for the whole page?
                                    //'visible' => Yii::app()->user->checkAccess('print'),  //should this be visible to the current user?
                                    'alt' => 'print', //text which will appear if image can't be loaded
                                    'debug' => false, //enable the debugger to see what you will get
                                    'id' => 'print_link'         //id of the print link
                                ));
                            }
                            ?>
                            <div class="clear"></div>
                        </div>
                        <!-- end page title -->
							
                        <!-- START CONTENT -->
                        <div class="content">
							<?php
							
							  foreach(Yii::app()->admin->getFlashes() as $key => $message) {
							echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
						  }
						  ?>
                            <?php echo $content; ?> 
                        </div>
                        <!-- END CONTENT -->
                    </div>
                </div>
                <!-- END PAGE -->

                <div class="clear"></div>
            </div>
            <!-- END MAIN -->

            <!-- START FOOTER -->
            <div id="footer">
                <div class="left-column">&copy; Copyright <?php echo date('Y'); ?> - All rights reserved.</div>	
				<?php include('expire.php');?>			
                <!--div class="right-column"><?php echo SystemConfig::getValue('site_name') ?> - Version: <?php echo SystemConfig::getValue('system_version') ?></a></div-->
				
            </div>
            <!-- END FOOTER -->    
        </div>

        <?php /*
          <div class="container" id="page">
          <div id="header">
          <div id="logo">
          <div class="header-left">
          <img width="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-admin.png" alt="" />
          <span class="main-text">Admin Panel</span>
          </div>

          <div class="header-right"><A href="<?php echo CHtml::encode(AdminModule::getLoginUrl())?>"><?php echo CHtml::encode(AdminModule::getLoginText()) ?></A>
          <?php echo CHtml::encode(AdminModule::getWelcomeText()); ?></div>
          </div>
          </div>
          <!-- header -->

          <div id="adminmenu" class="jqueryslidemenu">
          <?php  $this->widget('AdminMenu', JVAccessControlFilter::getAdminMenuItems()); ?>
          </div>
          <!-- mainmenu -->
          <!-- breadcrumbs start -->
          <div class="breadcrumb">
          <?php  $this->widget('application.extensions.inx.AdminBreadcrumbs', array(
          'links'=>$this->breadcrumbs,

          ));
          //m($this); //->breadcrumbs->homeLink = 'admin';
          ?>
          </div>
          <!-- breadcrumbs Ends -->
          <div class="maincont">
          <?php
          foreach(Yii::app()->user->getFlashes() as $key => $message) {
          echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
          }
          ?>
          <div class="content_left">
          <div id="sidebar">
          <?php
          $this->beginWidget('zii.widgets.CPortlet', array(
          'title'=>'Operations',
          ));
          $this->widget('zii.widgets.CMenu', array(
          'items'=>$this->menu,
          'htmlOptions'=>array('class'=>'operations'),
          ));
          $this->endWidget();
          ?>
          </div>
          <!-- sidebar -->
          </div>
          <div  class="content_right"> <?php echo $content; ?>
          <?php if($this->action->id != 'admin' && $this->action->id !='index'): ?>

          <a href="javascript:history.back()" class="backlink"> <?php echo Yii::t('app', 'Back');?> </a>
          <?php endif;?></div>
          </div>
          </div>
          <!-- footer -->
          <div id="footer"> Copyright &copy; <?php echo date('Y'); ?> by <?php echo SystemConfig::getValue('site_name')?>. All Rights Reserved.<br/>
          </div>

          <!-- page -->
         */ ?>
    </body>
</html>