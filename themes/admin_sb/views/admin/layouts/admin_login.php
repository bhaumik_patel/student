<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="language" content="en" />
<link rel="shortcut icon" href="<?php echo Yii::app()->getBaseUrl() ?>/favicon.ico" type="image/x-icon"/>
</head>
<body>
    <div class="loginform">
    	<div class="title"> 
		<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-admin.png" width="300"/></div>
        <div class="body">
       
       	  <?php
	    foreach(Yii::app()->admin->getFlashes() as $key => $message) {
	        echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
	    }
		?>
     	<?php echo $content; ?> 
        </div>
    </div>
</body>
</html>