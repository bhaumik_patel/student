ALTER TABLE `dream_student_batch`
	CHANGE COLUMN `paid_amount` `paid_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `fee_amount`,
	CHANGE COLUMN `unpaid_amount` `unpaid_amount` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `paid_amount`;
ALTER TABLE `dream_student_batch`
	ADD COLUMN `paid_registration` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `is_paid_registration`,
	ADD COLUMN `unpaid_registration` DECIMAL(10,2) NOT NULL DEFAULT '0.00' AFTER `paid_registration`;
ALTER TABLE `dream_student_payment`
	CHANGE COLUMN `amount_paid` `amount_paid` DECIMAL(10,2) NULL DEFAULT '0.00' COMMENT 'Paid Fee Amount' AFTER `month`,
	ADD COLUMN `registration_paid` DECIMAL(10,2) NULL DEFAULT '0.00' COMMENT 'Paid Registration Amount' AFTER `amount_paid`;
ALTER TABLE `dream_student_batch`
	ADD COLUMN `is_paid_fee` TINYINT(1) NULL DEFAULT '0' AFTER `fee_amount`;